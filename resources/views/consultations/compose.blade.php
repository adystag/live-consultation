@extends('layouts.app')

@section('main')
<div class="p-3 bg-white shadow-sm rounded">
    <h3 class="mb-3">
        Open Consultation
    </h3>

    @if (session('status'))
    <div class="alert alert-{{ session('status') }} alert-dismissible fade show mb-3" role="alert">
        {{ session('status-message') }}

        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

    <form method="POST" action="{{ route("create-consultation") }}">
        @csrf
        <div class="d-flex align-items-center">
            <h5 class="m-0">Pick Consultant</h5>

            <button type="submit" class="btn btn-primary ml-auto">Submit</button>
        </div>

        <compose-consultation></compose-consultation>
    </form>
</div>

<div id="profile-modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content p-3 d-flex flex-colum align-items-center justify-content-center" style="position: relative;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute; top: 0; right: 8px;">
                <span aria-hidden="true" style="font-size: 30px;">&times;</span>
            </button>

            <img id="profile-avatar" class="rounded-circle" src="" style="height: 128px; width: 128px; object-fit: contain; object-position: center;">

            <div class="mt-1 text-center">
                <p class="m-0" style="font-size: 24px;">
                    <b id="profile-name">User Name</b>
                </p>

                <p id="profile-role" style="font-size: 18px;">User Role</p>
            </div>
        </div>
    </div>
</div>
@endsection

@push('data-scripts')
<script>
    window.Laravel = {
        user: {!! json_encode(Auth::user()->toArray()) !!},
        categories: {!! json_encode($categories) !!},
        defaultProfpic: "{!! asset('img/user.png') !!}"
    }
</script>
@endpush
