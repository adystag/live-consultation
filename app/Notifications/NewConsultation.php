<?php

namespace App\Notifications;

use Carbon\Carbon;
use App\Models\Consultation;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;

class NewConsultation extends Notification
{
    use Queueable;

    protected $consultation;

    /**
    * Create a new notification instance.
    *
    * @return void
    */
    public function __construct(Consultation $consultation)
    {
        $this->consultation = $consultation;
    }

    /**
    * Get the notification's delivery channels.
    *
    * @param  mixed  $notifiable
    * @return array
    */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
    * Get the array representation of the notification.
    *
    * @param  mixed  $notifiable
    * @return array
    */
    public function toDatabase($notifiable)
    {
        $data = $this->consultation->toArray();
        $data['consultation_id'] = $this->consultation->id;

        return $data;
    }

    /**
    * Get the broadcastable representation of the notification.
    *
    * @param  mixed  $notifiable
    * @return BroadcastMessage
    */
    public function toBroadcast($notifiable)
    {
        $consultation = $this->consultation->toArray();
        $consultation['consultation_id'] = $this->consultation->id;
        $consultation['notified_at'] = Carbon::now()->toDateString();
        $consultation['isRead'] = false;

        return new BroadcastMessage($consultation);
    }
}
