<?php

namespace App\Notifications;

use Carbon\Carbon;
use App\Models\Respond;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;

class CaseRespond extends Notification
{
    use Queueable;

    protected $respond;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Respond $respond)
    {
        $this->respond = $respond;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
    * Get the array representation of the notification.
    *
    * @param  mixed  $notifiable
    * @return array
    */
    public function toDatabase($notifiable)
    {
        $data = $this->respond->toArray();
        $data['respond_id'] = $this->respond->id;
        
        return $data;
    }
    
    /**
    * Get the broadcastable representation of the notification.
    *
    * @param  mixed  $notifiable
    * @return BroadcastMessage
    */
    public function toBroadcast($notifiable)
    {
        $respond = $this->respond->toArray();
        $respond['respond_id'] = $this->respond->id;
        $respond['notified_at'] = Carbon::now()->toDateString();
        $respond['isRead'] = false;

        return new BroadcastMessage($respond);
    }
}
