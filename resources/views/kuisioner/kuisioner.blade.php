@extends('layouts.app')

@section('main')
<div class="p-3 rounded shadow-sm bg-white">

    @if (session('status'))
    <div class="alert alert-{{ session('status') }} alert-dismissible fade show mb-3" role="alert">
        {{ session('status-message') }}

        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <form action="{{ route('tambahdatabdi')}}" method="POST">
        @csrf
        <form id="regForm" action="">

            <h3 class="mb-3">Kuisioner</h3>

            <!-- One "tab" for each step in the form: -->
            <div class="tab">Dass
                <table class="table table-striped mb-0">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Kondisi</th>
                            <th scope="col">0</th>
                            <th scope="col">1</th>
                            <th scope="col">2</th>
                            <th scope="col">3</th>
                        </tr>
                    </thead>

                    <tbody>
                        <input type="hidden" name="email" value="email">
                        <tr>
                            <th>1</th>
                            <td>Menjadi marah karena hal-hal kecil/sepele
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d1" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d1" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d1" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d1" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>2</th>
                            <td>Mulut terasa kering
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d2" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d2" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d2" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d2" value="3"></label></td>
                        </tr>
                        <tr>
                            <th>3</th>
                            <td>Tidak dapat melihat hal yang positif dari suatu kejadian
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d3" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d3" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d3" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d3" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>4</th>
                            <td>Merasakan gangguan dalam bernafas (nafas cepat, sulit bernafas)
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d4" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d4" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d4" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d4" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>5</th>
                            <td>Merasa tidak kuat lagi untuk melakukan kegiatan
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d5" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d5" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d5" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d5" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>6</th>
                            <td>Cenderung bereaksi berlebihan terhadap suatu situasi
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d6" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d6" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d6" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d6" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>7</th>
                            <td>Kelemahan pada anggota tubuh
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d7" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d7" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d7" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d7" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>8</th>
                            <td>Kesulitan untuk berelaksasi/bersantai
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d8" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d8" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d8" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d8" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>9</th>
                            <td>Cemas berlebihan dalam suatu situasi namun bisa lega bila situasi/hal itu berakhir
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d9" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d9" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d9" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d9" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>10</th>
                            <td>Pesimis
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d10" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d10" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d10" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d10" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>11</th>
                            <td>Mudah merasa kesal
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d11" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d11" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d11" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d11" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>12</th>
                            <td>Merasa banyak menghasilkan energi karena cemas
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d12" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d12" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d12" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d12" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>13</th>
                            <td>Merasa sedih dan depresi
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d13" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d13" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d13" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d13" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>14</th>
                            <td>Tidak sabaran
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d14" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d14" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d14" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d14" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>15</th>
                            <td>Kelelahan
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d15" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d15" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d15" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d15" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>16</th>
                            <td>Kehilangan minat pada banyak hal (misal makan, ambulasi, sosialiasi)
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d16" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d16" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d16" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d16" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>17</th>
                            <td>Merasa diri tidak layak
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d17" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d17" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d17" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d17" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>18</th>
                            <td>Mudah tersinggung
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d18" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d18" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d18" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d18" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>19</th>
                            <td>Berkeringat
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d19" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d19" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d19" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d19" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>20</th>
                            <td>Ketakutan tanpa alasan yang jelas
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d20" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d20" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d20" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d20" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>21</th>
                            <td>Merasa hidup tidak berharga
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d21" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d21" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d21" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d21" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>22</th>
                            <td>Sulit untuk beristirahat
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d22" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d22" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d22" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d22" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>23</th>
                            <td>Kesulitan dalam menelan
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d23" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d23" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d23" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d23" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>24</th>
                            <td>Tidak dapat menikmati hal-hal yang dilakukan
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d24" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d24" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d24" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d24" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>25</th>
                            <td>Perubahan kegiatan jantung dan denyut nadi tanpa stimulasi oleh latihan fisik
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d25" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d25" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d25" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d25" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>26</th>
                            <td>Merasa hilang harapan dan putus asa
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d26" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d26" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d26" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d26" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>27</th>
                            <td>Mudah marah
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d27" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d27" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d27" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d27" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>28</th>
                            <td>Mudah panik
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d28" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d28" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d28" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d28" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>29</th>
                            <td>Kesulitan untuk tenang setelah sesuatu yang mengganggu
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d29" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d29" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d29" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d29" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>30</th>
                            <td>Takut diri terhambat oleh tugas -tugas yang tidak bisa dilakukan
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d30" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d30" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d30" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d30" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>31</th>
                            <td>Sulit untuk antusias pada banyak hal
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d31" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d31" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d31" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d31" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>32</th>
                            <td>Sulit menteloransi gangguan-gangguan terhadap hal-hal yang dilakukan
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d32" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d32" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d32" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d32" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>33</th>
                            <td>Berada pada keadaan tegang
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d33" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d33" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d33" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d33" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>34</th>
                            <td>Berasa tidak berharga
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d34" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d34" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d34" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d34" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>35</th>
                            <td>Tidak dapat memaklumi hal apapun yang menghalangi untuk menyelesaikan hal yang sedang
                                dilakukan
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d35" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d35" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d35" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d35" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>36</th>
                            <td>Ketakutan
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d36" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d36" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d36" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d36" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>37</th>
                            <td>Tidak ada harapan untuk masa depan
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d37" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d37" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d37" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d37" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>38</th>
                            <td>Merasa hidup tidak berarti
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d38" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d38" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d38" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d38" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>39</th>
                            <td>Mudah gelisah
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d39" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d39" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d39" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d39" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>40</th>
                            <td>Khawatir dengan situasi saat diri anda mungkin menjadi panik dan mempermalukan diri anda
                                sendiri
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d40" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d40" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d40" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d40" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>41</th>
                            <td>Gemetar
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d41" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d41" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d41" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d41" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>42</th>
                            <td>Sulit untuk meningkatkan inisaitif dalam melakukan sesuatu
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="d42" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d42" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d42" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="d42" value="3"></label></td>

                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="tab">BDI
                <table class="table table-striped mb-0">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Item</th>
                            <th scope="col">Kondisi</th>
                            <th scope="col">0</th>
                            <th scope="col">1</th>
                            <th scope="col">2</th>
                            <th scope="col">3</th>
                        </tr>
                    </thead>

                    <tbody>
                        <input type="hidden" name="email" value="{{ Auth::user()->email }}">
                        <tr>
                            <th>1</th>
                            <td>Kesedihan
                            </td>
                            <td>Saya begitu sedih sehingga saya merasa tidak tahan lagi
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="b1" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b1" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b1" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b1" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>2</th>
                            <td>Pesimis
                            </td>
                            <td>Saya merasa tidak ada sesuatu yang saya nantikan
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="b2" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b2" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b2" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b2" value="3"></label></td>
                        </tr>
                        <tr>
                            <th>3</th>
                            <td>Kegagalan
                            </td>
                            <td>Saya merasa lebih banyak mengalami kegagalan daripada rata – rata orang
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="b3" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b3" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b3" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b3" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>4</th>
                            <td>Kehilangan Kesenangan
                            </td>
                            <td>Saya tidak dapat menikmati segala sesuatu seperti biasanya
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="b4" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b4" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b4" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b4" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>5</th>
                            <td>Perasaan Bersalah
                            </td>
                            <td>Saya cukup sering merasa bersalah
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="b5" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b5" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b5" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b5" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>6</th>
                            <td>Perasaan akan Hukuman
                            </td>
                            <td>Saya merasa bahwa saya sedang dihukum
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="b6" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b6" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b6" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b6" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>7</th>
                            <td>Tidak Menyukai Diri Sendiri
                            </td>
                            <td>Saya tidak merasa kecewa terhadap diri saya sendiri
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="b7" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b7" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b7" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b7" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>8</th>
                            <td>Mengkritik Diri Sendiri
                            </td>
                            <td>Saya selalu mencela diri saya sendiri karena kelemahan atau kekeliruan saya
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="b8" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b8" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b8" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b8" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>9</th>
                            <td>Pikiran atau Keinginan Bunuh Diri
                            </td>
                            <td>Saya ingin bunuh diri
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="b9" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b9" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b9" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b9" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>10</th>
                            <td>Menangis
                            </td>
                            <td>Saya biasanya dapat menangis, tetapi sekarang saya tidak dapat menangis meskipun saya
                                ingin menangis
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="b10" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b10" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b10" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b10" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>11</th>
                            <td>Pergolakan dalam Diri
                            </td>
                            <td>Saya tidak dibuat jengkel oleh hal – hal yang biasanya menjengkelkan saya
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="b11" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b11" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b11" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b11" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>12</th>
                            <td>Kehilangan Minat
                            </td>
                            <td>Saya masih tetap senang bergaul dengan orang lain
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="b12" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b12" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b12" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b12" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>13</th>
                            <td>Bimbang/ragu-ragu
                            </td>
                            <td>Saya lebih banyak menunda keputusan daripada biasanya
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="b13" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b13" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b13" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b13" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>14</th>
                            <td>Perasaan Tidak Berharga
                            </td>
                            <td>Saya tidak merasa bahwa saya kelihatan lebih jelek daripada sebelumnya
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="b14" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b14" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b14" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b14" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>15</th>
                            <td>Kehilangan Energi
                            </td>
                            <td>Saya membutuhkan usaha istimewa untuk mulai mengerjakan sesuatu
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="b15" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b15" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b15" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b15" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>16</th>
                            <td>Kehilangan Pola Tidur
                            </td>
                            <td>Saya bangun beberapa jam lebih awal daripada biasanya dan tidak dapat tidur kembali
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="b16" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b16" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b16" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b16" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>17</th>
                            <td>Cepat Marah
                            </td>
                            <td>Saya merasa terlalu lelah untuk mengerjakan apa saja
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="b17" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b17" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b17" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b17" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>18</th>
                            <td>Perubahan Nafsu Makan
                            </td>
                            <td>Nafsu makan saya masih seperti biasanya
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="b18" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b18" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b18" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b18" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>19</th>
                            <td>Kesulitan Konsentrasi
                            </td>
                            <td>Saya telah kehilangan berat badan 2,5 kg lebih
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="b19" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b19" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b19" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b19" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>20</th>
                            <td>Keletihan atau Kelelahan
                            </td>
                            <td>Saya sangat cemas akan masalah kesehatan fisik saya dan sulit memikirkan hal – hal
                                lainnya
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="b20" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b20" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b20" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b20" value="3"></label></td>

                        </tr>
                        <tr>
                            <th>21</th>
                            <td>Kehilangan Ketertarikan akan Seks
                            </td>
                            <td>Saya sama sekali kehilangan minat terhadap seks
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="b21" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b21" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b21" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="b21" value="3"></label></td>

                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="tab">TMAS
                <table class="table table-striped mb-0">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Kondisi</th>
                            <th scope="col">Y</th>
                            <th scope="col">T</th>
                        </tr>
                    </thead>

                    <tbody>
                        <input type="hidden" name="email" value="{{ Auth::user()->email }}">
                        <tr>
                            <th>1</th>
                            <td>Saya tidak cepat lelah
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t1" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t1" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>2</th>
                            <td>Saya sering mengalami perasaan mual
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t2" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t2" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>3</th>
                            <td>Saya sering merasa tegang pada waktu bekerja/beraktifitas
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t3" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t3" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>4</th>
                            <td>Saya merasa sukar untuk konsentrasi pada suatu hal
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t4" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t4" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>5</th>
                            <td>Saya cemas akan keadaan sakit saya
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t5" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t5" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>6</th>
                            <td>Saya sering melihat bahwa tangan saya bergetar apabila saya mencoba mengerjakan sesuatu
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t6" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t6" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>7</th>
                            <td>Muka saya sering menjadi merah seperti juga terjadi pada orang lain
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t7" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t7" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>8</th>
                            <td>Saya sering merasa khawatir akan kemungkinan terjadinya hal-hal yang tidak menyenangkan
                                pada diri saya
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t8" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t8" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>9</th>
                            <td>Saya sering takut bahwa muka saya sering merah (tersipu malu)
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t9" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t9" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>1Y</th>
                            <td>Saya sering mengalami mimpi yang menakutkan pada waktu tidur
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t10" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t10" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>11</th>
                            <td>Saya mudah berkeringat meski hari tidak panas
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t11" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t11" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>12</th>
                            <td>Kadang-kadang saya merasa kikuk, saya menjadi berkeringat dan sangat mengganggu saya
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t12" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t12" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>13</th>
                            <td>Saya jarang merasa jantung berdebar-debar dan saya jarang merasa tersengal-sengal
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t13" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t13" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>14</th>
                            <td>Saya setiap saat merasa lapar
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t14" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t14" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>15</th>
                            <td>Saya sering mengalami gangguan perut
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t15" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t15" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>16</th>
                            <td>Saya sering tidak dapat tidur karena mengkhawatirkan sesuatu
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t16" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t16" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>17</th>
                            <td>Tidur saya tidak nyenyak dan sering terganggu
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t17" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t17" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>18</th>
                            <td>Saya sering mimpi mengenai hal yang tidak saya ceritakan pada orang lain
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t18" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t18" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>19</th>
                            <td>Saya mudah untuk merasa kikuk
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t19" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t19" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>2Y</th>
                            <td>Saya sering menemukan bahwa saya mengkhawatirkan sesuatu
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t20" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t20" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>21</th>
                            <td>Saya biasanya tenang dan tidak mudah marah
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t21" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t21" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>22</th>
                            <td>Saya mudah menangis
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t22" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t22" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>23</th>
                            <td>Saya hampir selalu merasa khawatir mengenai suatu hal atau seseorang
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t23" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t23" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>24</th>
                            <td>Saya hampir selalu gembira
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t24" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t24" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>25</th>
                            <td>Saya merasa gelisah apabila saya harus menanti
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t25" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t25" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>26</th>
                            <td>Pada waktu-waktu tertentu saya gelisah
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t26" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t26" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>27</th>
                            <td>Kadang-kadang saya begitu bergelora, sehingga sangat sukar bagi saya untuk tidur
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t27" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t27" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>28</th>
                            <td>Saya kadang-kadang merasa bahwa kesukaran-kesukaran menumpuk begitu tinggi sehingga saya
                                tidak dapat mengatasinya
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t28" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t28" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>29</th>
                            <td>Pada waktu-waktu tertentu saya merasa khawatir tanpa alasan mengenai sesuatu yang
                                sesungguhnya tidak berarti
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t29" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t29" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>30</th>
                            <td>Apabila dibanding dengan teman-teman saya, saya tidak banyak mempunyai
                                ketakutan-ketakutan seperti mereka
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t30" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t30" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>31</th>
                            <td>Saya sering takut terhadap benda-benda atau manusia yang saya tahu tidak akan menyakiti
                                saya
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t31" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t31" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>32</th>
                            <td>Pada waktu-waktu tertentu saya merasa tidak berguna
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t32" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t32" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>33</th>
                            <td>Saya merasa sukar untuk memusatkan perhatian saya
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t33" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t33" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>34</th>
                            <td>Saya adalah seseorang yang menganggap bahwa segala sesuatu berat
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t34" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t34" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>35</th>
                            <td>Saya adalah seorang yang sering gugup
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t35" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t35" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>36</th>
                            <td>Hidup sering merupakan beban bagi saya
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t36" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t36" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>37</th>
                            <td>Pada waktu-waktu tertentu saya merasa bahwa saya orang yang sama sekali tidak berguna
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t37" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t37" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>38</th>
                            <td>Saya benar-benar tidak percaya pada diri sendiri
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t38" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t38" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>39</th>
                            <td>Pada waktu-waktu tertentu saya merasa hancur
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t39" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t39" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>40</th>
                            <td>Saya tidak suka untuk menghadapi kesukaran atau membuat keputusan yang penting
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="t40" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="t40" value="0"></label></td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="tab">HARS
                <table class="table table-striped mb-0">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Kondisi</th>
                            <th scope="col">0</th>
                            <th scope="col">1</th>
                            <th scope="col">2</th>
                            <th scope="col">3</th>
                            <th scope="col">4</th>
                        </tr>
                    </thead>

                    <tbody>
                        <input type="hidden" name="email" value="{{ Auth::user()->email }}">
                        <tr>
                            <th>1</th>
                            <td>Perasaan cemas firasat buruk, takut akan pikiran sendiri, mudah tensinggung.
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="h1" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h1" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h1" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h1" value="3"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h1" value="4"></label></td>
                        </tr>
                        <tr>
                            <th>2</th>
                            <td>Ketegangan merasa tegang, gelisah, gemetar, mudah terganggu dan lesu.
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="h2" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h2" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h2" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h2" value="3"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h2" value="4"></label></td>

                        </tr>
                        <tr>
                            <th>3</th>
                            <td>Ketakutan: takut terhadap gelap, terhadap orang asing, bila tinggal sendiri dan takut
                                pada binatang besar.
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="h3" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h3" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h3" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h3" value="3"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h3" value="4"></label></td>
                        </tr>
                        <tr>
                            <th>4</th>
                            <td>Gangguan tidur sukar memulai tidur, terbangun pada malam hari, tidur tidak pulas dan
                                mimpi buruk.
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="h4" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h4" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h4" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h4" value="3"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h4" value="4"></label></td>
                        </tr>
                        <tr>
                            <th>5</th>
                            <td>Gangguan kecerdasan: penurunan daya ingat, mudah lupa dan sulit konsentrasi.
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="h5" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h5" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h5" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h5" value="3"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h5" value="4"></label></td>
                        </tr>
                        <tr>
                            <th>6</th>
                            <td>Perasaan depresi: hilangnya minat, berkurangnya kesenangan pada hoby, sedih, perasaan
                                tidak menyenangkan sepanjang hari.
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="h6" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h6" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h6" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h6" value="3"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h6" value="4"></label></td>
                        </tr>
                        <tr>
                            <th>7</th>
                            <td>Gejala somatik: nyeni path otot-otot dan kaku, gertakan gigi, suara tidak stabil dan
                                kedutan otot.
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="h7" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h7" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h7" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h7" value="3"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h7" value="4"></label></td>
                        </tr>
                        <tr>
                            <th>8</th>
                            <td>Gejala sensorik: perasaan ditusuk-tusuk, penglihatan kabur, muka merah dan pucat serta
                                merasa lemah.
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="h8" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h8" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h8" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h8" value="3"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h8" value="4"></label></td>
                        </tr>
                        <tr>
                            <th>9</th>
                            <td>Gejala kardiovaskuler: takikardi, nyeri di dada, denyut nadi mengeras dan detak jantung
                                hilang sekejap.
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="h9" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h9" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h9" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h9" value="3"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h9" value="4"></label></td>
                        </tr>
                        <tr>
                            <th>10</th>
                            <td>Gejala pemapasan: rasa tertekan di dada, perasaan tercekik, sering menarik napas panjang
                                dan merasa napas pendek.
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="h10" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h10" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h10" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h10" value="3"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h10" value="4"></label></td>
                        </tr>
                        <tr>
                            <th>11</th>
                            <td>Gejala gastrointestinal: sulit menelan, obstipasi, berat badan menurun, mual dan muntah,
                                nyeri lambung sebelum dan sesudah makan, perasaan panas di perut.
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="h11" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h11" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h11" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h11" value="3"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h11" value="4"></label></td>
                        </tr>
                        <tr>
                            <th>12</th>
                            <td>Gejala urogenital: sering kencing, tidak dapat menahan kencing, aminorea, ereksi lemah
                                atau impotensi.
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="h12" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h12" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h12" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h12" value="3"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h12" value="4"></label></td>
                        </tr>
                        <tr>
                            <th>13</th>
                            <td>Gejala vegetatif: mulut kering, mudah berkeringat, muka merah, bulu roma berdiri, pusing
                                atau sakit kepala.
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="h13" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h13" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h13" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h13" value="3"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h13" value="4"></label></td>
                        </tr>
                        <tr>
                            <th>14</th>
                            <td>Perilaku sewaktu wawancara: gelisah, jari-jari gemetar, mengkerutkan dahi atau kening,
                                muka tegang, tonus otot meningkat dan napas pendek dan cepat.
                            </td>

                            <td><label class="radio-inline"><input type="radio" name="h14" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h14" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h14" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h14" value="3"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="h14" value="4"></label></td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="tab">HOLMES
                <table class="table table-striped mb-0">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Kondisi</th>
                            <th scope="col">Y</th>
                            <th scope="col">T</th>
                        </tr>
                    </thead>

                    <tbody>
                        <input type="hidden" name="email" value="{{ Auth::user()->email }}">
                        <tr>
                            <th>1</th>
                            <td>Kematian suami/istri
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho1" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho1" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>2</th>
                            <td>Perceraian
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho2" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho2" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>3</th>
                            <td>Pemisahan perkawinan
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho3" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho3" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>4</th>
                            <td>Penjara
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho4" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho4" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>5</th>
                            <td>Kematian anggota keluarga dekat
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho5" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho5" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>6</th>
                            <td>Cedera atau sakit
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho6" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho6" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>7</th>
                            <td>Pernikahan
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho7" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho7" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>8</th>
                            <td>Dipecat dari tempat kerja
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho8" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho8" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>9</th>
                            <td>Perkawinan rekonsiliasi
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho9" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho9" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>10</th>
                            <td>Pensiun
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho10" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho10" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>11</th>
                            <td>Perubahan kesehatan anggota keluarga
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho11" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho11" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>12</th>
                            <td>Kehamilan
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho12" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho12" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>13</th>
                            <td>Kesulitan seks
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho13" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho13" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>14</th>
                            <td>Kehadiran anggota keluarga baru
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho14" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho14" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>15</th>
                            <td>Penyesuaian bisnis
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho15" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho15" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>16</th>
                            <td>Perubahan dalam status keuangan
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho16" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho16" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>17</th>
                            <td>Kematian teman dekat
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho17" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho17" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>18</th>
                            <td>Mutasi pekerjaan
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho18" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho18" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>19</th>
                            <td>Konflik dengan pasangan
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho19" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho19" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>20</th>
                            <td>Banyak hutang
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho20" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho20" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>21</th>
                            <td>Penyitaan harta
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho21" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho21" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>22</th>
                            <td>Perubahan tanggung jawab di tempat kerja
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho22" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho22" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>23</th>
                            <td>Anak meninggalkan rumah
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho23" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho23" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>24</th>
                            <td>Masalah dengan mertua
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho24" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho24" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>25</th>
                            <td>Posisi prestasi pribadi
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho25" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho25" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>26</th>
                            <td>Pasangan mulai atau berhenti bekerja
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho26" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho26" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>27</th>
                            <td>Memulai atau mengakhiri sekolah/kuliah
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho27" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho27" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>28</th>
                            <td>Perubahan kondisi hidup
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho28" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho28" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>29</th>
                            <td>Perubahan kebiasaan pribadi
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho29" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho29" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>30</th>
                            <td>Masalah dengan atasan
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho30" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho30" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>31</th>
                            <td>Perubahan kondisi atau jam kerja
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho31" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho31" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>32</th>
                            <td>Perubahan tempat tinggal
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho32" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho32" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>33</th>
                            <td>Perubahan di sekolah / perguruan tinggi
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho33" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho33" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>34</th>
                            <td>Perubahan rekreasi
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho34" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho34" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>35</th>
                            <td>Perubahan kegiatan ibadah
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho35" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho35" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>36</th>
                            <td>Perubahan kegiatan sosial
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho36" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho36" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>37</th>
                            <td>Hutang dalam jumlah kecil
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho37" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho37" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>38</th>
                            <td>Perubahan kebiasaan tidur
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho38" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho38" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>39</th>
                            <td>Perubahan kebiasaan silaturahmi keluarga
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho39" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho39" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>40</th>
                            <td>Perubahan kebiasaan makan
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho40" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho40" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>41</th>
                            <td>Liburan
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho41" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho41" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>42</th>
                            <td>Hari raya
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho42" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho42" value="0"></label></td>
                        </tr>
                        <tr>
                            <th>43</th>
                            <td>Pelanggaran ringan
                            </td>
                            <input type="hidden" name="id_dass" value="">
                            <td><label class="radio-inline"><input type="radio" name="ho43" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="ho43" value="0"></label></td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="tab">Kuis BDI
                <table class="table table-striped mb-0">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col"></th>
                            <th scope="col">Kondisi</th>
                            <th scope="col">0</th>
                            <th scope="col">1</th>
                            <th scope="col">2</th>
                            <th scope="col">3</th>
                        </tr>
                    </thead>

                    <tbody>
                        <input type="hidden" name="email" value="{{ Auth::user()->email }}">
                        <tr>
                            <th>1</th>
                            <td>Kesedihan
                            </td>
                            <td>
                                Saya tidak merasa sedih
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k1" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k1" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k1" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k1" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya merasa sedih
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k2" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k2" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k2" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k2" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya merasa sedih sepanjang waktu dan saya tidak dapat menghilangkannya
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k3" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k3" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k3" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k3" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya begitu sedih sehingga saya merasa tidak tahan lagi
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k4" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k4" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k4" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k4" value="3"></label></td>
                        </tr>
                        <tr>
                            <th>2</th>
                            <td>Pesimis
                            </td>
                            <td>
                                Saya tidak merasa berkecil hati terhadap masa depan
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k5" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k5" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k5" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k5" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya merasa berkecil hati terhadap masa depan
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k6" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k6" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k6" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k6" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya merasa tidak ada sesuatu yang saya nantikan
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k7" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k7" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k7" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k7" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya merasa bahwa tidak ada harapan di masa depan dan segala sesuatunya tidak dapat
                                diperbaiki
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k8" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k8" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k8" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k8" value="3"></label></td>
                        </tr>
                        <th>3</th>
                        <td>Kegagalan
                        </td>
                        <td>
                            Saya tidak merasa gagal
                        </td>
                        <td><label class="radio-inline"><input type="radio" name="k9" value="0"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k9" value="1"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k9" value="2"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k9" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya merasa lebih banyak mengalami kegagalan daripada rata – rata orang
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k10" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k10" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k10" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k10" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Kalau saya meninjau kembali hidup saya, yang dapat saya lihat hanyalah banyak kegagalan
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k11" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k11" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k11" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k11" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya merasa sebagai seorang pribadi yang gagal total
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k12" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k12" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k12" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k12" value="3"></label></td>
                        </tr>
                        <th>4</th>
                        <td>Kehilangan Kesenangan
                        </td>
                        <td>
                            Saya memperoleh kepuasan atas segala sesuatu seperti biasanya
                        </td>
                        <td><label class="radio-inline"><input type="radio" name="k13" value="0"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k13" value="1"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k13" value="2"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k13" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya tidak dapat menikmati segala sesuatu seperti biasanya
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k14" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k14" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k14" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k14" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya tidak lagi memperoleh kepuasan yang nyata dari segala sesuatu
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k15" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k15" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k15" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k15" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya merasa tidak puas atau bosan terhadap apa saja
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k16" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k16" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k16" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k16" value="3"></label></td>
                        </tr>
                        <th>5</th>
                        <td>Perasaan Bersalah
                        </td>
                        <td>
                            Saya tidak merasa bersalah
                        </td>
                        <td><label class="radio-inline"><input type="radio" name="k17" value="0"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k17" value="1"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k17" value="2"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k17" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya cukup sering merasa bersalah
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k18" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k18" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k18" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k18" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya sering merasa sangat bersalah
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k19" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k19" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k19" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k19" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya merasa bersalah sepanjang waktu
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k20" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k20" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k20" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k20" value="3"></label></td>
                        </tr>
                        <th>6</th>
                        <td>Perasaan akan Hukuman
                        </td>
                        <td>
                            Saya tidak merasa bahwa saya sedang dihukum
                        </td>
                        <td><label class="radio-inline"><input type="radio" name="k21" value="0"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k21" value="1"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k21" value="2"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k21" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya merasa bahwa saya mungkin dihukum
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k22" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k22" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k22" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k22" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya mengharapkan agar dihukum
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k23" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k23" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k23" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k23" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya merasa bahwa saya sedang dihukum
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k24" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k24" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k24" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k24" value="3"></label></td>
                        </tr>
                        <th>7</th>
                        <td>Tidak Menyukai Diri Sendiri
                        </td>
                        <td>
                            Saya tidak merasa kecewa terhadap diri saya sendiri
                        </td>
                        <td><label class="radio-inline"><input type="radio" name="k25" value="0"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k25" value="1"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k25" value="2"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k25" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya merasa kecewa terhadap diri saya sendiri
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k26" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k26" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k26" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k26" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya merasa jijik terhadap diri saya sendiri
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k27" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k27" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k27" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k27" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya membenci diri saya sendiri
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k28" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k28" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k28" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k28" value="3"></label></td>
                        </tr>
                        <th>8</th>
                        <td>Mengkritik Diri Sendiri
                        </td>
                        <td>
                            Saya tidak merasa bahwa saya lebih buruk daripada orang lain
                        </td>
                        <td><label class="radio-inline"><input type="radio" name="k29" value="0"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k29" value="1"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k29" value="2"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k29" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya selalu mencela diri saya sendiri karena kelemahan atau kekeliruan saya
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k30" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k30" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k30" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k30" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya menyalahkan diri saya sendiri sepanjang waktu atas kesalahan – kesalahan saya
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k31" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k31" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k31" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k31" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya menyalahkan diri saya sendiri atas semua hal buruk yang terjadi
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k32" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k32" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k32" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k32" value="3"></label></td>
                        </tr>
                        <th>9</th>
                        <td>Pikiran atau Keinginan Bunuh Diri
                        </td>
                        <td>
                            Saya tidak mempunyai pikiran untuk bunuh diri
                        </td>
                        <td><label class="radio-inline"><input type="radio" name="k33" value="0"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k33" value="1"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k33" value="2"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k33" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya mempunyai pikiran – pikiran untuk bunuh diri, tetapi saya tidak akan
                                melaksanakannya
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k34" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k34" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k34" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k34" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya ingin bunuh diri
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k35" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k35" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k35" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k35" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya akan bunuh diri kalau ada kesempatan
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k36" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k36" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k36" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k36" value="3"></label></td>
                        </tr>
                        <th>10</th>
                        <td>Menangis
                        </td>
                        <td>
                            Saya tidak menangis lebih dari biasanya
                        </td>
                        <td><label class="radio-inline"><input type="radio" name="k37" value="0"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k37" value="1"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k37" value="2"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k37" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Sekarang saya lebih banyak menangis daripada biasanya
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k38" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k38" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k38" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k38" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Sekarang saya menangis sepanjang waktu
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k39" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k39" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k39" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k39" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya biasanya dapat menangis, tetapi sekarang saya tidak dapat menangis meskipun saya
                                ingin menangis
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k40" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k40" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k40" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k40" value="3"></label></td>
                        </tr>
                        <th>11</th>
                        <td>Pergolakan dalam Diri
                        </td>
                        <td>
                            Sekarang saya tidak merasa lebih jengkel daripada sebelumnya
                        </td>
                        <td><label class="radio-inline"><input type="radio" name="k41" value="0"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k41" value="1"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k41" value="2"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k41" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya lebih mudah jengkel atau marah daripada biasanya
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k42" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k42" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k42" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k42" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya sekarang merasa jengkel sepanjang waktu
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k43" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k43" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k43" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k43" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya tidak dibuat jengkel oleh hal – hal yang biasanya menjengkelkan saya
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k44" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k44" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k44" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k44" value="3"></label></td>
                        </tr>
                        <th>12</th>
                        <td>Kehilangan Minat
                        </td>
                        <td>
                            Saya masih tetap senang bergaul dengan orang lain
                        </td>
                        <td><label class="radio-inline"><input type="radio" name="k45" value="0"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k45" value="1"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k45" value="2"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k45" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya kurang berminat pada orang lain dibandingkan dengan biasanya
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k46" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k46" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k46" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k46" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya tak kehilangan sebagian besar minat saya terhadap orang lain
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k47" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k47" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k47" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k47" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya telah kehilangan seluruh minat saya terhadap orang lain
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k48" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k48" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k48" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k48" value="3"></label></td>
                        </tr>
                        <th>13</th>
                        <td>Bimbang/ragu-ragu
                        </td>
                        <td>
                            Saya mengambil keputusan – keputusan sama baiknya dengan sebelumnya
                        </td>
                        <td><label class="radio-inline"><input type="radio" name="k49" value="0"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k49" value="1"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k49" value="2"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k49" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya lebih banyak menunda keputusan daripada biasanya
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k50" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k50" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k50" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k50" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya mempunyai kesulitan yang lebih besar dalam mengambil keputusan daripada sebelumnya
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k51" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k51" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k51" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k51" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya sama sekali tidak dapat mengambil keputusan apa pun
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k52" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k52" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k52" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k52" value="3"></label></td>
                        </tr>
                        <th>14</th>
                        <td>Perasaan Tidak Berharga
                        </td>
                        <td>
                            Saya tidak merasa bahwa saya kelihatan lebih jelek daripada sebelumnya
                        </td>
                        <td><label class="radio-inline"><input type="radio" name="k53" value="0"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k53" value="1"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k53" value="2"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k53" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya merasa cemas jangan – jangan saya tua atau tidak menarik
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k54" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k54" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k54" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k54" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya merasa bahwa ada perubahan – perubahan tetap pada penampilan saya yang membuat saya
                                kelihatan tidak menarik
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k55" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k55" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k55" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k55" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya yakin bahwa saya kelihatan jelek
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k56" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k56" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k56" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k56" value="3"></label></td>
                        </tr>
                        <th>15</th>
                        <td>Kehilangan Energi
                        </td>
                        <td>
                            Saya dapat bekerja dengan baik seperti sebelumnya
                        </td>
                        <td><label class="radio-inline"><input type="radio" name="k57" value="0"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k57" value="1"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k57" value="2"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k57" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya membutuhkan usaha istimewa untuk mulai mengerjakan sesuatu
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k58" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k58" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k58" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k58" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya harus memaksa diri saya untuk mengerjakan sesuatu
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k59" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k59" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k59" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k59" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya sama sekali tidak dapat mengerjakan apa – apa
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k60" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k60" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k60" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k60" value="3"></label></td>
                        </tr>
                        <th>16</th>
                        <td>Kehilangan Pola Tidur
                        </td>
                        <td>
                            Saya dapat tidur nyenyak seperti biasanya
                        </td>
                        <td><label class="radio-inline"><input type="radio" name="k61" value="0"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k61" value="1"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k61" value="2"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k61" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya lebih mudah lelah dari biasanya
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k62" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k62" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k62" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k62" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya bangun 2-3 jam lebih awal dari biasanya dan sukar tidur kembali
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k63" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k63" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k63" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k63" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya bangun beberapa jam lebih awal daripada biasanya dan tidak dapat tidur kembali
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k64" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k64" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k64" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k64" value="3"></label></td>
                        </tr>
                        <th>17</th>
                        <td>Cepat Marah
                        </td>
                        <td>
                            Saya tidak lebih lelah dari biasanya
                        </td>
                        <td><label class="radio-inline"><input type="radio" name="k65" value="0"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k65" value="1"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k65" value="2"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k65" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya lebih mudah lelah dari biasanya
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k66" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k66" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k66" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k66" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya hampir selalu merasa lelah dalam mengerjakan segala sesuatu
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k67" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k67" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k67" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k67" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya merasa terlalu lelah untuk mengerjakan apa saja
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k68" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k68" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k68" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k68" value="3"></label></td>
                        </tr>
                        <th>18</th>
                        <td>Perubahan Nafsu Makan
                        </td>
                        <td>
                            Nafsu makan saya masih seperti biasanya
                        </td>
                        <td><label class="radio-inline"><input type="radio" name="k69" value="0"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k69" value="1"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k69" value="2"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k69" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Nafsu makan saya tidak sebesar biasanya
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k70" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k70" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k70" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k70" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Sekarang nafsu makan saya jauh lebih berkurang
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k71" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k71" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k71" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k71" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya tidak mempunyai nafsu makan sama sekali
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k72" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k72" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k72" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k72" value="3"></label></td>
                        </tr>
                        <th>19</th>
                        <td>Kesulitan Konsentrasi
                        </td>
                        <td>
                            Saya tidak banyak kehilangan berat badan akhir - akhir ini
                        </td>
                        <td><label class="radio-inline"><input type="radio" name="k73" value="0"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k73" value="1"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k73" value="2"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k73" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya telah kehilangan berat badan 2,5 kg lebih
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k74" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k74" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k74" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k74" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya telah kehilangan berat badan 5 kg lebih
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k75" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k75" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k75" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k75" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya telah kehilangan berat badan 7,5 kg lebih. Saya sengaja berusaha mengurangi berat
                                badan dengan makan lebih sedikit :- ya – tidak
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k76" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k76" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k76" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k76" value="3"></label></td>
                        </tr>
                        <th>20</th>
                        <td>Keletihan atau Kelelahan
                        </td>
                        <td>
                            Saya tidak mencemaskan kesehatan saya melebihi biasanya
                        </td>
                        <td><label class="radio-inline"><input type="radio" name="k77" value="0"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k77" value="1"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k77" value="2"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k77" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya cemas akan masalah kesehatan fisik saya, seperti sakit dan rasa nyeri; sakit perut;
                                ataupun sembelit
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k78" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k78" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k78" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k78" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya sangat cemas akan masalah kesehatan fisik saya dan sulit memikirkan hal – hal
                                lainnya
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k79" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k79" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k79" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k79" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya begitu cemas akan kesehatan fisik saya sehingga saya tidak dapat berpikir mengenai
                                hal – hal lainnya
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k80" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k80" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k80" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k80" value="3"></label></td>
                        </tr>
                        <th>21</th>
                        <td>Kehilangan Ketertarikan akan Seks
                        </td>
                        <td>
                            Saya tidak merasa ada perubahan dalam minat saya terhadap seks pada  akhir – akhir ini
                        </td>
                        <td><label class="radio-inline"><input type="radio" name="k81" value="0"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k81" value="1"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k81" value="2"></label></td>
                        <td><label class="radio-inline"><input type="radio" name="k81" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya kurang berminat terhadap seks kalau dibandingkan dengan biasanya
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k82" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k82" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k82" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k82" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Sekarang saya sangat kurang berminat terhadap seks
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k83" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k83" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k83" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k83" value="3"></label></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                Saya sama sekali kehilangan minat terhadap seks
                            </td>
                            <td><label class="radio-inline"><input type="radio" name="k84" value="0"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k84" value="1"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k84" value="2"></label></td>
                            <td><label class="radio-inline"><input type="radio" name="k84" value="3"></label></td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div style="overflow:auto;">
                <div style="float:right;">
                    <button type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
                    <button type="button" id="nextBtn" onclick="nextPrev(1)">Next</button>
                </div>
            </div>

            <!-- Circles which indicates the steps of the form: -->
            <div style="text-align:center;margin-top:40px;">
                <span class="step"></span>
                <span class="step"></span>
                <span class="step"></span>
                <span class="step"></span>
                <span class="step"></span>
                <span class="step"></span>
            </div>

        </form>
        <div class="form-group mb-0 d-flex">
            <button type="submit" class="btn btn-primary ml-auto">Submit</button>
        </div>
    </form>
    {{-- <input type="text" name="id_user" value="1">
        <input type="text" name="id_dass" value="1">
        <input type="text" name="nilai_dass" value="1"> --}}


</div>

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css" />
<link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css') }}">
<style>
/* Style the form */
#regForm {
    background-color: #ffffff;
    margin: 100px auto;
    padding: 40px;
    width: 70%;
    min-width: 300px;
}



/* Hide all steps by default: */
/* .tab {
    display: none;
} */

/* Make circles that indicate the steps of the form: */
.step {
    height: 15px;
    width: 15px;
    margin: 0 2px;
    background-color: #bbbbbb;
    border: none;
    border-radius: 50%;
    display: inline-block;
    opacity: 0.5;
}

/* Mark the active step: */
.step.active {
    opacity: 1;
}

/* Mark the steps that are finished and valid: */
.step.finish {
    background-color: #4CAF50;
}
</style>
@endpush

@push('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
<script src="{{ asset('js/sweetalert2.min.js') }}"></script>
<script>
var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
    // This function will display the specified tab of the form ...
    var x = document.getElementsByClassName("tab");
    x[n].style.display = "block";
    // ... and fix the Previous/Next buttons:
    if (n == 0) {
        document.getElementById("prevBtn").style.display = "none";
    } else {
        document.getElementById("prevBtn").style.display = "inline";
    }
    if (n == (x.length - 1)) {
        document.getElementById("nextBtn").innerHTML = "Submit";
    } else {
        document.getElementById("nextBtn").innerHTML = "Next";
    }
    // ... and run a function that displays the correct step indicator:
    fixStepIndicator(n)
}

function nextPrev(n) {
    // This function will figure out which tab to display
    var x = document.getElementsByClassName("tab");
    // Exit the function if any field in the current tab is invalid:
    if (n == 1 && !validateForm()) return false;
    // Hide the current tab:
    x[currentTab].style.display = "none";
    // Increase or decrease the current tab by 1:
    currentTab = currentTab + n;
    // if you have reached the end of the form... :
    if (currentTab >= x.length) {
        //...the form gets submitted:
        document.getElementById("regForm").submit();
        return false;
    }
    // Otherwise, display the correct tab:
    showTab(currentTab);
}

function validateForm() {
    // This function deals with validation of the form fields
    var x, y, i, valid = true;
    x = document.getElementsByClassName("tab");
    y = x[currentTab].getElementsByTagName("input");
    // A loop that checks every input field in the current tab:
    for (i = 0; i < y.length; i++) {
        // If a field is empty...
        if (y[i].value == "") {
            // add an "invalid" class to the field:
            y[i].className += " invalid";
            // and set the current valid status to false:
            valid = false;
        }
    }
    // If the valid status is true, mark the step as finished and valid:
    if (valid) {
        document.getElementsByClassName("step")[currentTab].className += " finish";
    }
    return valid; // return the valid status
}

function fixStepIndicator(n) {
    // This function removes the "active" class of all steps...
    var i, x = document.getElementsByClassName("step");
    for (i = 0; i < x.length; i++) {
        x[i].className = x[i].className.replace(" active", "");
    }
    //... and adds the "active" class to the current step:
    x[n].className += " active";
}
</script>
@endpush