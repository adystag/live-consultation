@extends('layouts.app')

@section('main')
<div class="p-3 rounded shadow-sm bg-white">
    <h3 class="mb-3">Manage Users</h3>
    
    @if (session('status'))
    <div class="alert alert-{{ session('status') }} alert-dismissible fade show mb-3" role="alert">
        {{ session('status-message') }}
        
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    
    <table id="users-table" class="table table-striped">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">First Name</th>
                <th scope="col">Last Name</th>
                <th scope="col">Email</th>
                <th scope="col">Role</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        
        <tbody>
            @foreach ($users as $key => $user)
            <tr>
                <th>{{ $key + 1}}</th>
                <td>{{ $user->first_name }}</td>
                <td>{{ $user->last_name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->role ? $user->role->name : 'user' }}</td>
                <td>
                    <button class="btn btn-warning btn-sm" onclick="setRole({{ $user->id }}, {{ $user->role ? $user->role->id : ''}})">Set Role</button>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    
    <form id="update-user-role-form" method="POST">
        @csrf
        @method('PUT')
        <input type="hidden" name="role_id" />
    </form>
</div>
@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
<link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css') }}">
@endpush

@push('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
<script src="{{ asset('js/sweetalert2.min.js') }}"></script>
<script>
    $(document).ready(function () {
        $('#users-table').DataTable();
    })
    
    function setRole (id, role) {
        var url = '{!! url("/users") !!}'
        
        swal({
            title: 'Set User Role',
            input: 'select',
            inputClass: 'custom-select custom-select-lg',
            inputOptions: {
                @foreach ($roles as $role)
                {!! "'{$role->id}' : '{$role->name}'," !!}
                @endforeach
            },
            inputValue: role ? role : '',
            inputPlaceholder: 'Select Role',
            showCancelButton: true,
            confirmButtonText: 'Save'
        })
        .then(result => {
            if (!result.dismiss) {
                $('#update-user-role-form > input[name=role_id]').val(result.value)
                $('#update-user-role-form').attr('action', url + '/' + id + '/roles')
                $('#update-user-role-form').submit()
            }
        })
    }
</script>
@endpush