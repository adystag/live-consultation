@extends('layouts.app')

@section('main')
<div class="card">
    <div class="card-header">Setting</div>

    <div class="card-body">
        @if (session('status'))
        <div class="alert alert-{{ session('status') }}" role="alert">
            {{ session('status-message') }}
        </div>
        @endif

    <form action="{{route('setting.update', $point->id)}}" method="post">
      @csrf
      <input type="hidden" name="_method" value="put">
      Point<br>
      <input type="number" name="point" placeholder="Input Point" value="{{$point->setting_value}}">
      <br><br>

      <input type="submit" value="Submit">
    </form>

    </div>
</div>
@endsection
