@extends('layouts.app')

@section('main')
<div class="p-3 rounded shadow-sm bg-white">
        <nav class="nav nav-pills nav-justified">
                <a class="nav-item nav-link  active" href="/kuisioner/bdi">BDI</a>
                <a class="nav-item nav-link" href="/kuisioner/kuisbdi">Kuis BDI</a>
              </nav>
              <hr>
    <h3 class="mb-3">Beck Depression Inventory (BDI)
        </h3>
    
    @if (session('status'))
    <div class="alert alert-{{ session('status') }} alert-dismissible fade show mb-3" role="alert">
        {{ session('status-message') }}
        
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <form action="{{ route('tambahdatabdi')}}" method="POST">
            @csrf
    <table class="table table-striped mb-0">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Item</th>
                <th scope="col">Kondisi</th>
                <th scope="col">0</th>
                <th scope="col">1</th>
                <th scope="col">2</th>
                <th scope="col">3</th>
            </tr>
        </thead>
        
        <tbody>
                <input type="hidden" name="email" value="{{ Auth::user()->email }}">
                <tr>
                    <th>1</th>
                    <td>Kesedihan
                    </td>
                    <td>Saya begitu sedih sehingga saya merasa tidak tahan lagi
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d1" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d1" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d1" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d1" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>2</th>
                    <td>Pesimis
                    </td>
                    <td>Saya merasa tidak ada sesuatu yang saya nantikan
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d2" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d2" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d2" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d2" value="3"></label></td>
                </tr>
                <tr>
                    <th>3</th>
                    <td>Kegagalan
                    </td>
                    <td>Saya merasa lebih banyak mengalami kegagalan daripada rata – rata orang
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d3" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d3" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d3" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d3" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>4</th>
                    <td>Kehilangan Kesenangan
                    </td>
                    <td>Saya tidak dapat menikmati segala sesuatu seperti biasanya
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d4" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d4" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d4" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d4" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>5</th>
                    <td>Perasaan Bersalah
                    </td>
                    <td>Saya cukup sering merasa bersalah
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d5" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d5" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d5" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d5" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>6</th>
                    <td>Perasaan akan Hukuman
                    </td>
                    <td>Saya merasa bahwa saya sedang dihukum
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d6" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d6" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d6" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d6" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>7</th>
                    <td>Tidak Menyukai Diri Sendiri
                    </td>
                    <td>Saya tidak merasa kecewa terhadap diri saya sendiri
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d7" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d7" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d7" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d7" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>8</th>
                    <td>Mengkritik Diri Sendiri
                    </td>
                    <td>Saya selalu mencela diri saya sendiri karena kelemahan atau kekeliruan saya
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d8" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d8" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d8" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d8" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>9</th>
                    <td>Pikiran atau Keinginan Bunuh Diri
                    </td>
                    <td>Saya ingin bunuh diri
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d9" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d9" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d9" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d9" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>10</th>
                    <td>Menangis
                    </td>
                    <td>Saya biasanya dapat menangis, tetapi sekarang saya tidak dapat menangis meskipun saya ingin menangis
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d10" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d10" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d10" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d10" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>11</th>
                    <td>Pergolakan dalam Diri
                    </td>
                    <td>Saya tidak dibuat jengkel oleh hal – hal yang biasanya menjengkelkan saya
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d11" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d11" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d11" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d11" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>12</th>
                    <td>Kehilangan Minat
                    </td>
                    <td>Saya masih tetap senang bergaul dengan orang lain
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d12" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d12" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d12" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d12" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>13</th>
                    <td>Bimbang/ragu-ragu
                    </td>
                    <td>Saya lebih banyak menunda keputusan daripada biasanya
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d13" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d13" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d13" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d13" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>14</th>
                    <td>Perasaan Tidak Berharga
                    </td>
                    <td>Saya tidak merasa bahwa saya kelihatan lebih jelek daripada sebelumnya
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d14" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d14" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d14" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d14" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>15</th>
                    <td>Kehilangan Energi
                    </td>
                    <td>Saya membutuhkan usaha istimewa untuk mulai mengerjakan sesuatu
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d15" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d15" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d15" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d15" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>16</th>
                    <td>Kehilangan Pola Tidur
                    </td>
                    <td>Saya bangun beberapa jam lebih awal daripada biasanya dan tidak dapat tidur kembali
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d16" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d16" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d16" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d16" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>17</th>
                    <td>Cepat Marah
                    </td>
                    <td>Saya merasa terlalu lelah untuk mengerjakan apa saja
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d17" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d17" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d17" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d17" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>18</th>
                    <td>Perubahan Nafsu Makan
                    </td>
                    <td>Nafsu makan saya masih seperti biasanya
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d18" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d18" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d18" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d18" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>19</th>
                    <td>Kesulitan Konsentrasi
                    </td>
                    <td>Saya telah kehilangan berat badan 2,5 kg lebih
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d19" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d19" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d19" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d19" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>20</th>
                    <td>Keletihan atau Kelelahan
                    </td>
                    <td>Saya sangat cemas akan masalah kesehatan fisik saya dan sulit memikirkan hal – hal lainnya
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d20" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d20" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d20" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d20" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>21</th>
                    <td>Kehilangan Ketertarikan akan Seks
                    </td>
                    <td>Saya sama sekali kehilangan minat terhadap seks
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d21" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d21" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d21" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d21" value="3"></label></td>
                    
                </tr>
        </tbody>
    </table>
    <br>
    <div class="form-group mb-0 d-flex">
            <button type="submit" class="btn btn-primary ml-auto">Submit</button>
        </div>
</form>
        {{-- <input type="text" name="id_user" value="1">
        <input type="text" name="id_dass" value="1">
        <input type="text" name="nilai_dass" value="1"> --}}
                
                    
</div>

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
<link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css') }}">
@endpush

@push('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
<script src="{{ asset('js/sweetalert2.min.js') }}"></script>
<script>
    $(document).ready(function() {
    $('#table').DataTable( {
        "ordering": false
    } );
} );
    </script>
    @endpush

