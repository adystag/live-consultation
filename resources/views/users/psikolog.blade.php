@extends('layouts.app')

@section('main')
<div class="p-3 rounded shadow-sm bg-white mb-3">

    <h3 class="mb-3">Pilih Psikolog</h3>
    
    @if (session('status'))
    <div class="alert alert-{{ session('status') }} alert-dismissible fade show mb-3" role="alert">
        {{ session('status-message') }}
        
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
            @csrf
    <table class="table table-striped mb-0">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Nama</th>
                <th scope="col">Bio</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        
        <tbody>
                <?php $i=1; ?>
                @foreach($user as $row)
                <tr>
                        <td>{{ $i }}</td>
                        <td>{{ $row->first_name }} {{ $row->last_name }}</td>
                        <td>{{ $row->bio }}</td>
                        <td>
                            @if ($row->id == Auth::user()->psikolog)
                            <button type="button" class="btn btn-success">
                                Dipilih
                              </button>
                            @elseif ($row->id != Auth::user()->psikolog && Auth::user()->psikolog == '')
                              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal<?php echo $row->id ?>">
                                  Pilih
                                </button>
                            @elseif ($row->id != Auth::user()->psikolog)
                                <button type="button" class="btn btn-secondary">
                                    Tidak Dipilih
                                  </button>
                            @endif
                        </td>
                    </tr>
                    <?php $i++; ?>
                @endforeach
                
        </tbody>
    </table>
  </div>

                @if (Auth::user()->psikolog == "")
                
                

                @else
                
                <div class="p-3 rounded shadow-sm bg-white">
                    <h3 class="mb-3">Kuisioner</h3>
                <div class="row">
                    <div class="col-sm-3">
                      <div class="card">
                        <div class="card-body">
                          <h5 class="card-title">DASS 46</h5>
                          <a href="/kuisioner/dass" class="btn btn-primary">Isi Kuisioner</a>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-3">
                      <div class="card">
                        <div class="card-body">
                          <h5 class="card-title">Depresi</h5>
                          
                          <a href="/kuisioner/bdi" class="btn btn-primary">Isi Kuisioner</a>
                        </div>
                      </div>
                    </div>
                      <div class="col-sm-3">
                        <div class="card">
                          <div class="card-body">
                            <h5 class="card-title">Kecemasan</h5>
                            
                            <a href="/kuisioner/tmas" class="btn btn-primary">Isi Kuisioner</a>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="card">
                          <div class="card-body">
                            <h5 class="card-title">Stress</h5>
                            
                            <a href="/kuisioner/holmes" class="btn btn-primary">Isi Kuisioner</a>
                          </div>
                        </div>
                      </div>
                </div>
                @endif
  

        <!-- Modal -->
        <?php foreach ($user as $row):?>
<div class="modal fade" id="exampleModal<?php echo $row->id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Anda yakin memilih Psikolog : </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            
            <form method="post" action="/updatePsikolog/{{ Auth::user()->id }}">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
            <div class="modal-body">
                <p style="color: red;">*Psikolog yang telah dipilih tidak dapat diubah.</p>
              <label>Nama : </label><input class="form-control" value="{{ $row->first_name }} {{ $row->last_name }}" readonly/><br>
              <label>Bio : </label><input class="form-control" value="{{ $row->bio }}" readonly/>
              <input type="hidden" name="psikolog" value="{{ $row->id }}"/>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button  type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </form>
          </div>
        </div>
      </div>
      <?php endforeach; ?>    
                    


@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
<link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css') }}">
@endpush

@push('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
<script src="{{ asset('js/sweetalert2.min.js') }}"></script>
<script>
    $(document).ready(function() {
    $('#table').DataTable( {
        "ordering": false
    } );
} );
    </script>
    @endpush

