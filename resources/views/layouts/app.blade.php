@extends('layouts.shell')

@section('content')
<div id="app" style="height: 100vh;">
    @include('__partials.top')
    
    <div style="height: calc(100vh - 56px);">
        <div class="row no-gutters h-100">
            <div class="col-2 bg-white shadow-sm">
                @include('__partials.side.profile')
                
                @include('__partials.side.menu')
            </div>
            
            <div class="col mh-100" style="padding: 16px; overflow: auto;">
                @yield('main')

                @if (!Request::is('notifications'))
                <notification-display></notification-display>
                @endif
            </div>
            
            {{-- <div class="notification-area col-2 bg-white shadow-sm">
            </div> --}}
        </div>
    </div>
</div>
@endsection

@push('data-scripts')
<script>
    window.Notification = {
        user: {!! json_encode(Auth::user()->toArray()) !!},
        url: "{!! url('/') !!}",
        counter: {!! Auth::user()->unreadNotifications->count() !!}
    }
</script>
@endpush

@push('scripts')
<script>
    window.Echo.join('consultation')
</script>
@endpush