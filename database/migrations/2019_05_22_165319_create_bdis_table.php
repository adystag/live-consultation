<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBdisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bdis', function (Blueprint $table) {
            $table->increments('id_user_bdi');
            $table->string('email');
            $table->integer('d1')->length(1);
            $table->integer('d2')->length(1);
            $table->integer('d3')->length(1);
            $table->integer('d4')->length(1);
            $table->integer('d5')->length(1);
            $table->integer('d6')->length(1);
            $table->integer('d7')->length(1);
            $table->integer('d8')->length(1);
            $table->integer('d9')->length(1);
            $table->integer('d10')->length(1);
            $table->integer('d11')->length(1);
            $table->integer('d12')->length(1);
            $table->integer('d13')->length(1);
            $table->integer('d14')->length(1);
            $table->integer('d15')->length(1);
            $table->integer('d16')->length(1);
            $table->integer('d17')->length(1);
            $table->integer('d18')->length(1);
            $table->integer('d19')->length(1);
            $table->integer('d20')->length(1);
            $table->integer('d21')->length(1);
            $table->integer('total')->length(5)->nullable();
            $table->string('tingkat')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bdies');
    }
}