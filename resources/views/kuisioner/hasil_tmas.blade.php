@extends('layouts.app')

@section('main')
<div class="p-3 rounded shadow-sm bg-white mb-3">
    <h3 class="mb-3">Jawaban Kuisioner</h3>
    
    @if (session('status'))
    <div class="alert alert-{{ session('status') }} alert-dismissible fade show mb-3" role="alert">
        {{ session('status-message') }}
        
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    {{ csrf_field() }}
    {{ method_field('PUT') }}

   
    <table id="table" class="table table-striped mb-0">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Kondisi</th>
                            <th scope="col">Skor</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                            <tr>
                                    <th>1</th>
                                    <td>Saya tidak cepat lelah
                                    </td>
                                    <?php if ($hasil->d1 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td>
                                </tr>
                                <tr>
                                    <th>2</th>
                                    <td>Saya sering mengalami perasaan mual
                                    </td>
                                    <?php if ($hasil->d2 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td>
                                </tr>
                                <tr>
                                    <th>3</th>
                                    <td>Saya sering merasa tegang pada waktu bekerja/beraktifitas
                                    </td>
                                    <?php if ($hasil->d3 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td>
                                </tr>
                                <tr>
                                    <th>4</th>
                                    <td>Saya merasa sukar untuk konsentrasi pada suatu hal
                                    </td>
                                    <?php if ($hasil->d4 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td>
                                </tr>
                                <tr>
                                    <th>5</th>
                                    <td>Saya cemas akan keadaan sakit saya
                                    </td>
                                    <?php if ($hasil->d5 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td>
                                </tr>
                                <tr>
                                    <th>6</th>
                                    <td>Saya sering melihat bahwa tangan saya bergetar apabila saya mencoba mengerjakan sesuatu
                                    </td>
                                    <?php if ($hasil->d6 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td>
                                </tr>
                                <tr>
                                    <th>7</th>
                                    <td>Muka saya sering menjadi merah seperti juga terjadi pada orang lain
                                    </td>
                                    <?php if ($hasil->d7 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td>
                                </tr>
                                <tr>
                                    <th>8</th>
                                    <td>Saya sering merasa khawatir akan kemungkinan terjadinya hal-hal yang tidak menyenangkan pada diri saya
                                    </td>
                                    <?php if ($hasil->d8 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td>
                                </tr>
                                <tr>
                                    <th>9</th>
                                    <td>Saya sering takut bahwa muka saya sering merah (tersipu malu)
                                    </td>
                                    <?php if ($hasil->d9 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td>
                                </tr>
                                <tr>
                                    <th>10</th>
                                    <td>Saya sering mengalami mimpi yang menakutkan pada waktu tidur
                                    </td>
                                    <?php if ($hasil->d10 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td>
                                </tr>
                                <tr>
                                    <th>11</th>
                                    <td>Saya mudah berkeringat meski hari tidak panas
                                    </td>
                                    <?php if ($hasil->d11 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td>
                                </tr>
                                <tr>
                                    <th>12</th>
                                    <td>Kadang-kadang saya merasa kikuk, saya menjadi berkeringat dan sangat mengganggu saya
                                    </td>
                                    <?php if ($hasil->d12 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td>
                                </tr>
                                <tr>
                                    <th>13</th>
                                    <td>Saya jarang merasa jantung berdebar-debar dan saya jarang merasa tersengal-sengal
                                    </td>
                                    <?php if ($hasil->d13 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td>
                                </tr>
                                <tr>
                                    <th>14</th>
                                    <td>Saya setiap saat merasa lapar
                                    </td>
                                    <?php if ($hasil->d14 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td>
                                </tr>
                                <tr>
                                    <th>15</th>
                                    <td>Saya sering mengalami gangguan perut
                                    </td>
                                    <?php if ($hasil->d15 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td>
                                </tr>
                                <tr>
                                    <th>16</th>
                                    <td>Saya sering tidak dapat tidur karena mengkhawatirkan sesuatu
                                    </td>
                                    <?php if ($hasil->d16 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td>
                                </tr>
                                <tr>
                                    <th>17</th>
                                    <td>Tidur saya tidak nyenyak dan sering terganggu
                                    </td>
                                    <?php if ($hasil->d17 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td>
                                </tr>
                                <tr>
                                    <th>18</th>
                                    <td>Saya sering mimpi mengenai hal yang tidak saya ceritakan pada orang lain
                                    </td>
                                    <?php if ($hasil->d18 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td>
                                </tr>
                                <tr>
                                    <th>19</th>
                                    <td>Saya mudah untuk merasa kikuk
                                    </td>
                                    <?php if ($hasil->d19 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td>
                                </tr>
                                <tr>
                                    <th>20</th>
                                    <td>Saya sering menemukan bahwa saya mengkhawatirkan sesuatu
                                    </td>
                                    <?php if ($hasil->d20 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td>
                                </tr>
                                <tr>
                                    <th>21</th>
                                    <td>Saya biasanya tenang dan tidak mudah marah
                                    </td>
                                    <?php if ($hasil->d21 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td>
                                </tr>
                                <tr>
                                    <th>22</th>
                                    <td>Saya mudah menangis
                                    </td>
                                    <?php if ($hasil->d22 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td>
                                </tr>
                                <tr>
                                    <th>23</th>
                                    <td>Saya hampir selalu merasa khawatir mengenai suatu hal atau seseorang
                                    </td>
                                    <?php if ($hasil->d23 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td>
                                </tr>
                                <tr>
                                    <th>24</th>
                                    <td>Saya hampir selalu gembira
                                    </td>
                                    <?php if ($hasil->d24 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td>
                                </tr>
                                <tr>
                                    <th>25</th>
                                    <td>Saya merasa gelisah apabila saya harus menanti
                                    </td>
                                    <?php if ($hasil->d25 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td>
                                </tr>
                                <tr>
                                    <th>26</th>
                                    <td>Pada waktu-waktu tertentu saya gelisah
                                    </td>
                                    <?php if ($hasil->d26 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td>
                                </tr>
                                <tr>
                                    <th>27</th>
                                    <td>Kadang-kadang saya begitu bergelora, sehingga sangat sukar bagi saya untuk tidur
                                    </td>
                                    <?php if ($hasil->d27 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td>
                                </tr>
                                <tr>
                                    <th>28</th>
                                    <td>Saya kadang-kadang merasa bahwa kesukaran-kesukaran menumpuk begitu tinggi sehingga saya tidak dapat mengatasinya
                                    </td>
                                    <?php if ($hasil->d28 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>29</th>
                                    <td>Pada waktu-waktu tertentu saya merasa khawatir tanpa alasan mengenai sesuatu yang sesungguhnya tidak berarti
                                    </td>
                                    <?php if ($hasil->d29 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>30</th>
                                    <td>Apabila dibanding dengan teman-teman saya, saya tidak banyak mempunyai ketakutan-ketakutan seperti mereka
                                    </td>
                                    <?php if ($hasil->d30 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td>
                                </tr>
                                <tr>
                                    <th>31</th>
                                    <td>Saya sering takut terhadap benda-benda atau manusia yang saya tahu tidak akan menyakiti saya
                                    </td>
                                    <?php if ($hasil->d31 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td>
                                </tr>
                                <tr>
                                    <th>32</th>
                                    <td>Pada waktu-waktu tertentu saya merasa tidak berguna
                                    </td>
                                    <?php if ($hasil->d32 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td>
                                </tr>
                                <tr>
                                    <th>33</th>
                                    <td>Saya merasa sukar untuk memusatkan perhatian saya
                                    </td>
                                    <?php if ($hasil->d33 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td>
                                </tr>
                                <tr>
                                    <th>34</th>
                                    <td>Saya adalah seseorang yang menganggap bahwa segala sesuatu berat
                                    </td>
                                    <?php if ($hasil->d34 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td>
                                </tr>
                                <tr>
                                    <th>35</th>
                                    <td>Saya adalah seorang yang sering gugup
                                    </td>
                                    <?php if ($hasil->d35 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td>
                                </tr>
                                <tr>
                                    <th>36</th>
                                    <td>Hidup sering merupakan beban bagi saya
                                    </td>
                                     <?php if ($hasil->d36 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td>
                                </tr>
                                <tr>
                                    <th>37</th>
                                    <td>Pada waktu-waktu tertentu saya merasa bahwa saya orang yang sama sekali tidak berguna
                                    </td>
                                    <?php if ($hasil->d37 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td>
                                </tr>
                                <tr>
                                    <th>38</th>
                                    <td>Saya benar-benar tidak percaya pada diri sendiri
                                    </td>
                                    <?php if ($hasil->d38 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td>
                                </tr>
                                <tr>
                                    <th>39</th>
                                    <td>Pada waktu-waktu tertentu saya merasa hancur
                                    </td>
                                    <?php if ($hasil->d39 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td>
                                </tr>
                                <tr>
                                    <th>40</th>
                                    <td>Saya tidak suka untuk menghadapi kesukaran atau membuat keputusan yang penting
                                    </td>
                                    <?php if ($hasil->d1 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td>
                                </tr>
                    </tbody>
                </table>
                <br>
                <hr>
            </div>

            
                    
                    @if (!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant'))
                    <div class="p-3 rounded shadow-sm bg-white">
                            <h3 class="mb-3">Hasil Kuisioner</h3>
                        <table class="table table-striped mb-0">  
                                <thead>
                                        <tr>
                                            <th scope="col"></th>
                                            <th scope="col">Skor</th>
                                            <th scope="col">Tingkat</th>
                                        </tr>
                                    </thead>                 
                                <tbody>
                                        <tr>
                                            <td>Kecemasan</td>
                                            <td>{{ $hasil->total }}</td>
                                            <td>{{ $hasil->tingkat }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                    <div class="p-3 rounded shadow-sm bg-white mb-3">
                            <form method="post" action="/kuisioner/updateTmas/{{ $hasil->id }}">

                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <h3 class="mb-3">Keterangan</h3>
                                <textarea class="form-control" name="penjelasan" >{{ $hasil->penjelasan }}</textarea>
                        <input type="hidden" class="form-control" name="status" value="Selesai di Analisis"/>
                        <br>

                        <div class="form-group mb-0 d-flex">
                                <button type="submit" class="btn btn-primary ml-auto">Submit</button>
                            </div>
                        </form>
                    </div>

                    
            @elseif (!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user' && $hasil->status == 'Selesai di Analisis'))
            <div class="p-3 rounded shadow-sm bg-white">
                    <h3 class="mb-3">Hasil Kuisioner</h3>
                <table class="table table-striped mb-0">  
                        <thead>
                                <tr>
                                    <th scope="col"></th>
                                    <th scope="col">Skor</th>
                                    <th scope="col">Tingkat</th>
                                </tr>
                            </thead>                 
                        <tbody>
                                <tr>
                                    <td>Kecemasan</td>
                                    <td>{{ $hasil->total }}</td>
                                    <td>{{ $hasil->tingkat }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    
            <div class="p-3 rounded shadow-sm bg-white mb-3">
                        <h3 class="mb-3">Keterangan</h3>
                        <textarea class="form-control" name="penjelasan" >{{ $hasil->penjelasan }}</textarea>
                <input type="hidden" class="form-control" name="status" value="Selesai di Analisis"/>
                <br>

            @endif

          
@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
<link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css') }}">
@endpush

@push('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
<script src="{{ asset('js/sweetalert2.min.js') }}"></script>
<script>
    $(document).ready(function() {
    $('#table').DataTable( {
        "ordering": false
    } );
} );
    </script>
    @endpush

