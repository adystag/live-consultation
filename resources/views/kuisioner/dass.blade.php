@extends('layouts.app')

@section('main')
<div class="p-3 rounded shadow-sm bg-white">
    <h3 class="mb-3">DASS</h3>
    
    @if (session('status'))
    <div class="alert alert-{{ session('status') }} alert-dismissible fade show mb-3" role="alert">
        {{ session('status-message') }}
        
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <form action="{{ route('tambahdatadass')}}" method="POST">
            @csrf
    <table class="table table-striped mb-0">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Kondisi</th>
                <th scope="col">0</th>
                <th scope="col">1</th>
                <th scope="col">2</th>
                <th scope="col">3</th>
            </tr>
        </thead>
        
        <tbody>
                <input type="hidden" name="email" value="{{ Auth::user()->email }}">
                <tr>
                    <th>1</th>
                    <td>Menjadi marah karena hal-hal kecil/sepele
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d1" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d1" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d1" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d1" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>2</th>
                    <td>Mulut terasa kering
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d2" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d2" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d2" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d2" value="3"></label></td>
                </tr>
                <tr>
                    <th>3</th>
                    <td>Tidak dapat melihat hal yang positif dari suatu kejadian
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d3" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d3" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d3" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d3" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>4</th>
                    <td>Merasakan gangguan dalam bernafas (nafas cepat, sulit bernafas)
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d4" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d4" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d4" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d4" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>5</th>
                    <td>Merasa tidak kuat lagi untuk melakukan kegiatan
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d5" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d5" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d5" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d5" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>6</th>
                    <td>Cenderung bereaksi berlebihan terhadap suatu situasi
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d6" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d6" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d6" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d6" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>7</th>
                    <td>Kelemahan pada anggota tubuh
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d7" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d7" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d7" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d7" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>8</th>
                    <td>Kesulitan untuk berelaksasi/bersantai
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d8" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d8" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d8" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d8" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>9</th>
                    <td>Cemas berlebihan dalam suatu situasi namun bisa lega bila situasi/hal itu berakhir
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d9" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d9" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d9" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d9" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>10</th>
                    <td>Pesimis
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d10" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d10" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d10" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d10" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>11</th>
                    <td>Mudah merasa kesal
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d11" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d11" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d11" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d11" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>12</th>
                    <td>Merasa banyak menghasilkan energi karena cemas
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d12" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d12" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d12" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d12" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>13</th>
                    <td>Merasa sedih dan depresi
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d13" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d13" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d13" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d13" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>14</th>
                    <td>Tidak sabaran
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d14" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d14" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d14" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d14" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>15</th>
                    <td>Kelelahan
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d15" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d15" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d15" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d15" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>16</th>
                    <td>Kehilangan minat pada banyak hal (misal makan, ambulasi, sosialiasi)
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d16" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d16" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d16" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d16" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>17</th>
                    <td>Merasa diri tidak layak
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d17" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d17" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d17" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d17" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>18</th>
                    <td>Mudah tersinggung
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d18" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d18" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d18" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d18" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>19</th>
                    <td>Berkeringat
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d19" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d19" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d19" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d19" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>20</th>
                    <td>Ketakutan tanpa alasan yang jelas
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d20" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d20" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d20" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d20" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>21</th>
                    <td>Merasa hidup tidak berharga
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d21" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d21" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d21" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d21" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>22</th>
                    <td>Sulit untuk beristirahat
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d22" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d22" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d22" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d22" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>23</th>
                    <td>Kesulitan dalam menelan
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d23" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d23" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d23" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d23" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>24</th>
                    <td>Tidak dapat menikmati hal-hal yang dilakukan
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d24" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d24" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d24" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d24" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>25</th>
                    <td>Perubahan kegiatan jantung dan denyut nadi tanpa stimulasi oleh latihan fisik
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d25" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d25" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d25" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d25" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>26</th>
                    <td>Merasa hilang harapan dan putus asa
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d26" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d26" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d26" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d26" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>27</th>
                    <td>Mudah marah
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d27" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d27" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d27" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d27" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>28</th>
                    <td>Mudah panik
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d28" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d28" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d28" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d28" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>29</th>
                    <td>Kesulitan untuk tenang setelah sesuatu yang mengganggu
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d29" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d29" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d29" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d29" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>30</th>
                    <td>Takut diri terhambat oleh tugas -tugas yang tidak bisa dilakukan
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d30" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d30" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d30" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d30" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>31</th>
                    <td>Sulit untuk antusias pada banyak hal
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d31" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d31" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d31" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d31" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>32</th>
                    <td>Sulit menteloransi gangguan-gangguan terhadap hal-hal yang dilakukan
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d32" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d32" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d32" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d32" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>33</th>
                    <td>Berada pada keadaan tegang
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d33" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d33" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d33" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d33" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>34</th>
                    <td>Berasa tidak berharga
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d34" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d34" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d34" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d34" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>35</th>
                    <td>Tidak dapat memaklumi hal apapun yang menghalangi untuk menyelesaikan hal yang sedang dilakukan
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d35" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d35" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d35" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d35" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>36</th>
                    <td>Ketakutan
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d36" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d36" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d36" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d36" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>37</th>
                    <td>Tidak ada harapan untuk masa depan
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d37" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d37" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d37" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d37" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>38</th>
                    <td>Merasa hidup tidak berarti
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d38" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d38" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d38" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d38" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>39</th>
                    <td>Mudah gelisah
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d39" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d39" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d39" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d39" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>40</th>
                    <td>Khawatir dengan situasi saat diri anda mungkin menjadi panik dan mempermalukan diri anda sendiri
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d40" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d40" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d40" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d40" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>41</th>
                    <td>Gemetar
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d41" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d41" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d41" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d41" value="3"></label></td>
                    
                </tr>
                <tr>
                    <th>42</th>
                    <td>Sulit untuk meningkatkan inisaitif dalam melakukan sesuatu
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d42" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d42" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d42" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d42" value="3"></label></td>
                    
                </tr>
        </tbody>
    </table><br>
    {{-- <div class="form-group">
        <input type="submit" class="btn btn-success" value="Simpan">
    </div>
    <form action="{{ route('tambahdata')}}" method="POST">
            @csrf
            <input type="text" name="id_user" value="1">
            <input type="text" name="id_dass" value="1">
            <input type="text" name="nilai_dass" value="1"> --}}
            <div class="form-group mb-0 d-flex">
                    <button type="submit" class="btn btn-primary ml-auto">Submit</button>
                </div>
    </form>
        
                
                    
</div>

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
<link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css') }}">
@endpush

@push('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
<script src="{{ asset('js/sweetalert2.min.js') }}"></script>
<script>
    $(document).ready(function() {
    $('#table').DataTable( {
        "ordering": false
    } );
} );
    </script>
    @endpush

