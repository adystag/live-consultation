<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Dass;
use App\Models\User;
use App\Models\Holmes;
use App\Models\Kuisbdi;
use App\Models\Tmas;
use App\Models\Hars;
use App\Models\Bdi;
use App\Models\RekapUser;

class KuisionerController extends Controller
{
  public function rekap_user_DassK()
    {
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'admin') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user')){
        $pasien = Dass::all()->where('email',Auth::user()->email);

        return view('kuisioner.user_rekap_dass_k',['pasien' => $pasien]);
      }else{
        echo "Anda tidak berhak mengakses halaman ini";
      }
    }

    public function rekap_user_DassS()
    {
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'admin') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user')){
      $pasien = Dass::all()->where('email',Auth::user()->email);

      return view('kuisioner.user_rekap_dass_s',['pasien' => $pasien]);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
    }

    public function rekap_user_DassD()
    {
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'admin') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user')){
      $pasien = Dass::all()->where('email',Auth::user()->email);

      return view('kuisioner.user_rekap_dass_d',['pasien' => $pasien]);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
    }

    
    public function rekapDassK()
    {
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'admin') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user')){
      $pasien = Dass::all();
      $psikolog = Dass::all()->where('psikolog',Auth::user()->id);
      return view('kuisioner.rekap_dass_k',['pasien' => $pasien],['psikolog' => $psikolog]);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
    }

    public function rekapDassS()
    {
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'admin') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user')){
      $pasien = Dass::all();
      $psikolog = Dass::all()->where('psikolog',Auth::user()->id);
      return view('kuisioner.rekap_dass_s',['pasien' => $pasien],['psikolog' => $psikolog]);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
    }

    public function rekapDassD()
    {
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'admin') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user')){
      $pasien = Dass::all();
      $psikolog = Dass::all()->where('psikolog',Auth::user()->id);
      return view('kuisioner.rekap_dass_d',['pasien' => $pasien],['psikolog' => $psikolog]);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
    }

    public function hasilDass($id)
    {
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'admin') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user')){
    		$dasses = Dass::find($id);
        return view('kuisioner.hasil_dass', ['dasses' => $dasses]);
      }else{
        echo "Anda tidak berhak mengakses halaman ini";
      }
    }

    public function rekap_user_KuisBdi()
    {
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'admin') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user')){
      $pasien = KuisBdi::all()->where('email',Auth::user()->email);

      return view('kuisioner.user_rekap_kuisBdi',['pasien' => $pasien]);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
    }

    public function rekapKuisBdi()
    {
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'admin') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user')){
      $pasien = KuisBdi::all();
      $psikolog = KuisBdi::all()->where('psikolog',Auth::user()->id);
      return view('kuisioner.rekap_kuisBdi',['pasien' => $pasien],['psikolog' => $psikolog]);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
    }

    public function hasilKuisBdi($id)
    {
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'admin') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user')){
    		$hasil = Kuisbdi::find($id);
        return view('kuisioner.hasil_kuisBdi', ['hasil' => $hasil]);
      }else{
        echo "Anda tidak berhak mengakses halaman ini";
      }
    }

    public function rekap_user_Bdi()
    {
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'admin') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user')){
      $pasien = Bdi::all()->where('email',Auth::user()->email);

      return view('kuisioner.user_rekap_bdi',['pasien' => $pasien]);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
    }

    public function rekapBdi()
    {
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'admin') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user')){
      $pasien = Bdi::all();
      $psikolog = Bdi::all()->where('psikolog',Auth::user()->id);
      return view('kuisioner.rekap_bdi',['pasien' => $pasien],['psikolog' => $psikolog]);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
    }

    public function hasilBdi($id)
    {
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'admin') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user')){
    		$hasil = Bdi::find($id);
        return view('kuisioner.hasil_bdi', ['hasil' => $hasil]);
      }else{
        echo "Anda tidak berhak mengakses halaman ini";
      }
    }

    public function rekap_user_Hars()
    {
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'admin') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user')){
      $pasien = Hars::all()->where('email',Auth::user()->email);
      
      return view('kuisioner.user_rekap_hars',['pasien' => $pasien]);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
    }

    public function rekapHars()
    {
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'admin') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user')){
      $pasien = Hars::all();
      $psikolog = Hars::all()->where('psikolog',Auth::user()->id);
      return view('kuisioner.rekap_hars',['pasien' => $pasien],['psikolog' => $psikolog]);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
    }

    public function hasilHars($id)
    {
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'admin') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user')){
    		$hasil = Hars::find($id);
        return view('kuisioner.hasil_hars', ['hasil' => $hasil]);
      }else{
        echo "Anda tidak berhak mengakses halaman ini";
      }
    }

    public function rekap_user_Holmes()
    {
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'admin') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user')){
      $pasien = Holmes::all()->where('email',Auth::user()->email);
      
      return view('kuisioner.user_rekap_Hars',['pasien' => $pasien]);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
    }

    public function rekapHolmes()
    {
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'admin') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user')){
      $pasien = Holmes::all();
      $psikolog = Holmes::all()->where('psikolog',Auth::user()->id);
      return view('kuisioner.rekap_Holmes',['pasien' => $pasien],['psikolog' => $psikolog]);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
    }

    public function hasilHolmes($id)
    {
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'admin') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user')){
    		$hasil = Holmes::find($id);
        return view('kuisioner.hasil_Holmes', ['hasil' => $hasil]);
      }else{
        echo "Anda tidak berhak mengakses halaman ini";
      }
    }

    public function rekap_user_Tmas()
    {
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'admin') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user')){
      $pasien = Tmas::all()->where('email',Auth::user()->email);
      
      return view('kuisioner.user_rekap_Tmas',['pasien' => $pasien]);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
    }

    public function rekapTmas()
    {
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'admin') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user')){
      $pasien = Tmas::all();
      $psikolog = Tmas::all()->where('psikolog',Auth::user()->id);
      return view('kuisioner.rekap_Tmas',['pasien' => $pasien],['psikolog' => $psikolog]);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
    }

    public function hasilTmas($id)
    {
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'admin') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user')){
    		$hasil = Tmas::find($id);
        return view('kuisioner.hasil_Tmas', ['hasil' => $hasil]);
      }else{
        echo "Anda tidak berhak mengakses halaman ini";
      }
    }

    public function rekaphasil()
    {
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'admin') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user')){
      $dass = DB::table('dasses')->join('bdis','dasses.id_user_dass','=','bdis.id_user_bdi')->get();

      return view('kuisioner.rekap_hasil',compact('dass'));
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
    }

    public function kuisioner()
    {
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'admin') || !Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user')){
      return view('kuisioner.kuisioner');
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
    }

    public function tambah_dass(Request $request){
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user')){
      $tambah = new Dass;
      $tambah->email = $request->email;
      $tambah->d1 = $request->d1;
      $tambah->d2 = $request->d2;
      $tambah->d3 = $request->d3;
      $tambah->d4 = $request->d4;
      $tambah->d5 = $request->d5;
      $tambah->d6 = $request->d6;
      $tambah->d7 = $request->d7;
      $tambah->d8 = $request->d8;
      $tambah->d9 = $request->d9;
      $tambah->d10 = $request->d10;
      $tambah->d11 = $request->d11;
      $tambah->d12 = $request->d12;
      $tambah->d13 = $request->d13;
      $tambah->d14 = $request->d14;
      $tambah->d15 = $request->d15;
      $tambah->d16 = $request->d16;
      $tambah->d17 = $request->d17;
      $tambah->d18 = $request->d18;
      $tambah->d19 = $request->d19;
      $tambah->d20 = $request->d20;
      $tambah->d21 = $request->d21;
      $tambah->d22 = $request->d22;
      $tambah->d23 = $request->d23;
      $tambah->d24 = $request->d24;
      $tambah->d25 = $request->d25;
      $tambah->d26 = $request->d26;
      $tambah->d27 = $request->d27;
      $tambah->d28 = $request->d28;
      $tambah->d29 = $request->d29;
      $tambah->d30 = $request->d30;
      $tambah->d31 = $request->d31;
      $tambah->d32 = $request->d32;
      $tambah->d33 = $request->d33;
      $tambah->d34 = $request->d34;
      $tambah->d35 = $request->d35;
      $tambah->d36 = $request->d36;
      $tambah->d37 = $request->d37;
      $tambah->d38 = $request->d38;
      $tambah->d39 = $request->d39;
      $tambah->d40 = $request->d40;
      $tambah->d41 = $request->d41;
      $tambah->d42 = $request->d42;

      $total = $request->d1+$request->d2+$request->d3+$request->d4+$request->d5+$request->d6+$request->d7+$request->d8+$request->d9+$request->d10+$request->d11+$request->d12+$request->d13+$request->d14+$request->d15+$request->d16+$request->d17+$request->d18+$request->d19+$request->d20+$request->d21+$request->d22+$request->d23+$request->d24+$request->d25+$request->d26+$request->d27+$request->d28+$request->d29+$request->d30+$request->d31+$request->d32+$request->d33+$request->d34+$request->d35+$request->d36+$request->d37+$request->d38+$request->d39+$request->d40+$request->d41+$request->d42;
      $tambah->total = $total;

      $d = $request->d3+$request->d5+$request->d10+$request->d13+$request->d16+$request->d17+$request->d21+$request->d24+$request->d26+$request->d31+$request->d34+$request->d37+$request->d38+$request->d42;
      $tambah->depresi = $d;
      $a = $request->d2+$request->d4+$request->d7+$request->d9+$request->d15+$request->d19+$request->d20+$request->d23+$request->d25+$request->d28+$request->d30+$request->d36+$request->d40+$request->d41;
      $tambah->kecemasan = $a;
      $s = $request->d1+$request->d6+$request->d8+$request->d11+$request->d12+$request->d14+$request->d18+$request->d22+$request->d27+$request->d29+$request->d32+$request->d33+$request->d35+$request->d39;
      $tambah->stress = $s;

      if($d <=9){
        $tambah->tingkat_d = 'Normal';
      }elseif ($d <= 13) {
        $tambah->tingkat_d = 'Ringan';
      }elseif ($d <= 20) {
        $tambah->tingkat_d = 'Sedang';
      }elseif ($d <= 27) {
        $tambah->tingkat_d = 'Berat';
      }else{
        $tambah->tingkat_d = 'Sangat Berat';
      }

      if($a <=7){
        $tambah->tingkat_a = 'Normal';
      }elseif ($a <= 9) {
        $tambah->tingkat_a = 'Ringan';
      }elseif ($a <= 14) {
        $tambah->tingkat_a = 'Sedang';
      }elseif ($a <= 19) {
        $tambah->tingkat_a = 'Berat';
      }else{
        $tambah->tingkat_a = 'Sangat Berat';
      }

      if($s <=14){
        $tambah->tingkat_s = 'Normal';
      }elseif ($s <= 18) {
        $tambah->tingkat_s = 'Ringan';
      }elseif ($s <= 25) {
        $tambah->tingkat_s = 'Sedang';
      }elseif ($s <= 33) {
        $tambah->tingkat_s = 'Berat';
      }else{
        $tambah->tingkat_s = 'Sangat Berat';
      }

      $tambah->psikolog = Auth::user()->psikolog;
      $tambah->save();
    	
  
      return redirect('/kuisioner/rekapUserDassD');
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
    }

    public function update_dass($id, Request $request)
    {
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant')){
    		$hasil = Dass::find($id);
    		$hasil->penjelasan = $request->penjelasan;
    		$hasil->status = $request->status;
    		$hasil->save();
        return redirect('/kuisioner/rekapDassD');
      }else{
        echo "Anda tidak berhak mengakses halaman ini";
      }
    }

    public function tambah_bdi(Request $request){
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user')){
      $tambah = new Bdi;
      $tambah->email = $request->email;
      $tambah->d1 = $request->d1;
      $tambah->d2 = $request->d2;
      $tambah->d3 = $request->d3;
      $tambah->d4 = $request->d4;
      $tambah->d5 = $request->d5;
      $tambah->d6 = $request->d6;
      $tambah->d7 = $request->d7;
      $tambah->d8 = $request->d8;
      $tambah->d9 = $request->d9;
      $tambah->d10 = $request->d10;
      $tambah->d11 = $request->d11;
      $tambah->d12 = $request->d12;
      $tambah->d13 = $request->d13;
      $tambah->d14 = $request->d14;
      $tambah->d15 = $request->d15;
      $tambah->d16 = $request->d16;
      $tambah->d17 = $request->d17;
      $tambah->d18 = $request->d18;
      $tambah->d19 = $request->d19;
      $tambah->d20 = $request->d20;
      $tambah->d21 = $request->d21;
      

      $total = $request->d1+$request->d2+$request->d3+$request->d4+$request->d5+$request->d6+$request->d7+$request->d8+$request->d9+$request->d10+$request->d11+$request->d12+$request->d13+$request->d14+$request->d15+$request->d16+$request->d17+$request->d18+$request->d19+$request->d20+$request->d21;
      $tambah->total = $total;
      
      if($total <=10){
        $tambah->tingkat = 'Naik turunnya perasaan ini tergolong wajar';
      }elseif ($total <= 16) {
        $tambah->tingkat = 'Gangguan mood atau perasaan murung yang ringan';
      }elseif ($total <= 20) {
        $tambah->tingkat = 'Garis batas depresi klinis';
      }elseif ($total <= 30) {
        $tambah->tingkat = 'Depresi Sedang';
      }elseif ($total <= 40) {
        $tambah->tingkat = 'Depresi Parah';
      }elseif ($total <= 40) {
        $tambah->tingkat = 'Depresi Ekstrim';
      }else{
        $tambah->tingkat = 'Berat';
      }
      $tambah->psikolog = Auth::user()->psikolog;
      $tambah->save();
  
      return redirect('/kuisioner/rekapUserBdi');
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
    }

    public function update_bdi($id, Request $request)
    {
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant')){
    		$hasil = Bdi::find($id);
    		$hasil->penjelasan = $request->penjelasan;
    		$hasil->status = $request->status;
    		$hasil->save();
        return redirect('/kuisioner/rekapBdi');
      }else{
        echo "Anda tidak berhak mengakses halaman ini";
      }
    }

    public function tambah_tmas(Request $request){
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user')){
      $tambah = new Tmas;
      $tambah->email = $request->email;
      $tambah->d1 = $request->d1;
      $tambah->d2 = $request->d2;
      $tambah->d3 = $request->d3;
      $tambah->d4 = $request->d4;
      $tambah->d5 = $request->d5;
      $tambah->d6 = $request->d6;
      $tambah->d7 = $request->d7;
      $tambah->d8 = $request->d8;
      $tambah->d9 = $request->d9;
      $tambah->d10 = $request->d10;
      $tambah->d11 = $request->d11;
      $tambah->d12 = $request->d12;
      $tambah->d13 = $request->d13;
      $tambah->d14 = $request->d14;
      $tambah->d15 = $request->d15;
      $tambah->d16 = $request->d16;
      $tambah->d17 = $request->d17;
      $tambah->d18 = $request->d18;
      $tambah->d19 = $request->d19;
      $tambah->d20 = $request->d20;
      $tambah->d21 = $request->d21;
      $tambah->d22 = $request->d22;
      $tambah->d23 = $request->d23;
      $tambah->d24 = $request->d24;
      $tambah->d25 = $request->d25;
      $tambah->d26 = $request->d26;
      $tambah->d27 = $request->d27;
      $tambah->d28 = $request->d28;
      $tambah->d29 = $request->d29;
      $tambah->d30 = $request->d30;
      $tambah->d31 = $request->d31;
      $tambah->d32 = $request->d32;
      $tambah->d33 = $request->d33;
      $tambah->d34 = $request->d34;
      $tambah->d35 = $request->d35;
      $tambah->d36 = $request->d36;
      $tambah->d37 = $request->d37;
      $tambah->d38 = $request->d38;
      $tambah->d39 = $request->d39;
      $tambah->d40 = $request->d40;

      $total = $request->d1+$request->d2+$request->d3+$request->d4+$request->d5+$request->d6+$request->d7+$request->d8+$request->d9+$request->d10+$request->d11+$request->d12+$request->d13+$request->d14+$request->d15+$request->d16+$request->d17+$request->d18+$request->d19+$request->d20+$request->d21+$request->d22+$request->d23+$request->d24+$request->d25+$request->d26+$request->d27+$request->d28+$request->d29+$request->d30+$request->d31+$request->d32+$request->d33+$request->d34+$request->d35+$request->d36+$request->d37+$request->d38+$request->d39+$request->d40;
      $tambah->total = $total;
      
      if($total <=14){
        $tambah->tingkat = 'Ringan/Normal';
      }elseif ($total <= 19) {
        $tambah->tingkat = 'Sedang';
      }elseif ($total <= 22) {
        $tambah->tingkat = 'Berat';
      }else{
        $tambah->tingkat = 'Sangat Berat';
      }
      $tambah->psikolog = Auth::user()->psikolog;
      $tambah->save();

      return redirect('/kuisioner/rekapUserTmas');
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
    }

    public function upd()
    {

    		var_dump(Auth::user()->psikolog);
    }

    public function update_tmas($id, Request $request)
    {
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant')){
    		$hasil = Tmas::find($id);
    		$hasil->penjelasan = $request->penjelasan;
    		$hasil->status = $request->status;
    		$hasil->save();
        return redirect('/kuisioner/rekapTmas');
      }else{
        echo "Anda tidak berhak mengakses halaman ini";
      }
    }

    public function tambah_holmes(Request $request){
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user')){
      $tambah = new Holmes;
      $tambah->email = $request->email;
      $tambah->d1 = $request->d1;
      $tambah->d2 = $request->d2;
      $tambah->d3 = $request->d3;
      $tambah->d4 = $request->d4;
      $tambah->d5 = $request->d5;
      $tambah->d6 = $request->d6;
      $tambah->d7 = $request->d7;
      $tambah->d8 = $request->d8;
      $tambah->d9 = $request->d9;
      $tambah->d10 = $request->d10;
      $tambah->d11 = $request->d11;
      $tambah->d12 = $request->d12;
      $tambah->d13 = $request->d13;
      $tambah->d14 = $request->d14;
      $tambah->d15 = $request->d15;
      $tambah->d16 = $request->d16;
      $tambah->d17 = $request->d17;
      $tambah->d18 = $request->d18;
      $tambah->d19 = $request->d19;
      $tambah->d20 = $request->d20;
      $tambah->d21 = $request->d21;
      $tambah->d22 = $request->d22;
      $tambah->d23 = $request->d23;
      $tambah->d24 = $request->d24;
      $tambah->d25 = $request->d25;
      $tambah->d26 = $request->d26;
      $tambah->d27 = $request->d27;
      $tambah->d28 = $request->d28;
      $tambah->d29 = $request->d29;
      $tambah->d30 = $request->d30;
      $tambah->d31 = $request->d31;
      $tambah->d32 = $request->d32;
      $tambah->d33 = $request->d33;
      $tambah->d34 = $request->d34;
      $tambah->d35 = $request->d35;
      $tambah->d36 = $request->d36;
      $tambah->d37 = $request->d37;
      $tambah->d38 = $request->d38;
      $tambah->d39 = $request->d39;
      $tambah->d40 = $request->d40;
      $tambah->d41 = $request->d41;
      $tambah->d42 = $request->d42;
      $tambah->d43 = $request->d43;

      $a9 = 9;
     $a11 = 11;
      $a12 = 12;
       $a13 = 13;
       $a15 = 15;
        $a16 = 16;
         $a17 = 17;
          $a18=18;
           $a19=19;
            $a20=20;
             $a23=23;
              $a24=24;
               $a25=25;
                $a26=26;
      $a28 =28;
       $a29=29;
        $a30=30;
         $a35=35;
          $a36=36;
           $a37=37;
            $a38=38;
             $a39=39;
              $a40=40;
               $a44=44;
                $a45=45;
                 $a47=47;
                  $a50=50;
                   $a63=63;
                    $a65=65;
                     $a73=73;
                      $a100=100;
                       $a1=1;
                        $a2=2;

      $total = ($request->d1*$a100*$a1)+
      ($request->d2*$a73*$a1)+
      ($request->d3*$a65*$a1)+
      ($request->d4*$a63*$a1)+
      ($request->d5*$a63*$a1)+
      ($request->d6*$a9*$a2)+
      ($request->d7*$a50*$a1)+
      ($request->d8*$a47*$a1)+
      ($request->d9*$a45*$a1)+
      ($request->d10*$a45*$a1)+
      ($request->d11*$a44*$a1)+
      ($request->d12*$a40*$a1)+
      ($request->d13*$a39*$a1)+
      ($request->d14*$a39*$a1)+
      ($request->d15*$a38*$a1)+
      ($request->d16*$a38*$a1)+
      ($request->d17*$a37*$a1)+
      ($request->d18*$a36*$a1)+
      ($request->d19*$a35*$a1)+
      ($request->d20*$a30*$a1)+
      ($request->d21*$a29*$a1)+
      ($request->d22*$a29*$a1)+
      ($request->d23*$a29*$a1)+
      ($request->d24*$a29*$a1)+
      ($request->d25*$a28*$a1)+
      ($request->d26*$a26*$a1)+
      ($request->d27*$a26*$a1)+
      ($request->d28*$a25*$a1)+
      ($request->d29*$a24*$a1)+
      ($request->d30*$a23*$a1)+
      ($request->d31*$a20*$a1)+
      ($request->d32*$a20*$a1)+
      ($request->d33*$a20*$a1)+
      ($request->d34*$a19*$a1)+
      ($request->d35*$a19*$a1)+
      ($request->d36*$a18*$a1)+
      ($request->d37*$a17*$a1)+
      ($request->d38*$a16*$a1)+
      ($request->d39*$a15*$a1)+
      ($request->d40*$a15*$a1)+
      ($request->d41*$a13*$a1)+
      ($request->d42*$a12*$a1)+
      ($request->d43*$a11*$a1);
      $tambah->total = $total;
      
      if($total <=149){
        $tambah->tingkat = 'Ringan/Normal';
      }elseif ($total <= 299) {
        $tambah->tingkat = 'Sedang';
      }else{
        $tambah->tingkat = 'Berat';
      }
      $tambah->psikolog = Auth::user()->psikolog;
      $tambah->save();
    	
  
      return redirect('/kuisioner/rekapUserHolmes');
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
    }

    public function update_holmes($id, Request $request)
    {
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant')){
    		$hasil = Holmes::find($id);
    		$hasil->penjelasan = $request->penjelasan;
    		$hasil->status = $request->status;
    		$hasil->save();
        return redirect('/kuisioner/rekapHolmes');
      }else{
        echo "Anda tidak berhak mengakses halaman ini";
      }
    }

    public function tambah_hars(Request $request){
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user')){
      $tambah = new Hars;
      $tambah->email = $request->email;
      $tambah->d1 = $request->d1;
      $tambah->d2 = $request->d2;
      $tambah->d3 = $request->d3;
      $tambah->d4 = $request->d4;
      $tambah->d5 = $request->d5;
      $tambah->d6 = $request->d6;
      $tambah->d7 = $request->d7;
      $tambah->d8 = $request->d8;
      $tambah->d9 = $request->d9;
      $tambah->d10 = $request->d10;
      $tambah->d11 = $request->d11;
      $tambah->d12 = $request->d12;
      $tambah->d13 = $request->d13;
      $tambah->d14 = $request->d14;

      $total = $request->d1+$request->d2+$request->d3+$request->d4+$request->d5+$request->d6+$request->d7+$request->d8+$request->d9+$request->d10+$request->d11+$request->d12+$request->d13+$request->d14;
      $tambah->total = $total;

      if($total <=6){
        $tambah->tingkat = 'Normal';
      }elseif ($total <= 14) {
        $tambah->tingkat = 'Ringan';
      }elseif ($total <= 27) {
        $tambah->tingkat = 'Sedang';
      }else{
        $tambah->tingkat = 'Berat';
      }

      $tambah->psikolog = Auth::user()->psikolog;

      $tambah->save();
      return redirect('/kuisioner/rekapUserHars');
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
    }

    public function edit_hars($id)
    {
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user')){
    		$hasil = Hars::find($id);
        return view('kuisioner.edit_kuisioner_hars', ['hasil' => $hasil]);
      }else{
        echo "Anda tidak berhak mengakses halaman ini";
      }
    }

    public function update_kuisioner_hars($id, Request $request){
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant')){
      $tambah = Hars::find($id);
      $tambah->email = $request->email;
      $tambah->d1 = $request->d1;
      $tambah->d2 = $request->d2;
      $tambah->d3 = $request->d3;
      $tambah->d4 = $request->d4;
      $tambah->d5 = $request->d5;
      $tambah->d6 = $request->d6;
      $tambah->d7 = $request->d7;
      $tambah->d8 = $request->d8;
      $tambah->d9 = $request->d9;
      $tambah->d10 = $request->d10;
      $tambah->d11 = $request->d11;
      $tambah->d12 = $request->d12;
      $tambah->d13 = $request->d13;
      $tambah->d14 = $request->d14;

      $total = $request->d1+$request->d2+$request->d3+$request->d4+$request->d5+$request->d6+$request->d7+$request->d8+$request->d9+$request->d10+$request->d11+$request->d12+$request->d13+$request->d14;
      $tambah->total = $total;

      if($total <=6){
        $tambah->tingkat = 'Normal';
      }elseif ($total <= 14) {
        $tambah->tingkat = 'Ringan';
      }elseif ($total <= 27) {
        $tambah->tingkat = 'Sedang';
      }else{
        $tambah->tingkat = 'Berat';
      }

      $tambah->psikolog = Auth::user()->psikolog;

      $tambah->save();
      return redirect('/kuisioner/rekapHars');
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
    }

    public function update_Hars($id, Request $request)
    {
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant')){
    		$hasil = Hars::find($id);
    		$hasil->penjelasan = $request->penjelasan;
    		$hasil->status = $request->status;
    		$hasil->save();
        return redirect('/kuisioner/rekapHars');
      }else{
        echo "Anda tidak berhak mengakses halaman ini";
      }
    }

    public function tambah_kuisbdi(Request $request){
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user')){
      $tambah = new Kuisbdi;
      $tambah->email = $request->email;
      $tambah->d1 = $request->d1;
      $tambah->d2 = $request->d2;
      $tambah->d3 = $request->d3;
      $tambah->d4 = $request->d4;
      $tambah->d5 = $request->d5;
      $tambah->d6 = $request->d6;
      $tambah->d7 = $request->d7;
      $tambah->d8 = $request->d8;
      $tambah->d9 = $request->d9;
      $tambah->d10 = $request->d10;
      $tambah->d11 = $request->d11;
      $tambah->d12 = $request->d12;
      $tambah->d13 = $request->d13;
      $tambah->d14 = $request->d14;
      $tambah->d15 = $request->d15;
      $tambah->d16 = $request->d16;
      $tambah->d17 = $request->d17;
      $tambah->d18 = $request->d18;
      $tambah->d19 = $request->d19;
      $tambah->d20 = $request->d20;
      $tambah->d21 = $request->d21;
      $tambah->d22 = $request->d22;
      $tambah->d23 = $request->d23;
      $tambah->d24 = $request->d24;
      $tambah->d25 = $request->d25;
      $tambah->d26 = $request->d26;
      $tambah->d27 = $request->d27;
      $tambah->d28 = $request->d28;
      $tambah->d29 = $request->d29;
      $tambah->d30 = $request->d30;
      $tambah->d31 = $request->d31;
      $tambah->d32 = $request->d32;
      $tambah->d33 = $request->d33;
      $tambah->d34 = $request->d34;
      $tambah->d35 = $request->d35;
      $tambah->d36 = $request->d36;
      $tambah->d37 = $request->d37;
      $tambah->d38 = $request->d38;
      $tambah->d39 = $request->d39;
      $tambah->d40 = $request->d40;
      $tambah->d41 = $request->d41;
      $tambah->d42 = $request->d42;
      $tambah->d43 = $request->d43;
      $tambah->d44 = $request->d44;
      $tambah->d45 = $request->d45;
      $tambah->d46 = $request->d46;
      $tambah->d47 = $request->d47;
      $tambah->d48 = $request->d48;
      $tambah->d49 = $request->d49;
      $tambah->d50 = $request->d50;
      $tambah->d51 = $request->d51;
      $tambah->d52 = $request->d52;
      $tambah->d53 = $request->d53;
      $tambah->d54 = $request->d54;
      $tambah->d55 = $request->d55;
      $tambah->d56 = $request->d56;
      $tambah->d57 = $request->d57;
      $tambah->d58 = $request->d58;
      $tambah->d59 = $request->d59;
      $tambah->d60 = $request->d60;
      $tambah->d61 = $request->d61;
      $tambah->d62 = $request->d62;
      $tambah->d63 = $request->d63;
      $tambah->d64 = $request->d64;
      $tambah->d65 = $request->d65;
      $tambah->d66 = $request->d66;
      $tambah->d67 = $request->d67;
      $tambah->d68 = $request->d68;
      $tambah->d69 = $request->d69;
      $tambah->d70 = $request->d70;
      $tambah->d71 = $request->d71;
      $tambah->d72 = $request->d72;
      $tambah->d73 = $request->d73;
      $tambah->d74 = $request->d74;
      $tambah->d75 = $request->d75;
      $tambah->d76 = $request->d76;
      $tambah->d77 = $request->d77;
      $tambah->d78 = $request->d78;
      $tambah->d79 = $request->d79;
      $tambah->d80 = $request->d80;
      $tambah->d81 = $request->d81;
      $tambah->d82 = $request->d82;
      $tambah->d83 = $request->d83;
      $tambah->d84 = $request->d84;
      $tambah->psikolog = Auth::user()->psikolog;
      $tambah->save();
    	
  
      return redirect('/kuisioner/rekapUserKuisbdi');
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
    }

    public function update_kuisBdi($id, Request $request)
    {
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user')){
    		$hasil = Kuisbdi::find($id);
    		$hasil->penjelasan = $request->penjelasan;
    		$hasil->status = $request->status;
    		$hasil->save();
        return redirect('/kuisioner/rekapKuisBdi');
      }else{
        echo "Anda tidak berhak mengakses halaman ini";
      }
    }

    public function deleteDass($id)
    {
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user')){
    		$hasil = Dass::find($id);
    		$hasil->delete();
        return redirect('/kuisioner/rekapDassD');
      }else{
        echo "Anda tidak berhak mengakses halaman ini";
      }
    }

    public function deleteBdi($id)
    {
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user')){
    		$hasil = Bdi::find($id);
    		$hasil->delete();
        return redirect('/kuisioner/rekapBdi');
      }else{
        echo "Anda tidak berhak mengakses halaman ini";
      }
    }

    public function deleteTmas($id)
    {
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user')){
    		$hasil = Tmas::find($id);
    		$hasil->delete();
        return redirect('/kuisioner/rekapTmas');
      }else{
        echo "Anda tidak berhak mengakses halaman ini";
      }
    }

    public function deleteHars($id)
    {
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user')){
    		$hasil = Hars::find($id);
    		$hasil->delete();
        return redirect('/kuisioner/rekapHars');
      }else{
        echo "Anda tidak berhak mengakses halaman ini";
      }
    }

    public function deleteHolmes($id)
    {
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user')){
    		$hasil = Holmes::find($id);
    		$hasil->delete();
        return redirect('/kuisioner/rekapHolmes');
      }else{
        echo "Anda tidak berhak mengakses halaman ini";
      }
    }

    public function deleteKuisbdi($id)
    {
      if(!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user')){
    		$hasil = Kuisbdi::find($id);
    		$hasil->delete();
        return redirect('/kuisioner/rekapKuisbdi');
      }else{
        echo "Anda tidak berhak mengakses halaman ini";
      }
    }

}