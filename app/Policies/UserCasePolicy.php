<?php

namespace App\Policies;

use App\Models\User;
use App\Models\UserCase;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserCasePolicy
{
    use HandlesAuthorization;

    public function index (User $user)
    {
        return !$user->role || ($user->role &&( $user->role->name == 'user' || $user->role->name == 'consultant'));
    }
    
    public function create (User $user)
    {
        return !$user->role || ($user->role && $user->role->name == 'user');
    }

    protected function canManage (User $user, UserCase $userCase)
    {
        if (!$user->role) {
            return $user->id == $userCase->user_id;
        }

        if ($user->role) {
            if ($user->role->name == 'user') {
                return $user->id == $userCase->user_id;
            }
            
            if ($user->role->name == 'consultant') {
                return $user->id == $userCase->consultant_id;
            }
        }

        return false;
    }

    public function show (User $user, UserCase $userCase)
    {
        return $this->canManage($user, $userCase);
    }

    public function respond (User $user, UserCase $userCase)
    {
        return $this->canManage($user, $userCase);
    }

    public function update (User $user, UserCase $userCase)
    {
        if (!$user->role || ($user->role && $user->role->name == 'user')) {
            return $user->id == $userCase->user_id;
        }

        return false;
    }

    public function updateStatus (User $user, UserCase $userCase)
    {
        if ($user->role && $user->role->name == 'consultant') {
            return $user->id == $userCase->consultant_id;
        }

        return false;
    }
}
