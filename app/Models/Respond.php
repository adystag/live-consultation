<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Respond extends Model
{
    protected $fillable = [
        'content',
        'user_id',
        'respondable_id',
        'respondable_type'
    ];

    public function user ()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function respondable ()
    {
        return $this->morphTo();
    }
}
