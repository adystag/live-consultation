<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Setting;

class SettingController extends Controller
{
  public function setting()
  {
      return view('setting');
  }
  public function edit()
  {
      $point = Setting::where('setting_key', 'point')->first();
      return view('setting', compact('point'));
  }
  public function update(Request $req, $id)
  {
      if ($req['poin'] <= 0) {
        return redirect()->back()->with('status', 'danger')
                                ->with('status-message', 'Poin tidak valid.');
      }

      $setting = Setting::find($id);
      $setting->setting_value=$req['point'];
      $setting->update();
      return redirect()->route('setting.edit');
  }

}
