@extends('layouts.app')

@section('main')
<div class="p-3 bg-white shadow-sm rounded">
    <h3 class="mb-3">
        Notifications
    </h3>

    <notifications></notifications>
</div>
@endsection

@push('data-scripts')
<script>
    window.Laravel = {
        user: {!! json_encode(Auth::user()->toArray()) !!},
        notifications: {!! json_encode($notifications) !!},
        url: "{!! url('/') !!}"
    }
</script>
@endpush
