@extends('layouts.app')

@section('main')
<div class="p-3 bg-white rounded shadow-sm">
    <h3 class="mb-3">User Profile</h3>
    
    @if (session('status'))
    <div class="alert alert-{{ session('status') }} alert-dismissible fade show mb-3" role="alert">
        {{ session('status-message') }}
        
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    
    <form style="margin-bottom: 32px;" method="POST" action="{{ route('update-user-avatar') }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="mb-3">
            <div class="ml-auto mr-auto" style="height: 128px; width: 128px; position: relative;">
                <img class="rounded-circle border" src="{{ !is_null($user->avatar) && !empty($user->avatar) ?  $user->avatar_url : asset('img/user.png') }}" style="height: 128px; width: 128px; object-fit: contain; object-position: center;">
                
                @if (!is_null($user->avatar) && !empty($user->avatar))
                <div class="btn rounded-circle btn-danger d-flex align-items-center justify-content-center p-0" style="position: absolute; bottom: 0; right: 0; height: 32px; width: 32px;" onclick="confirmDeleteAvatar()">
                    <i class="fas fa-trash"></i>
                </div>
                @endif
            </div>
        </div>
        
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Avatar</label>
            
            <div class="col-sm-8">
                <div class="custom-file">
                    <input type="file" class="custom-file-input {{ $errors->has('avatar') ? ' is-invalid' : '' }}" id="avatar" name="avatar" accept=".png,.jpg">
                    <label class="custom-file-label" for="avatar">Choose file</label>
                    
                    @if ($errors->has('avatar'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('avatar') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            
            <div class="col-sm-2">
                <button type="submit" class="btn btn-primary btn-block">Save</button>
            </div>
        </div>
    </form>
    
    <form method="POST" action="{{ route('update-user-profile') }}" class="mb-3">
        @csrf
        @method('PUT')
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">First Name</label>
            <div class="col-sm-10">
                <input class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" placeholder="First name..." value="{{ $user->first_name }}">
                
                @if ($errors->has('first_name'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('first_name') }}</strong>
                </span>
                @endif
            </div>
        </div>
        
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Last Name</label>
            <div class="col-sm-10">
                <input class="form-control {{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" placeholder="Last name..." value="{{ $user->last_name }}">
                
                @if ($errors->has('last_name'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('last_name') }}</strong>
                </span>
                @endif
            </div>
        </div>
        
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Email</label>
            <div class="col-sm-10">
                <input type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email" name="email" value="{{ $user->email }}">
                
                @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>
        </div>
        
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Bio</label>
            <div class="col-sm-10">
                <textarea class="form-control" placeholder="Bio" rows="5" name="bio">{{ $user->bio }}</textarea>
            </div>
        </div>
        
        <div class="form-group d-flex m-0">
            <button type="submit" class="btn btn-primary ml-auto">Save</button>
        </div>
    </form>

    <h5 class="mb-3">Change Password</h5>

    <form method="POST" action="{{ route('update-user-password') }}">
            @csrf
            @method('PUT')
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Old Password</label>
                <div class="col-sm-10">
                    <input type="password" class="form-control {{ $errors->has('old_password') ? ' is-invalid' : '' }}" name="old_password" placeholder="Old password...">
                    
                    @if ($errors->has('old_password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('old_password') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">New Password</label>
                <div class="col-sm-10">
                    <input type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="New password...">
                    
                    @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Password Confirmation</label>
                <div class="col-sm-10">
                    <input type="password" class="form-control {{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" placeholder="Password confirmation..." name="password_confirmation">
                    
                    @if ($errors->has('password_confirmation'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            
            <div class="form-group d-flex m-0">
                <button type="submit" class="btn btn-primary ml-auto">Save</button>
            </div>
        </form>
    
    @if (!is_null($user->avatar) && !empty($user->avatar))
    <form id="remove-user-avatar-form" method="POST" action="{{ route('remove-user-avatar') }}">
        @csrf
        @method('DELETE')
    </form>
    @endif
</div>
@endsection

@push('styles')
<link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css') }}">
@endpush

@push('scripts')
<script src="{{ asset('js/sweetalert2.min.js') }}"></script>
<script>
    $('#avatar').on('change', function (e) {
        if ($(this)[0].files[0]) {
            $('label[for=avatar]').html($(this)[0].files[0].name)
        } else {
            $('label[for=avatar]').html('Choose file')
        }
    })
    
    function confirmDeleteAvatar () {
        swal({
            title: 'Delete Avatar',
            text: 'Do you want to delete your avatar?',
            type: 'question',
            showCancelButton: true,
            confirmButtonText: 'Yes'
        })
        .then(result => {
            $('#remove-user-avatar-form').submit()
        })
    }
</script>
@endpush