<?php


use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRekapUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('rekap_user', function (Blueprint $table) {
            $table->increments('id_rekap_user');
            $table->integer('id_user')->length(11);
            $table->integer('id_dass')->length(11);
            $table->integer('nilai_dass')->length(11);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rekap_user');
    }
}