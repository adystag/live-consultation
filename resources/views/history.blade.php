@extends('layouts.app')

@section('main')
<div class="card">
    <div class="card-header">History</div>

    <div class="card-body">
        @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
        @endif

<div>

  <table class="table table-bordered" id="history-table">
    @if (!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user'))
        <thead>
            <tr>
                <th>Id</th>
                <th>Subject</th>
                <th>Category</th>
                <th>Status</th>
                <th>Consultant</th>
                <th>Date</th>
            </tr>
        </thead>
        @elseif (!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant'))
        <thead>
            <tr>
                <th>Id</th>
                <th>Subject</th>
                <th>Category</th>
                <th>Status</th>
                <th>Client</th>
                <th>Date</th>
            </tr>
        </thead>
        @endif

    </table>

</div>

    </div>
</div>
@endsection

@push('scripts')
<script>

  $(function() {
      $('#history-table').DataTable({
          processing: true,
          serverSide: true,
          ajax: '{!! route('history.data') !!}',
          columns: [

              { data: 'id', name: 'id' },
              { data: 'subject', name: 'subject' },
              { data: 'category.name', name: 'category_id' },
              { data: 'status', name: 'status' },
            //   { data: null, render: function ( data, type, row ) {
            //     if (data.role_name =='consultant') {
            //         return data.user.first_name+' '+data.user.last_name;
            //     }else {
            //         return data.consultant.first_name+' '+data.consultant.last_name;
            //     }
            //
            //   }
            // },
              {data: 'related', name: "related"},
              { data: 'created_at', name: 'created_at' },
          ]
      });
  });


</script>
@endpush
