@extends('categories.manage.layouts')

@section('members')
<table id="category-members-table" class="table table-striped">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">First Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">Email</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    
    <tbody>
        @foreach ($category->users as $key => $user)
        <tr>
            <th>{{ $key + 1}}</th>
            <td>{{ $user->first_name }}</td>
            <td>{{ $user->last_name }}</td>
            <td>{{ $user->email }}</td>
            <td>
                <button class="btn btn-danger btn-sm" onclick="confirmDeleteMember({{ $user->id }}, '{{ $user->first_name.' '.$user->last_name }}')">Delete</button>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

<form id="delete-category-member-form" method="POST">
    @csrf
    @method('DELETE')
</form>
@endsection

@push('scripts')
<script>    
    function confirmDeleteMember (id, name) {
        var url = '{!! url("/categories/{$category->id}/users") !!}'
        
        swal({
            title: 'Delete Member',
            text: 'Do you want to delete this member: ' + name,
            type: 'question',
            showCancelButton: true,
            confirmButtonText: 'Yes'
        })
        .then(result => {
            if (!result.dismiss) {
                $('#delete-category-member-form').attr('action', url + '/' + id)
                $('#delete-category-member-form').submit()
            }
        })
    }
</script>
@endpush