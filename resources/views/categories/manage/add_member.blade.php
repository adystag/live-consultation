@extends('categories.manage.layouts')

@section('members')
<table id="category-members-table" class="table table-striped">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">First Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">Email</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    
    <tbody>
        @foreach ($users as $key => $user)
        <tr>
            <th>{{ $key + 1}}</th>
            <td>{{ $user->first_name }}</td>
            <td>{{ $user->last_name }}</td>
            <td>{{ $user->email }}</td>
            <td>
                <button class="btn btn-success btn-sm" onclick="confirmAddMember({{ $user->id }}, '{{ $user->first_name.' '.$user->last_name }}')">Add</button>
            </td>
        </tr>
        @endforeach
    </tbody> 
</table>

<form id="add-category-member-form" method="POST" action="{{ route('add-category-member', [ 'categoryId' => $category->id ]) }}">
    @csrf
    <input type="hidden" name="user_id" />
</form>
@endsection

@push('scripts')
<script>    
    function confirmAddMember (id, name) {        
        swal({
            title: 'Add Member',
            text: 'Do you want to add this user: ' + name + ' as member?',
            type: 'question',
            showCancelButton: true,
            confirmButtonText: 'Yes'
        })
        .then(result => {
            if (!result.dismiss) {
                $('#add-category-member-form > input[name=user_id]').val(id)
                $('#add-category-member-form').submit()
            }
        })
    }
</script>
@endpush