@extends('layouts.app')

@section('main')
<div class="p-3 rounded shadow-sm bg-white">
        <nav class="nav nav-pills nav-justified">
                <a class="nav-item nav-link" href="/kuisioner/bdi">BDI</a>
                <a class="nav-item nav-link active" href="/kuisioner/kuisbdi">Kuis BDI</a>
              </nav>
              <hr>

    <h3 class="mb-3">Kuis Beck Depression Inventory (BDI)
        </h3>

    @if (session('status'))
    <div class="alert alert-{{ session('status') }} alert-dismissible fade show mb-3" role="alert">
        {{ session('status-message') }}

        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <form action="{{ route('tambahdatakuisbdi')}}" method="POST">
        @csrf
        <table class="table table-striped mb-0">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col"></th>
                    <th scope="col">Kondisi</th>
                    <th scope="col">0</th>
                    <th scope="col">1</th>
                    <th scope="col">2</th>
                    <th scope="col">3</th>
                </tr>
            </thead>

            <tbody>
                <input type="hidden" name="email" value="{{ Auth::user()->email }}">
                <tr>
                    <th>1</th>
                    <td>Kesedihan
                    </td>
                    <td>
                        Saya tidak merasa sedih
                    </td>
                    <td><label class="radio-inline"><input type="radio" name="d1" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d1" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d1" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d1" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                        Saya merasa sedih
                    </td>
                    <td><label class="radio-inline"><input type="radio" name="d2" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d2" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d2" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d2" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                        Saya merasa sedih sepanjang waktu dan saya tidak dapat menghilangkannya
                    </td>
                    <td><label class="radio-inline"><input type="radio" name="d3" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d3" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d3" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d3" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                        Saya begitu sedih sehingga saya merasa tidak tahan lagi
                    </td>
                    <td><label class="radio-inline"><input type="radio" name="d4" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d4" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d4" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d4" value="3"></label></td>
                </tr>
                <tr>
                    <th>2</th>
                    <td>Pesimis
                    </td>
                    <td>
                        Saya tidak merasa berkecil hati terhadap masa depan
                    </td>
                    <td><label class="radio-inline"><input type="radio" name="d5" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d5" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d5" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d5" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                        Saya merasa berkecil hati terhadap masa depan
                    </td>
                    <td><label class="radio-inline"><input type="radio" name="d6" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d6" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d6" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d6" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                        Saya merasa tidak ada sesuatu yang saya nantikan
                    </td>
                    <td><label class="radio-inline"><input type="radio" name="d7" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d7" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d7" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d7" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                        Saya merasa bahwa tidak ada harapan di masa depan dan segala sesuatunya tidak dapat diperbaiki
                    </td>
                    <td><label class="radio-inline"><input type="radio" name="d8" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d8" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d8" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d8" value="3"></label></td>
                </tr>
                <th>3</th>
                <td>Kegagalan
                </td>
                <td>
                        Saya tidak merasa gagal
                    </td>
                <td><label class="radio-inline"><input type="radio" name="d9" value="0" checked="checked"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d9" value="1"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d9" value="2"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d9" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya merasa lebih banyak mengalami kegagalan daripada rata – rata orang
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d10" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d10" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d10" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d10" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Kalau saya meninjau kembali hidup saya, yang dapat saya lihat hanyalah banyak kegagalan
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d11" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d11" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d11" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d11" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya merasa sebagai seorang pribadi yang gagal total
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d12" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d12" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d12" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d12" value="3"></label></td>
                </tr>
                <th>4</th>
                <td>Kehilangan Kesenangan
                </td>
                <td>
                        Saya memperoleh kepuasan atas segala sesuatu seperti biasanya
                    </td>
                <td><label class="radio-inline"><input type="radio" name="d13" value="0" checked="checked"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d13" value="1"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d13" value="2"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d13" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya tidak dapat menikmati segala sesuatu seperti biasanya
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d14" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d14" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d14" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d14" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya tidak lagi memperoleh kepuasan yang nyata dari segala sesuatu
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d15" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d15" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d15" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d15" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya merasa tidak puas atau bosan terhadap apa saja
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d16" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d16" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d16" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d16" value="3"></label></td>
                </tr>
                <th>5</th>
                <td>Perasaan Bersalah
                </td>
                <td>
                        Saya tidak merasa bersalah
                    </td>
                <td><label class="radio-inline"><input type="radio" name="d17" value="0" checked="checked"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d17" value="1"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d17" value="2"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d17" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya cukup sering merasa bersalah
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d18" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d18" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d18" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d18" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya sering merasa sangat bersalah
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d19" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d19" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d19" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d19" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya merasa bersalah sepanjang waktu
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d20" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d20" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d20" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d20" value="3"></label></td>
                </tr>
                <th>6</th>
                <td>Perasaan akan Hukuman
                </td>
                <td>
                        Saya tidak merasa bahwa saya sedang dihukum
                    </td>
                <td><label class="radio-inline"><input type="radio" name="d21" value="0" checked="checked"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d21" value="1"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d21" value="2"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d21" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya merasa bahwa saya mungkin dihukum
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d22" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d22" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d22" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d22" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya mengharapkan agar dihukum
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d23" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d23" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d23" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d23" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya merasa bahwa saya sedang dihukum
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d24" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d24" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d24" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d24" value="3"></label></td>
                </tr>
                <th>7</th>
                <td>Tidak Menyukai Diri Sendiri
                </td>
                <td>
                        Saya tidak merasa kecewa terhadap diri saya sendiri
                    </td>
                <td><label class="radio-inline"><input type="radio" name="d25" value="0" checked="checked"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d25" value="1"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d25" value="2"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d25" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya merasa kecewa terhadap diri saya sendiri
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d26" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d26" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d26" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d26" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya merasa jijik terhadap diri saya sendiri
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d27" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d27" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d27" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d27" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya membenci diri saya sendiri
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d28" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d28" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d28" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d28" value="3"></label></td>
                </tr>
                <th>8</th>
                <td>Mengkritik Diri Sendiri
                </td>
                <td>
                        Saya tidak merasa bahwa saya lebih buruk daripada orang lain
                    </td>
                <td><label class="radio-inline"><input type="radio" name="d29" value="0" checked="checked"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d29" value="1"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d29" value="2"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d29" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya selalu mencela diri saya sendiri karena kelemahan atau kekeliruan saya
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d30" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d30" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d30" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d30" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya menyalahkan diri saya sendiri sepanjang waktu atas kesalahan – kesalahan saya
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d31" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d31" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d31" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d31" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya menyalahkan diri saya sendiri atas semua hal buruk yang terjadi
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d32" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d32" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d32" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d32" value="3"></label></td>
                </tr>
                <th>9</th>
                <td>Pikiran atau Keinginan Bunuh Diri
                </td>
                <td>
                        Saya tidak mempunyai pikiran untuk bunuh diri
                    </td>
                <td><label class="radio-inline"><input type="radio" name="d33" value="0" checked="checked"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d33" value="1"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d33" value="2"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d33" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya mempunyai pikiran – pikiran untuk bunuh diri, tetapi saya tidak akan melaksanakannya
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d34" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d34" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d34" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d34" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya ingin bunuh diri
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d35" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d35" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d35" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d35" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya akan bunuh diri kalau ada kesempatan
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d36" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d36" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d36" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d36" value="3"></label></td>
                </tr>
                <th>10</th>
                <td>Menangis
                </td>
                <td>
                        Saya tidak menangis lebih dari biasanya
                    </td>
                <td><label class="radio-inline"><input type="radio" name="d37" value="0" checked="checked"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d37" value="1"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d37" value="2"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d37" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Sekarang saya lebih banyak menangis daripada biasanya
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d38" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d38" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d38" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d38" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Sekarang saya menangis sepanjang waktu
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d39" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d39" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d39" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d39" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya biasanya dapat menangis, tetapi sekarang saya tidak dapat menangis meskipun saya ingin menangis
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d40" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d40" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d40" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d40" value="3"></label></td>
                </tr>
                <th>11</th>
                <td>Pergolakan dalam Diri
                </td>
                <td>
                        Sekarang saya tidak merasa lebih jengkel daripada sebelumnya
                    </td>
                <td><label class="radio-inline"><input type="radio" name="d41" value="0" checked="checked"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d41" value="1"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d41" value="2"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d41" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya lebih mudah jengkel atau marah daripada biasanya
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d42" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d42" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d42" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d42" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya sekarang merasa jengkel sepanjang waktu
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d43" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d43" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d43" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d43" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya tidak dibuat jengkel oleh hal – hal yang biasanya menjengkelkan saya
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d44" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d44" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d44" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d44" value="3"></label></td>
                </tr>
                <th>12</th>
                <td>Kehilangan Minat
                </td>
                <td>
                        Saya masih tetap senang bergaul dengan orang lain
                    </td>
                <td><label class="radio-inline"><input type="radio" name="d45" value="0" checked="checked"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d45" value="1"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d45" value="2"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d45" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya kurang berminat pada orang lain dibandingkan dengan biasanya
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d46" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d46" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d46" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d46" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya tak kehilangan sebagian besar minat saya terhadap orang lain
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d47" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d47" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d47" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d47" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya telah kehilangan seluruh minat saya terhadap orang lain
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d48" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d48" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d48" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d48" value="3"></label></td>
                </tr>
                <th>13</th>
                <td>Bimbang/ragu-ragu
                </td>
                <td>
                        Saya mengambil keputusan – keputusan sama baiknya dengan sebelumnya
                    </td>
                <td><label class="radio-inline"><input type="radio" name="d49" value="0" checked="checked"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d49" value="1"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d49" value="2"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d49" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya lebih banyak menunda keputusan daripada biasanya
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d50" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d50" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d50" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d50" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya mempunyai kesulitan yang lebih besar dalam mengambil keputusan daripada sebelumnya
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d51" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d51" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d51" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d51" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya sama sekali tidak dapat mengambil keputusan apa pun
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d52" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d52" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d52" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d52" value="3"></label></td>
                </tr>
                <th>14</th>
                <td>Perasaan Tidak Berharga
                </td>
                <td>
                        Saya tidak merasa bahwa saya kelihatan lebih jelek daripada sebelumnya
                    </td>
                <td><label class="radio-inline"><input type="radio" name="d53" value="0" checked="checked"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d53" value="1"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d53" value="2"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d53" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya merasa cemas jangan – jangan saya tua atau tidak menarik
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d54" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d54" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d54" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d54" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya merasa bahwa ada perubahan – perubahan tetap pada penampilan saya yang membuat saya kelihatan tidak menarik
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d55" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d55" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d55" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d55" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya yakin bahwa saya kelihatan jelek
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d56" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d56" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d56" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d56" value="3"></label></td>
                </tr>
                <th>15</th>
                <td>Kehilangan Energi
                </td>
                <td>
                        Saya dapat bekerja dengan baik seperti sebelumnya
                    </td>
                <td><label class="radio-inline"><input type="radio" name="d57" value="0" checked="checked"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d57" value="1"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d57" value="2"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d57" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya membutuhkan usaha istimewa untuk mulai mengerjakan sesuatu
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d58" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d58" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d58" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d58" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya harus memaksa diri saya untuk mengerjakan sesuatu
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d59" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d59" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d59" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d59" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya sama sekali tidak dapat mengerjakan apa – apa
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d60" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d60" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d60" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d60" value="3"></label></td>
                </tr>
                <th>16</th>
                <td>Kehilangan Pola Tidur
                </td>
                <td>
                        Saya dapat tidur nyenyak seperti biasanya
                    </td>
                <td><label class="radio-inline"><input type="radio" name="d61" value="0" checked="checked"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d61" value="1"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d61" value="2"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d61" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya lebih mudah lelah dari biasanya
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d62" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d62" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d62" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d62" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya bangun 2-3 jam lebih awal dari biasanya dan sukar tidur kembali
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d63" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d63" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d63" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d63" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya bangun beberapa jam lebih awal daripada biasanya dan tidak dapat tidur kembali
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d64" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d64" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d64" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d64" value="3"></label></td>
                </tr>
                <th>17</th>
                <td>Cepat Marah
                </td>
                <td>
                        Saya tidak lebih lelah dari biasanya
                    </td>
                <td><label class="radio-inline"><input type="radio" name="d65" value="0" checked="checked"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d65" value="1"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d65" value="2"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d65" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya lebih mudah lelah dari biasanya
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d66" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d66" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d66" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d66" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya hampir selalu merasa lelah dalam mengerjakan segala sesuatu
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d67" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d67" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d67" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d67" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya merasa terlalu lelah untuk mengerjakan apa saja
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d68" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d68" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d68" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d68" value="3"></label></td>
                </tr>
                <th>18</th>
                <td>Perubahan Nafsu Makan
                </td>
                <td>
                        Nafsu makan saya masih seperti biasanya
                    </td>
                <td><label class="radio-inline"><input type="radio" name="d69" value="0" checked="checked"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d69" value="1"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d69" value="2"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d69" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Nafsu makan saya tidak sebesar biasanya
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d70" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d70" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d70" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d70" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Sekarang nafsu makan saya jauh lebih berkurang
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d71" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d71" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d71" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d71" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya tidak mempunyai nafsu makan sama sekali
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d72" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d72" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d72" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d72" value="3"></label></td>
                </tr>
                <th>19</th>
                <td>Kesulitan Konsentrasi
                </td>
                <td>
                        Saya tidak banyak kehilangan berat badan akhir - akhir ini
                    </td>
                <td><label class="radio-inline"><input type="radio" name="d73" value="0" checked="checked"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d73" value="1"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d73" value="2"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d73" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya telah kehilangan berat badan 2,5 kg lebih
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d74" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d74" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d74" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d74" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya telah kehilangan berat badan 5 kg lebih
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d75" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d75" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d75" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d75" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya telah kehilangan berat badan 7,5 kg lebih. Saya sengaja berusaha mengurangi berat badan dengan makan lebih sedikit :- ya – tidak
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d76" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d76" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d76" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d76" value="3"></label></td>
                </tr>
                <th>20</th>
                <td>Keletihan atau Kelelahan
                </td>
                <td>
                        Saya tidak mencemaskan kesehatan saya melebihi biasanya
                    </td>
                <td><label class="radio-inline"><input type="radio" name="d77" value="0" checked="checked"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d77" value="1"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d77" value="2"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d77" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya cemas akan masalah kesehatan fisik saya, seperti sakit dan rasa nyeri; sakit perut; ataupun sembelit
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d78" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d78" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d78" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d78" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya sangat cemas akan masalah kesehatan fisik saya dan sulit memikirkan hal – hal lainnya
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d79" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d79" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d79" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d79" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya begitu cemas akan kesehatan fisik saya sehingga saya tidak dapat berpikir mengenai hal – hal lainnya
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d80" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d80" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d80" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d80" value="3"></label></td>
                </tr>
                <th>21</th>
                <td>Kehilangan Ketertarikan akan Seks
                </td>
                <td>
                        Saya tidak merasa ada perubahan dalam minat saya terhadap seks pada  akhir – akhir ini
                    </td>
                <td><label class="radio-inline"><input type="radio" name="d81" value="0" checked="checked"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d81" value="1"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d81" value="2"></label></td>
                <td><label class="radio-inline"><input type="radio" name="d81" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya kurang berminat terhadap seks kalau dibandingkan dengan biasanya
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d82" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d82" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d82" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d82" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Sekarang saya sangat kurang berminat terhadap seks
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d83" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d83" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d83" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d83" value="3"></label></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya sama sekali kehilangan minat terhadap seks
                        </td>
                    <td><label class="radio-inline"><input type="radio" name="d84" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d84" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d84" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d84" value="3"></label></td>
                </tr>
            </tbody>
        </table><br>
        <div class="form-group mb-0 d-flex">
                <button type="submit" class="btn btn-primary ml-auto">Submit</button>
            </div>
    </form>



</div>

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css" />
<link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css') }}">
@endpush

@push('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
<script src="{{ asset('js/sweetalert2.min.js') }}"></script>
<script>
$(document).ready(function() {
    $('#table').DataTable({
        "ordering": false
    });
});
</script>
@endpush