<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHolmesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('holmes', function (Blueprint $table) {
            $table->increments('id_user_holmes');
            $table->string('email');
            $table->integer('d1')->length(1);
            $table->integer('d2')->length(1);
            $table->integer('d3')->length(1);
            $table->integer('d4')->length(1);
            $table->integer('d5')->length(1);
            $table->integer('d6')->length(1);
            $table->integer('d7')->length(1);
            $table->integer('d8')->length(1);
            $table->integer('d9')->length(1);
            $table->integer('d10')->length(1);
            $table->integer('d11')->length(1);
            $table->integer('d12')->length(1);
            $table->integer('d13')->length(1);
            $table->integer('d14')->length(1);
            $table->integer('d15')->length(1);
            $table->integer('d16')->length(1);
            $table->integer('d17')->length(1);
            $table->integer('d18')->length(1);
            $table->integer('d19')->length(1);
            $table->integer('d20')->length(1);
            $table->integer('d21')->length(1);
            $table->integer('d22')->length(1);
            $table->integer('d23')->length(1);
            $table->integer('d24')->length(1);
            $table->integer('d25')->length(1);
            $table->integer('d26')->length(1);
            $table->integer('d27')->length(1);
            $table->integer('d28')->length(1);
            $table->integer('d29')->length(1);
            $table->integer('d30')->length(1);
            $table->integer('d31')->length(1);
            $table->integer('d32')->length(1);
            $table->integer('d33')->length(1);
            $table->integer('d34')->length(1);
            $table->integer('d35')->length(1);
            $table->integer('d36')->length(1);
            $table->integer('d37')->length(1);
            $table->integer('d38')->length(1);
            $table->integer('d39')->length(1);
            $table->integer('d40')->length(1);
            $table->integer('d41')->length(1);
            $table->integer('d42')->length(1);
            $table->integer('d43')->length(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('holmes');
    }
}