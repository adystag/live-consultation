@extends('layouts.app')

@section('main')
<consultation></consultation>

<div id="profile-modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content p-3 d-flex flex-colum align-items-center justify-content-center" style="position: relative;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute; top: 0; right: 8px;">
                <span aria-hidden="true" style="font-size: 30px;">&times;</span>
            </button>
            
            <img id="profile-avatar" class="rounded-circle" src="" style="height: 128px; width: 128px; object-fit: contain; object-position: center;">
            
            <div class="mt-1 text-center">
                <p class="m-0" style="font-size: 24px;">
                    <b id="profile-name">User Name</b>
                </p>
                
                <p id="profile-role" style="font-size: 18px;">User Role</p>
            </div>
        </div>
    </div>
</div>
@endsection

@push('data-scripts')
<script>
    window.Laravel = {
        user: {!! json_encode($user->toArray()) !!},
        consultation: {!! json_encode($consultation) !!},
        responds: {!! json_encode($responds) !!},
        defaultProfpic: "{!! asset('img/user.png') !!}"
    }
</script>
@endpush