<div class="h-75 mh-75" style="overflow: auto">
    <ul class="nav nav-pills flex-column">
        <li class="p-2 nav-item">
            <a class="nav-link d-flex align-items-center text-dark" href="{{ route('home') }}">
                <i class="fas fa-road"></i>
                <span class="ml-3">Users Guide</span>
            </a>
        </li>

        <li class="p-2 nav-item">
            <a class="nav-link d-flex align-items-center text-dark" href="{{ route('tac') }}">
                <i class="fas fa-list-alt"></i>
                <span class="ml-3">Term and Condition</span>
            </a>
        </li>

        @can('index', App\Models\UserCase::class)
        <li class="p-2 nav-item">
            <a class="nav-link d-flex align-items-center text-dark" href="{{ route('index-cases') }}">
                <i class="fas fa-file"></i>
                <span class="ml-3">Cases</span>
            </a>
        </li>
        @endcan

        @can('index', App\Models\Consultation::class)
        <li class="p-2 nav-item">
            <a class="nav-link d-flex align-items-center text-dark" href="{{ route('index-consultations') }}">
                <i class="fas fa-comment"></i>
                <span class="ml-3">Live Consultation</span>
            </a>
        </li>
        @endcan

        @can('index', App\Models\Payment::class)
        <li class="p-2 nav-item">
            <a class="nav-link d-flex align-items-center text-dark" href="{{ route('index-payments') }}">
                <i class="fas fa-money-bill"></i>
                <span class="ml-3">Payments</span>
            </a>
        </li>
        @endcan

        @if (!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user'))
        <li class="p-2 nav-item">
                <a class="nav-link d-flex align-items-center text-dark " href="/pilihPsikolog">
                  <i class="fas fa-file"></i>
                  <span class="ml-3">Kuisioner</span>
                </a>
            </li>
        @endif

        @if (!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user'))
        <li class="p-2 nav-item">
            <a class="nav-link d-flex align-items-center text-dark " data-toggle="dropdown" href="#">
              <i class="fas fa-table"></i>
              <span class="ml-3">Hasil Kuisioner</span>
            </a>
              <ul class="dropdown-menu">
                <li><a class="nav-link d-flex align-items-center text-dark" href="/kuisioner/rekapUserBdi">Depresi</a></li>
                <li><a class="nav-link d-flex align-items-center text-dark" href="/kuisioner/rekapUserTmas">Kecemasan</a></li>
                <li><a class="nav-link d-flex align-items-center text-dark" href="/kuisioner/rekapUserHolmes">Stress</a></li>
              </ul>
        </li>
        @endif

        @if (!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant') || (Auth::user()->role && Auth::user()->role->name == 'admin'))
        <li class="p-2 nav-item">
            <a class="nav-link d-flex align-items-center text-dark " data-toggle="dropdown" href="#">
              <i class="fas fa-table"></i>
              <span class="ml-3">Rekapitulasi Kuisioner</span>
            </a>
              <ul class="dropdown-menu">
                <li><a class="nav-link d-flex align-items-center text-dark" href="/kuisioner/rekapBdi">Depresi</a></li>
                <li><a class="nav-link d-flex align-items-center text-dark" href="/kuisioner/rekapTmas">Kecemasan</a></li>
                <li><a class="nav-link d-flex align-items-center text-dark" href="/kuisioner/rekapHolmes">Stress</a></li>
              </ul>
        </li>
        @endif

        @can('index', App\Models\UserCase::class)
        <li class="p-2 nav-item">
            <a class="nav-link d-flex align-items-center text-dark" href="{{ route('notifications') }}">
                <i class="fas fa-bell"></i>

                <div class="ml-3 d-flex align-items-center">
                    Notifications

                    <notification-counter></notification-counter>
                </div>
            </a>
        </li>
        @endcan



        @if (!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name != 'admin'))
        <li class="p-2 nav-item">
            <a class="nav-link d-flex align-items-center text-dark " data-toggle="dropdown" href="#">
              <i class="fa fa-clock"></i>
              <span class="ml-3">History</span>
            </a>
              <ul class="dropdown-menu">
                <li><a class="nav-link d-flex align-items-center text-dark" href="{{ route('history') }}">Case History</a></li>
                <li><a class="nav-link d-flex align-items-center text-dark" href="{{ route('historyChat') }}">Live Chat History</a></li>
              </ul>
        </li>
        @endif

        @can('manage-categories')
        <li class="p-2 nav-item">
            <a class="nav-link d-flex align-items-center text-dark" href="{{ route('index-categories') }}">
                <i class="fas fa-list-ul"></i>
                <span class="ml-3">Manage Categories</span>
            </a>
        </li>
        @endcan

        @if (!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'admin'))
        <li class="p-2 nav-item">
            <a class="nav-link d-flex align-items-center text-dark" href="{{ route('setting.edit') }}">
                <i class="fas fa-cog"></i>
                <span class="ml-3">Setting Point</span>
            </a>
        </li>
        @endif

        @can('update-user-role')
        <li class="p-2 nav-item">
            <a class="nav-link d-flex align-items-center text-dark" href="{{ route('index-users') }}">
                <i class="fas fa-users"></i>
                <span class="ml-3">Manage Users</span>
            </a>
        </li>
        @endcan

        @can('access-forum')
        <li class="p-2 nav-item">
            <a class="nav-link d-flex align-items-center text-dark" href="{{ route('chatter.home') }}">
                <i class="fas fa-comments"></i>
                <span class="ml-3">Forum</span>
            </a>
        </li>
        @endcan
    </ul>
</div>
