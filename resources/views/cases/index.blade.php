@extends('layouts.app')

@section('main')
<div class="p-3 rounded shadow-sm bg-white">
    <div class="d-flex align-items-center mb-3">
        <h3 class="m-0">Cases</h3>

        @can('create', App\Models\UserCase::class)
        <span class="ml-auto d-flex align-items-center justify-content-center">
            <span class="mr-2">Point remaining:</span>
            <span class="badge badge-pill badge-secondary">{{ $userPoints }}</span>
        </span>

        @if ($userPoints <= 0)
        <a href="{{ route('index-payments') }}" class="ml-3">
            <small>Add point</small>
        </a>
        @else
        <a class="btn btn-primary ml-3" href="{{ route('compose-case') }}">
            Open Case
        </a>
        @endif
        @endcan
    </div>

    @if (session('status'))
    <div class="alert alert-{{ session('status') }} alert-dismissible fade show mb-3" role="alert">
        {{ session('status-message') }}

        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

    <ul class="nav nav-tabs nav-fill mb-3">
        <li class="nav-item">
            <a class="nav-link {{ Request::get('status') != 'closed' ? 'active' : '' }} d-flex align-items-center justify-content-center" href="{{ route('index-cases') }}">
                <span class="mr-2">Active Cases</span>
                <span class="badge badge-pill badge-success">{{ $openCasesCount }}</span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link {{ Request::get('status') == 'closed' ? 'active' : '' }} d-flex align-items-center justify-content-center"  href="{{ route('index-cases') }}?status=closed">
                <span class="mr-2">Closed Cases</span>
                <span class="badge badge-pill badge-danger">{{ $closedCasesCount }}</span>
            </a>
        </li>
    </ul>

    @foreach ($userCases as $userCase)
    <div class="user-case p-3 border rounded bg-white row no-gutters">
        <div class="user-case-profile col-2 d-flex flex-column justify-content-center align-items-center" onclick="showProfileModal({{ Auth::user()->role && Auth::user()->role->name == 'consultant' ? json_encode($userCase->user->toArray()) : json_encode($userCase->consultant->toArray()) }})">
            @if (!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user'))
            <img class="rounded-circle" src="{{ !is_null($userCase->consultant->avatar) && !empty($userCase->consultant->avatar) ?  $userCase->consultant->avatar_url : asset('img/user.png') }}" style="height: 64px; width: 64px; object-fit: contain; object-position: center;">
            @else
            <img class="rounded-circle" src="{{ !is_null($userCase->user->avatar) && !empty($userCase->user->avatar) ?  $userCase->user->avatar_url : asset('img/user.png') }}" style="height: 64px; width: 64px; object-fit: contain; object-position: center;">
            @endif

            <div class="mt-1 text-center">
                @if (!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user'))
                <p class="m-0" style="display: -webkit-box; -webkit-line-clamp: 1; -webkit-box-orient: vertical; overflow: hidden;">
                    <b>{{ $userCase->consultant->first_name.' '.$userCase->consultant->last_name }}</b>
                </p>

                <small>{{ ucfirst($userCase->consultant->role->name) }}</small>


                @else
                <p class="m-0" style="display: -webkit-box; -webkit-line-clamp: 1; -webkit-box-orient: vertical; overflow: hidden;">
                    <b>{{ $userCase->user->first_name.' '.$userCase->user->last_name }}</b>
                </p>

                <small>{{ ucfirst($userCase->user->role->name) }}</small>
                @endif
            </div>
        </div>

        <a class="col-7 pl-3" style="text-decoration: none; color: inherit;">
            <p style="font-size: 16px;" class="mb-1">
                <b>{{ $userCase->subject }}</b>
            </p>

            <div>

              {{ str_limit(strip_tags($userCase->question), $limit = 150, $end = '...') }}
            </div>
        </a>

        <div class="col-3 pl-3 text-right d-flex flex-column">
            <small class="text-muted">{{ $userCase->created_at }}</small>
            <small><b>Case Number: </b>{{ $userCase->id }}</small>
            <small class="ml-auto">
                <b>Status:</b>
                <span style="color: {{ $userCase->status == 'open' ? 'green' : 'red' }}">{{ strtoupper($userCase->status) }}</span>
            </small>

            <div class="mt-auto">
              <a class="btn btn-primary" href="{{ route('show-case', [ 'caseId' => $userCase->id ]) }}">
                  View
              </a>
                @if ($userCase->status == 'open')

                @can('update', $userCase)
                <a class="btn btn-warning" href="{{ route('edit-case', ['caseId' => $userCase->id]) }}">Edit</a>
                @endcan

                @can('updateStatus', $userCase)
                <button class="btn btn-danger" onclick="confirmCloseCase({{ $userCase->id }})">Close Case</button>
                <form id="update-case-form-{{ $userCase->id }}" method="POST" action="{{ route('update-case-status', [ 'caseId' => $userCase->id]) }}">
                    @csrf
                    @method('PUT')
                    <input type="hidden" name="status" value="closed" />
                </form>
                @endcan
                @endif
            </div>
        </div>
    </div>
    @endforeach

    <div class="d-flex align-items-center justify-content-end">
        @if (Request::get('status') == 'closed')
        {{ $userCases->appends(['status' => 'closed'])->links() }}
        @else
        {{ $userCases->links() }}
        @endif
    </div>
</div>

<div id="profile-modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content p-3 d-flex flex-colum align-items-center justify-content-center" style="position: relative;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute; top: 0; right: 8px;">
                <span aria-hidden="true" style="font-size: 30px;">&times;</span>
            </button>

            <img id="profile-avatar" class="rounded-circle" src="{{ asset('img/user-1.jpg') }}" style="height: 128px; width: 128px; object-fit: contain; object-position: center;">

            <div class="mt-1 text-center">
                <p class="m-0" style="font-size: 24px;">
                    <b id="profile-name">User Name</b>
                </p>

                <p id="profile-role" style="font-size: 18px;">User Role</p>

                <p id="profile-bio" style="font-size: 18px;">Bio</p>
            </div>
        </div>
    </div>
</div>
@endsection

@push('styles')
<link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css') }}">
<style>
    .user-case:not(:last-child) {
        margin-bottom: 8px;
    }

    .user-case-profile:hover {
        cursor: pointer;
    }

    .pagination {
        margin-bottom: 0;
    }
</style>
@endpush

@push('scripts')
<script src="{{ asset('js/sweetalert2.min.js') }}"></script>
<script>
    var defaultProfpic = "{!! asset("img/user.png") !!}"

    function showProfileModal (user) {
        $('#profile-modal #profile-name').html(user.first_name + ' ' + user.last_name)
        var userRole = user.role ? (user.role.name[0].toUpperCase() + user.role.name.substring(1)) : 'User';
        $('#profile-modal #profile-role').html(userRole)
        $('#profile-modal #profile-avatar').attr('src', (user.avatar ? user.avatar_url : defaultProfpic))
        $('#profile-modal #profile-bio').html(user.bio)
        $('#profile-modal').modal('show')
    }

    function confirmCloseCase (id) {
        swal({
            title: 'Close Case',
            text: 'Do you want to close this case?',
            type: 'question',
            showCancelButton: true,
            confirmButtonText: 'Yes'
        })
        .then(result => {
            if (!result.dismiss) {
                $('#update-case-form-'+id).submit()
            }
        })
    }
</script>
@endpush
