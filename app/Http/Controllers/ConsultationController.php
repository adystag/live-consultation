<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Category;
use App\Models\Consultation;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Notifications\NewConsultation;
use App\Events\ConsultationResponded;
use Illuminate\Support\Facades\Notification;
use App\Models\Setting;

class ConsultationController extends Controller
{
    public function index(Request $request)
    {
        $cases = $request->user()->cases;
        $consultationCount = $request->user()->consultations;
        $status = $request->get('status') && $request->get('status') == 'closed' ? [$request->get('status')] : ['active', 'suspended'];
        $point = Setting::where('setting_key', 'point')->first();

        if (!$request->user()->role || ($request->user()->role && $request->user()->role->name == 'user')) {
            $consultations = $request->user()->consultations()->with('consultant', 'consultant.role')->whereIn('status', $status)->paginate(15);
            $cases = $request->user()->cases;
            $userPayments = $request->user()->payments()->where('status', 'accepted')->get();
            $userPoints = ($userPayments->count() * (int) $point->setting_value) - ($cases->count() + $consultationCount->count());
        } else {
            $consultations = $request->user()->consultations()->with('user', 'user.role')->whereIn('status', $status)->paginate(15);
            $userPoints = 0;
        }

        $activeConsultationsCount = $consultationCount->filter(function ($value, $key) {
            return $value->status == 'active' || $value->status == 'suspended';
        });
        $closedConsultationsCount = $consultationCount->filter(function ($value, $key) {
            return $value->status == 'closed';
        });

        return view('consultations.index', [
            'consultations' => $consultations,
            'userPoints' => $userPoints,
            'activeConsultationsCount' => $activeConsultationsCount->count(),
            'closedConsultationsCount' => $closedConsultationsCount->count()
        ]);
    }

    public function compose(Request $request)
    {
        $cases = $request->user()->cases;
        $consultations = $request->user()->consultations;
        $point = Setting::where('setting_key', 'point')->first();
        $userPayments = $request->user()->payments()->where('status', 'accepted')->get();
        $userPoints = ($userPayments->count() * (int) $point->setting_value) - ($cases->count() + $consultations->count());

        if ($userPoints > 0) {
            $categories = Category::all();

            return view('consultations.compose', [
                'categories' => $categories
            ]);
        }

        return redirect()->route('index-payments')
            ->with('status', 'danger')
            ->with('status-message', 'Pleas add your point to make a live consultation.');
    }

    public function store(Request $request)
    {
        $validatedInput = $request->validate([
            'consultant_id' => 'required|exists:users,id'
        ]);

        $consultant = User::with(['consultations' => function ($query) {
            $query->where('status', 'active');
        }, 'role'])->find($validatedInput['consultant_id']);

        if (
            !$consultant || ($consultant->role && $consultant->role->name != 'consultant') || ($consultant->consultations->count() >= 1)
        ) {
            return redirect()->back()->with('status', 'danger')
                ->with('status-message', 'Invalid Consultant');
        }

        $consultation = Consultation::create([
            'user_id' => $request->user()->id,
            'consultant_id' => $validatedInput['consultant_id'],
            'time_remaining' => 3600000
        ]);
        $consultation->load('user', 'consultant');

        Notification::send($consultation->consultant, new NewConsultation($consultation));

        return redirect()
            ->route('show-consultation', ['consultationId' => $consultation->id])
            ->with('status', 'success')
            ->with('status-message', 'Consultation started.');
    }

    public function show(Request $request, $consultationId)
    {
        $consultation = Consultation::with('user', 'consultant')->find($consultationId);

        if (!$consultation) {
            return redirect()->route('index-consultations')
                ->with('status', 'danger')
                ->with('status-message', 'Consultation not found.');
        }

        $this->authorize('show', $consultation);

        $responds = $consultation->responds()->with('user', 'user.role')->get();
        $user = $request->user();
        $user->load('role');

        return view('consultations.show', [
            'user'  => $user,
            'consultation' => $consultation->toArray(),
            'responds' => $responds->toArray()
        ]);
    }

    public function update(Request $request, $consultationId)
    {
        $validatedInput = $request->validate([
            'status' => [
                'required',
                Rule::in(['active', 'suspended', 'closed']),
            ],
            'time_remaining' => 'required|numeric|max:3600000'
        ]);

        $consultation = Consultation::with('user', 'consultant')->find($consultationId);

        if (!$consultation) {
            return response()->json([
                'err'   => 'NotFound',
                'message'   => 'Consultation not found.'
            ], 404);
        }

        $this->authorize('update', $consultation);

        if ($consultation->status == 'closed') {
            return response()->json([
                'err'   => 'Invalid',
                'message'   => 'Consultation already closed.'
            ], 422);
        }

        $consultation->status = $validatedInput['status'];
        $consultation->time_remaining = $validatedInput['time_remaining'];
        $consultation->save();

        return response()->json($consultation, 200);
    }

    public function respond(Request $request, $consultationId)
    {
        $validatedData = $request->validate([
            'content' => 'required|string',
        ]);

        $consultation = Consultation::find($consultationId);

        if (!$consultation) {
            return response()->json([
                "err" => 'NotFound',
                "message" => 'Consultation not found.'
            ], 404);
        }

        $this->authorize('respond', $consultation);

        if ($consultation->status == 'closed' || $consultation->status == 'suspended') {
            return response()->json([
                "err" => 'Invalid',
                "message" => $consultation->status == 'suspended' ? 'Consultation is suspended.' : 'Consultation is closed.'
            ], 404);
        }

        $respond = $consultation->responds()->create([
            'content' => $validatedData['content'],
            'user_id' => $request->user()->id
        ]);
        $respond->load('user', 'user.role');

        broadcast(new ConsultationResponded($respond))->toOthers();

        return response()->json($respond, 200);
    }
}
