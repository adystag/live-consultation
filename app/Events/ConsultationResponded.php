<?php

namespace App\Events;

use App\Models\Respond;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ConsultationResponded implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    
    public $respond;
    
    /**
    * Create a new event instance.
    *
    * @return void
    */
    public function __construct(Respond $respond)
    {
        $this->respond = $respond;
    }
    
    /**
    * Get the channels the event should broadcast on.
    *
    * @return \Illuminate\Broadcasting\Channel|array
    */
    public function broadcastOn()
    {
        return new PresenceChannel("consultation.{$this->respond->respondable->id}");
    }
}
