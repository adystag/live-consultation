<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Consultation;
use Illuminate\Auth\Access\HandlesAuthorization;

class ConsultationPolicy
{
    use HandlesAuthorization;

    public function index (User $user)
    {
        return !$user->role || ($user->role &&( $user->role->name == 'user' || $user->role->name == 'consultant'));
    }
    
    public function create (User $user)
    {
        return !$user->role || ($user->role && $user->role->name == 'user');
    }

    protected function canManage (User $user, Consultation $consultation)
    {
        if (!$user->role) {
            return $user->id == $consultation->user_id;
        }

        if ($user->role) {
            if ($user->role->name == 'user') {
                return $user->id == $consultation->user_id;
            }
            
            if ($user->role->name == 'consultant') {
                return $user->id == $consultation->consultant_id;
            }
        }

        return false;
    }

    public function show (User $user, Consultation $consultation)
    {
        return $this->canManage($user, $consultation);
    }

    public function respond (User $user, Consultation $consultation)
    {
        return $this->canManage($user, $consultation);
    }

    public function update (User $user, Consultation $consultation)
    {
        return $this->canManage($user, $consultation);
    }
}
