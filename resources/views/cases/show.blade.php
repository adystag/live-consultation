@extends('layouts.app')

@section('main')
<div class="p-3 bg-white rounded shadow-sm">
    <h3 class="mb-3">Case Number: {{ $case->id }}</h3>

    @if (session('status'))
    <div class="alert alert-{{ session('status') }} alert-dismissible fade show" role="alert">
        {{ session('status-message') }}

        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

    <div class="rounded bg-white row no-gutters mb-3">
        <div class="user-case-profile col-2 d-flex flex-column justify-content-center align-items-center">
            <img class="rounded-circle" src="{{ !is_null($case->user->avatar) && !empty($case->user->avatar) ?  $case->user->avatar_url : asset('img/user.png') }}" style="height: 64px; width: 64px; object-fit: contain; object-position: center;">

            <div class="mt-1 text-center">
                <p class="m-0" style="display: -webkit-box; -webkit-line-clamp: 1; -webkit-box-orient: vertical; overflow: hidden;">
                    <b>{{ $case->user->first_name.' '.$case->user->last_name }}</b>
                </p>

                <small>{{ ucfirst($case->user->role->name) }}</small>
            </div>
        </div>

        <div class="col-7 pl-3">
            <p style="font-size: 16px;" class="mb-1">
                <b>{{ $case->subject }}</b>
            </p>

            <div>
                {!! $case->question !!}
            </div>
        </div>

        <div class="col-3 pl-3 text-right d-flex flex-column">
            <small class="text-muted">{{ $case->created_at }}</small>
            <small class="ml-auto">
                <b>Status:</b>
                <span style="color: {{ $case->status == 'open' ? 'green' : 'red' }}">{{ strtoupper($case->status) }}</span>
            </small>

            <div class="mt-auto">
                @if ($case->status == 'open')
                @can('update', $case)
                <a class="btn btn-warning" href="{{ route('edit-case', ['caseId' => $case->id]) }}">Edit</a>
                @endcan

                @can('updateStatus', $case)
                <button class="btn btn-danger" onclick="confirmCloseCase()">Close Case</button>
                <form id="update-case-form" method="POST" action="{{ route('update-case-status', [ 'caseId' => $case->id]) }}">
                    @csrf
                    @method('PUT')
                    <input type="hidden" name="status" value="closed" />
                </form>
                @endcan
                @endif
            </div>
        </div>
    </div>

    <div class="responds-wrapper">
        @foreach ($responds as $respond)
        <div class="case-respond border p-3 rounded bg-white row no-gutters" style="position: relative;">
            <div class="user-case-profile col-1 d-flex flex-column justify-content-center align-items-center">
                <img class="rounded-circle" src="{{ !is_null($respond->user->avatar) && !empty($respond->user->avatar) ?  $respond->user->avatar_url : asset('img/user.png') }}" style="height: 64px; width: 64px; object-fit: contain; object-position: center;">
            </div>

            <div class="col-10 pl-3">
                <p class="m-0">
                    <b>{{ $respond->user->first_name.' '.$respond->user->last_name }}</b>
                    <small class="ml-2">{{ ucfirst($respond->user->role ? $respond->user->role->name : 'user') }}</small>
                </p>

                {!! $respond->content !!}
            </div>

            <small class="text-muted" style="position: absolute; bottom: 16px; right: 16px;">{{ $case->created_at }}</small>
        </div>
        @endforeach
    </div>

    <div class="d-flex align-items-center justify-content-end mt-3">
        {{ $responds->links() }}
    </div>

    @if ($case->status == 'open')
    <div class="respond-form-wrapper mt-3">
        <form method="POST" action="{{ route('respond-case', [ 'caseId' => $case->id ]) }}">
            @csrf
            <div class="form-group">
                <textarea id="case-respond" name="content" rows="5" class="form-control{{ $errors->has('content') ? ' is-invalid' : '' }}">
                    {{ old('content') }}
                </textarea>

                @if ($errors->has('content'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('content') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group d-flex m-0">
                <button type="submit" class="ml-auto btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
    @endif
</div>
@endsection

@push('styles')
<style>
    .case-respond:not(:last-child) {
        margin-bottom: 8px;
    }

    .pagination {
        margin-bottom: 8px;
    }
</style>
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css') }}">
@endpush

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.js"></script>
<script src="{{ asset('js/sweetalert2.min.js') }}"></script>
<script>
    $(document).ready(function () {
        $('#case-respond').summernote({
            height: 200
        })
    })

    function confirmCloseCase () {
        swal({
            title: 'Close Case',
            text: 'Do you want to close this case?',
            type: 'question',
            showCancelButton: true,
            confirmButtonText: 'Yes'
        })
        .then(result => {
            if (!result.dismiss) {
                $('#update-case-form').submit()
            }
        })
    }
</script>
@endpush
