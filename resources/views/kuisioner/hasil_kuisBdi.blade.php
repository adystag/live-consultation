@extends('layouts.app')

@section('main')
<div class="p-3 rounded shadow-sm bg-white mb-3">
    <h3 class="mb-3">Jawaban Kuisioner</h3>
    
    @if (session('status'))
    <div class="alert alert-{{ session('status') }} alert-dismissible fade show mb-3" role="alert">
        {{ session('status-message') }}
        
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    {{ csrf_field() }}
    {{ method_field('PUT') }}

   
    <table class="table table-striped mb-0">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col"></th>
                    <th scope="col">Kondisi</th>
                    <th scope="col">Skor</th>
                </tr>
            </thead>

            <tbody>
                <input type="hidden" name="email" value="{{ Auth::user()->email }}">
                <tr>
                    <th>1</th>
                    <td>Kesedihan
                    </td>
                    <td>
                        Saya tidak merasa sedih
                    </td>
                    <td>{{ $hasil->d1 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                        Saya merasa sedih
                    </td>
                    <td>{{ $hasil->d2 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                        Saya merasa sedih sepanjang waktu dan saya tidak dapat menghilangkannya
                    </td>
                    <td>{{ $hasil->d3 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                        Saya begitu sedih sehingga saya merasa tidak tahan lagi
                    </td>
                    <td>{{ $hasil->d4 }}</td>
                </tr>
                <tr>
                    <th>2</th>
                    <td>Pesimis
                    </td>
                    <td>
                        Saya tidak merasa berkecil hati terhadap masa depan
                    </td>
                    <td>{{ $hasil->d5 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                        Saya merasa berkecil hati terhadap masa depan
                    </td>
                    <td>{{ $hasil->d6 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                        Saya merasa tidak ada sesuatu yang saya nantikan
                    </td>
                    <td>{{ $hasil->d7 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                        Saya merasa bahwa tidak ada harapan di masa depan dan segala sesuatunya tidak dapat diperbaiki
                    </td>
                    <td>{{ $hasil->d8 }}</td>
                </tr>
                <th>3</th>
                <td>Kegagalan
                </td>
                <td>
                        Saya tidak merasa gagal
                    </td>
                    <td>{{ $hasil->d9 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya merasa lebih banyak mengalami kegagalan daripada rata – rata orang
                        </td>
                        <td>{{ $hasil->d10 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Kalau saya meninjau kembali hidup saya, yang dapat saya lihat hanyalah banyak kegagalan
                        </td>
                        <td>{{ $hasil->d11 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya merasa sebagai seorang pribadi yang gagal total
                        </td>
                        <td>{{ $hasil->d12 }}</td>
                </tr>
                <th>4</th>
                <td>Kehilangan Kesenangan
                </td>
                <td>
                        Saya memperoleh kepuasan atas segala sesuatu seperti biasanya
                    </td>
                    <td>{{ $hasil->d13 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya tidak dapat menikmati segala sesuatu seperti biasanya
                        </td>
                        <td>{{ $hasil->d14 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya tidak lagi memperoleh kepuasan yang nyata dari segala sesuatu
                        </td>
                        <td>{{ $hasil->d15 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya merasa tidak puas atau bosan terhadap apa saja
                        </td>
                        <td>{{ $hasil->d16 }}</td>
                </tr>
                <th>5</th>
                <td>Perasaan Bersalah
                </td>
                <td>
                        Saya tidak merasa bersalah
                    </td>
                    <td>{{ $hasil->d17 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya cukup sering merasa bersalah
                        </td>
                        <td>{{ $hasil->d18 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya sering merasa sangat bersalah
                        </td>
                        <td>{{ $hasil->d19 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya merasa bersalah sepanjang waktu
                        </td>
                        <td>{{ $hasil->d20 }}</td>
                </tr>
                <th>6</th>
                <td>Perasaan akan Hukuman
                </td>
                <td>
                        Saya tidak merasa bahwa saya sedang dihukum
                    </td>
                    <td>{{ $hasil->d21 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya merasa bahwa saya mungkin dihukum
                        </td>
                        <td>{{ $hasil->d22 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya mengharapkan agar dihukum
                        </td>
                        <td>{{ $hasil->d23 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya merasa bahwa saya sedang dihukum
                        </td>
                        <td>{{ $hasil->d24 }}</td>
                </tr>
                <th>7</th>
                <td>Tidak Menyukai Diri Sendiri
                </td>
                <td>
                        Saya tidak merasa kecewa terhadap diri saya sendiri
                    </td>
                    <td>{{ $hasil->d25 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya merasa kecewa terhadap diri saya sendiri
                        </td>
                        <td>{{ $hasil->d26 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya merasa jijik terhadap diri saya sendiri
                        </td>
                        <td>{{ $hasil->d27 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya membenci diri saya sendiri
                        </td>
                        <td>{{ $hasil->d28 }}</td>
                </tr>
                <th>8</th>
                <td>Mengkritik Diri Sendiri
                </td>
                <td>
                        Saya tidak merasa bahwa saya lebih buruk daripada orang lain
                    </td>
                    <td>{{ $hasil->d29 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya selalu mencela diri saya sendiri karena kelemahan atau kekeliruan saya
                        </td>
                        <td>{{ $hasil->d30 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya menyalahkan diri saya sendiri sepanjang waktu atas kesalahan – kesalahan saya
                        </td>
                        <td>{{ $hasil->d31 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya menyalahkan diri saya sendiri atas semua hal buruk yang terjadi
                        </td>
                        <td>{{ $hasil->d32 }}</td>
                </tr>
                <th>9</th>
                <td>Pikiran atau Keinginan Bunuh Diri
                </td>
                <td>
                        Saya tidak mempunyai pikiran untuk bunuh diri
                    </td>
                    <td>{{ $hasil->d33 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya mempunyai pikiran – pikiran untuk bunuh diri, tetapi saya tidak akan melaksanakannya
                        </td>
                        <td>{{ $hasil->d34 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya ingin bunuh diri
                        </td>
                        <td>{{ $hasil->d35 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya akan bunuh diri kalau ada kesempatan
                        </td>
                        <td>{{ $hasil->d36 }}</td>
                </tr>
                <th>10</th>
                <td>Menangis
                </td>
                <td>
                        Saya tidak menangis lebih dari biasanya
                    </td>
                    <td>{{ $hasil->d37 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Sekarang saya lebih banyak menangis daripada biasanya
                        </td>
                        <td>{{ $hasil->d38 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Sekarang saya menangis sepanjang waktu
                        </td>
                        <td>{{ $hasil->d39 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya biasanya dapat menangis, tetapi sekarang saya tidak dapat menangis meskipun saya ingin menangis
                        </td>
                        <td>{{ $hasil->d40 }}</td>
                </tr>
                <th>11</th>
                <td>Pergolakan dalam Diri
                </td>
                <td>
                        Sekarang saya tidak merasa lebih jengkel daripada sebelumnya
                    </td>
                    <td>{{ $hasil->d41 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya lebih mudah jengkel atau marah daripada biasanya
                        </td>
                        <td>{{ $hasil->d42 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya sekarang merasa jengkel sepanjang waktu
                        </td>
                        <td>{{ $hasil->d43 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya tidak dibuat jengkel oleh hal – hal yang biasanya menjengkelkan saya
                        </td>
                        <td>{{ $hasil->d44 }}</td>
                </tr>
                <th>12</th>
                <td>Kehilangan Minat
                </td>
                <td>
                        Saya masih tetap senang bergaul dengan orang lain
                    </td>
                    <td>{{ $hasil->d45 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya kurang berminat pada orang lain dibandingkan dengan biasanya
                        </td>
                        <td>{{ $hasil->d46 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya tak kehilangan sebagian besar minat saya terhadap orang lain
                        </td>
                        <td>{{ $hasil->d47 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya telah kehilangan seluruh minat saya terhadap orang lain
                        </td>
                        <td>{{ $hasil->d48 }}</td>
                </tr>
                <th>13</th>
                <td>Bimbang/ragu-ragu
                </td>
                <td>
                        Saya mengambil keputusan – keputusan sama baiknya dengan sebelumnya
                    </td>
                    <td>{{ $hasil->d49 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya lebih banyak menunda keputusan daripada biasanya
                        </td>
                        <td>{{ $hasil->d50 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya mempunyai kesulitan yang lebih besar dalam mengambil keputusan daripada sebelumnya
                        </td>
                        <td>{{ $hasil->d51 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya sama sekali tidak dapat mengambil keputusan apa pun
                        </td>
                        <td>{{ $hasil->d52 }}</td>
                </tr>
                <th>14</th>
                <td>Perasaan Tidak Berharga
                </td>
                <td>
                        Saya tidak merasa bahwa saya kelihatan lebih jelek daripada sebelumnya
                    </td>
                    <td>{{ $hasil->d53 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya merasa cemas jangan – jangan saya tua atau tidak menarik
                        </td>
                        <td>{{ $hasil->d54 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya merasa bahwa ada perubahan – perubahan tetap pada penampilan saya yang membuat saya kelihatan tidak menarik
                        </td>
                        <td>{{ $hasil->d55 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya yakin bahwa saya kelihatan jelek
                        </td>
                        <td>{{ $hasil->d56 }}</td>
                </tr>
                <th>15</th>
                <td>Kehilangan Energi
                </td>
                <td>
                        Saya dapat bekerja dengan baik seperti sebelumnya
                    </td>
                    <td>{{ $hasil->d57 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya membutuhkan usaha istimewa untuk mulai mengerjakan sesuatu
                        </td>
                        <td>{{ $hasil->d58 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya harus memaksa diri saya untuk mengerjakan sesuatu
                        </td>
                        <td>{{ $hasil->d59 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya sama sekali tidak dapat mengerjakan apa – apa
                        </td>
                        <td>{{ $hasil->d60 }}</td>
                </tr>
                <th>16</th>
                <td>Kehilangan Pola Tidur
                </td>
                <td>
                        Saya dapat tidur nyenyak seperti biasanya
                    </td>
                    <td>{{ $hasil->d61 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya lebih mudah lelah dari biasanya
                        </td>
                        <td>{{ $hasil->d62 }}</td>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya bangun 2-3 jam lebih awal dari biasanya dan sukar tidur kembali
                        </td>
                        <td>{{ $hasil->d63 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya bangun beberapa jam lebih awal daripada biasanya dan tidak dapat tidur kembali
                        </td>
                        <td>{{ $hasil->d64 }}</td>
                </tr>
                <th>17</th>
                <td>Cepat Marah
                </td>
                <td>
                        Saya tidak lebih lelah dari biasanya
                    </td>
                    <td>{{ $hasil->d65 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya lebih mudah lelah dari biasanya
                        </td>
                        <td>{{ $hasil->d66 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya hampir selalu merasa lelah dalam mengerjakan segala sesuatu
                        </td>
                        <td>{{ $hasil->d67 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya merasa terlalu lelah untuk mengerjakan apa saja
                        </td>
                        <td>{{ $hasil->d68 }}</td>
                </tr>
                <th>18</th>
                <td>Perubahan Nafsu Makan
                </td>
                <td>
                        Nafsu makan saya masih seperti biasanya
                    </td>
                    <td>{{ $hasil->d69 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Nafsu makan saya tidak sebesar biasanya
                        </td>
                        <td>{{ $hasil->d70 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Sekarang nafsu makan saya jauh lebih berkurang
                        </td>
                        <td>{{ $hasil->d71 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya tidak mempunyai nafsu makan sama sekali
                        </td>
                        <td>{{ $hasil->d72 }}</td>
                </tr>
                <th>19</th>
                <td>Kesulitan Konsentrasi
                </td>
                <td>
                        Saya tidak banyak kehilangan berat badan akhir - akhir ini
                    </td>
                    <td>{{ $hasil->d73 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya telah kehilangan berat badan 2,5 kg lebih
                        </td>
                        <td>{{ $hasil->d74 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya telah kehilangan berat badan 5 kg lebih
                        </td>
                        <td>{{ $hasil->d75 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya telah kehilangan berat badan 7,5 kg lebih. Saya sengaja berusaha mengurangi berat badan dengan makan lebih sedikit :- ya – tidak
                        </td>
                        <td>{{ $hasil->d76 }}</td>
                </tr>
                <th>20</th>
                <td>Keletihan atau Kelelahan
                </td>
                <td>
                        Saya tidak mencemaskan kesehatan saya melebihi biasanya
                    </td>
                    <td>{{ $hasil->d77 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya cemas akan masalah kesehatan fisik saya, seperti sakit dan rasa nyeri; sakit perut; ataupun sembelit
                        </td>
                        <td>{{ $hasil->d78 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya sangat cemas akan masalah kesehatan fisik saya dan sulit memikirkan hal – hal lainnya
                        </td>
                        <td>{{ $hasil->d79 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya begitu cemas akan kesehatan fisik saya sehingga saya tidak dapat berpikir mengenai hal – hal lainnya
                        </td>
                        <td>{{ $hasil->d80 }}</td>
                </tr>
                <th>21</th>
                <td>Kehilangan Ketertarikan akan Seks
                </td>
                <td>
                        Saya tidak merasa ada perubahan dalam minat saya terhadap seks pada  akhir – akhir ini
                    </td>
                    <td>{{ $hasil->d81 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya kurang berminat terhadap seks kalau dibandingkan dengan biasanya
                        </td>
                        <td>{{ $hasil->d82 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Sekarang saya sangat kurang berminat terhadap seks
                        </td>
                        <td>{{ $hasil->d83 }}</td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td>
                            Saya sama sekali kehilangan minat terhadap seks
                        </td>
                        <td>{{ $hasil->d84 }}</td>
                </tr>
            </tbody>
        </table>
                <br>
            </div>

                    @if (!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant'))
                                    
                            <form method="post" action="/kuisioner/updateBdi/{{ $hasil->id }}">

                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                            <h3 class="mb-3">Keterangan</h3>
                        <textarea class="form-control" name="penjelasan" >{{ $hasil->penjelasan }}</textarea>
                        <input type="hidden" class="form-control" name="status" value="Selesai di Analisis"/>
                        <br>

                        <div class="form-group mb-0 d-flex">
                                <button type="submit" class="btn btn-primary ml-auto">Submit</button>
                            </div>
                        </form>
                    </div>

                    
            @elseif (!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user' && $hasil->status == 'Selesai di Analisis'))
            <div class="p-3 rounded shadow-sm bg-white mb-3">

                    <h3 class="mb-3">Keterangan</h3>
                <textarea class="form-control" name="penjelasan" >{{ $hasil->penjelasan }}</textarea>
                <input type="hidden" class="form-control" name="status" value="Selesai di Analisis"/>
                <br>
            </div>

            @endif

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
<link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css') }}">
@endpush

@push('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
<script src="{{ asset('js/sweetalert2.min.js') }}"></script>
<script>
    $(document).ready(function() {
    $('#table').DataTable( {
        "ordering": false
    } );
} );
    </script>
    @endpush

