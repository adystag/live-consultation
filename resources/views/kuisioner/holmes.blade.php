@extends('layouts.app')

@section('main')
<div class="p-3 rounded shadow-sm bg-white">
    <h3 class="mb-3">Holmes & Rahe
        </h3>
    
    @if (session('status'))
    <div class="alert alert-{{ session('status') }} alert-dismissible fade show mb-3" role="alert">
        {{ session('status-message') }}
        
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <form action="{{ route('tambahdataholmes')}}" method="POST">
            @csrf
    <table class="table table-striped mb-0">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Kondisi</th>
                <th scope="col">Y</th>
                <th scope="col">T</th>
            </tr>
        </thead>
        
        <tbody>
                <input type="hidden" name="email" value="{{ Auth::user()->email }}">
                <tr>
                    <th>1</th>
                    <td>Kematian suami/istri
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d1" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d1" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>2</th>
                    <td>Perceraian
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d2" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d2" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>3</th>
                    <td>Pemisahan perkawinan
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d3" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d3" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>4</th>
                    <td>Penjara
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d4" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d4" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>5</th>
                    <td>Kematian anggota keluarga dekat
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d5" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d5" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>6</th>
                    <td>Cedera atau sakit
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d6" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d6" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>7</th>
                    <td>Pernikahan
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d7" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d7" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>8</th>
                    <td>Dipecat dari tempat kerja
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d8" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d8" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>9</th>
                    <td>Perkawinan rekonsiliasi
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d9" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d9" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>10</th>
                    <td>Pensiun
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d10" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d10" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>11</th>
                    <td>Perubahan kesehatan anggota keluarga
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d11" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d11" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>12</th>
                    <td>Kehamilan
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d12" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d12" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>13</th>
                    <td>Kesulitan seks
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d13" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d13" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>14</th>
                    <td>Kehadiran anggota keluarga baru
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d14" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d14" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>15</th>
                    <td>Penyesuaian bisnis
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d15" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d15" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>16</th>
                    <td>Perubahan dalam status keuangan
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d16" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d16" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>17</th>
                    <td>Kematian teman dekat
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d17" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d17" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>18</th>
                    <td>Mutasi pekerjaan
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d18" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d18" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>19</th>
                    <td>Konflik dengan pasangan
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d19" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d19" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>20</th>
                    <td>Banyak hutang
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d20" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d20" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>21</th>
                    <td>Penyitaan harta
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d21" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d21" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>22</th>
                    <td>Perubahan tanggung jawab di tempat kerja
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d22" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d22" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>23</th>
                    <td>Anak meninggalkan rumah
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d23" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d23" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>24</th>
                    <td>Masalah dengan mertua
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d24" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d24" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>25</th>
                    <td>Posisi prestasi pribadi
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d25" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d25" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>26</th>
                    <td>Pasangan mulai atau berhenti bekerja
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d26" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d26" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>27</th>
                    <td>Memulai atau mengakhiri sekolah/kuliah
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d27" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d27" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>28</th>
                    <td>Perubahan kondisi hidup
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d28" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d28" value="0" checked="checked"></label></td> 
                </tr>
                <tr>
                    <th>29</th>
                    <td>Perubahan kebiasaan pribadi
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d29" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d29" value="0" checked="checked"></label></td>  
                </tr>
                <tr>
                    <th>30</th>
                    <td>Masalah dengan atasan
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d30" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d30" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>31</th>
                    <td>Perubahan kondisi atau jam kerja
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d31" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d31" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>32</th>
                    <td>Perubahan tempat tinggal
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d32" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d32" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>33</th>
                    <td>Perubahan di sekolah / perguruan tinggi
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d33" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d33" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>34</th>
                    <td>Perubahan rekreasi
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d34" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d34" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>35</th>
                    <td>Perubahan kegiatan ibadah
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d35" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d35" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>36</th>
                    <td>Perubahan kegiatan sosial
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d36" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d36" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>37</th>
                    <td>Hutang dalam jumlah kecil
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d37" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d37" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>38</th>
                    <td>Perubahan kebiasaan tidur
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d38" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d38" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>39</th>
                    <td>Perubahan kebiasaan silaturahmi keluarga
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d39" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d39" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>40</th>
                    <td>Perubahan kebiasaan makan
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d40" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d40" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>41</th>
                    <td>Liburan
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d41" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d41" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>42</th>
                    <td>Hari raya
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d42" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d42" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>43</th>
                    <td>Pelanggaran ringan
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d43" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d43" value="0" checked="checked"></label></td>
                </tr>
        </tbody>
    </table><br>
    <div class="form-group mb-0 d-flex">
            <button type="submit" class="btn btn-primary ml-auto">Submit</button>
        </div>
    </form>
        
                
                    
</div>

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
<link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css') }}">
@endpush

@push('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
<script src="{{ asset('js/sweetalert2.min.js') }}"></script>
<script>
    $(document).ready(function() {
    $('#table').DataTable( {
        "ordering": false
    } );
} );
    </script>
    @endpush

