<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCase extends Model
{
    protected $fillable = [
        'subject',
        'question',
        'status',
        'user_id',
        'consultant_id',
        'category_id'

    ];

    public function user ()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function consultant ()
    {
        return $this->belongsTo('App\Models\User', 'consultant_id');
    }

    public function responds ()
    {
        return $this->morphMany('App\Models\Respond', 'respondable');
    }

    public function category ()
    {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }


}
