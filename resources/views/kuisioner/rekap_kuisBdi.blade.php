@extends('layouts.app')

@section('main')
<div class="p-3 rounded shadow-sm bg-white">
        <nav class="nav nav-pills nav-justified">
                <a class="nav-item nav-link" href="/kuisioner/rekapDassD">Dass 42</a>
                <a class="nav-item nav-link" href="/kuisioner/rekapBdi">BDI</a>
                <a class="nav-item nav-link  active" href="/kuisioner/rekapKuisbdi">Kuis BDI</a>
              </nav>
              <hr>
    <h3 class="mb-3">Rekapitulasi Kuisioner Kuis BDI</h3>
    
    @if (session('status'))
    <div class="alert alert-{{ session('status') }} alert-dismissible fade show mb-3" role="alert">
        {{ session('status-message') }}
        
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
            @csrf
    
            @if (!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant'))
            <table class="table table-striped mb-0">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Email</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                
                <tbody>
                        <?php $i=1; ?>
                        @foreach($psikolog as $row)
                        <tr>
                                <td>{{ $i }}</td>
                                <td>{{ $row->email }}</td>
                                <td>
                                        <a href="/kuisioner/hasilKuisbdi/{{ $row->id }}" class="btn btn-success">Lihat</a>
                                </td>
                            </tr>
                            <?php $i++; ?>
                        @endforeach
                        
                </tbody>
            </table>
        
                @elseif (!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'admin'))
                <table class="table table-striped mb-0">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Email</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                            <?php $i=1; ?>
                            @foreach($pasien as $row)
                            <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $row->email }}</td>
                                    <td>
                                            <a href="/kuisioner/hasilKuisbdi/{{ $row->id }}" class="btn btn-success">Lihat</a>
                                        <a href="/kuisioner/hasilDass/{{ $row->id }}" class="btn btn-warning">Edit</a>
                                        <a href="/kuisioner/hapusKuisbdi/{{ $row->id }}" class="btn btn-danger">Hapus</a>
                                    </td>
                                </tr>
                                <?php $i++; ?>
                            @endforeach
                            
                    </tbody>
                </table>
                @endif 

    

        
                
                    
</div>

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
<link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css') }}">
@endpush

@push('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
<script src="{{ asset('js/sweetalert2.min.js') }}"></script>
<script>
    $(document).ready(function() {
    $('#table').DataTable( {
        "ordering": false
    } );
} );
    </script>
    @endpush

