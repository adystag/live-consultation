<nav class="navbar navbar-expand-lg navbar-light bg-white shadow-sm" style="height: 56px; z-index: 1;">
    <a class="navbar-brand" href="#">{{ config('app.name', 'Laravel') }}</a>
    
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
            {{-- <li class="nav-item d-flex align-items-center mr-3">
                <a class="nav-link" role="button" href="#">
                    <i class="far fa-bell fa-lg"></i>
                </a>
            </li> --}}
            
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle d-flex align-items-center" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img class="rounded-circle" src="{{ !is_null(Auth::user()->avatar) && !empty(Auth::user()->avatar) ?  Auth::user()->avatar_url : asset('img/user.png') }}" style="height: 32px; width: 32px; object-fit: contain; object-position: center;">
                </a>
                
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('edit-user-profile') }}">Profile</a>

                    <div class="dropdown-divider"></div>

                    <a class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        </ul>
    </div>
</nav>