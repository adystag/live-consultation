<?php

namespace App\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Models\Payment' => 'App\Policies\PaymentPolicy',
        'App\Models\UserCase' => 'App\Policies\UserCasePolicy',
        'App\Models\Consultation' => 'App\Policies\ConsultationPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('manage-categories', function ($user) {
            return $user->role && $user->role->name == 'admin';
        });
        Gate::define('update-user-role', function ($user) {
            return $user->role && $user->role->name == 'admin';
        });
        Gate::define('access-forum', function ($user) {
            return $user->role && ($user->role->name == 'consultant' || $user->role->name == 'admin');
        });
    }
}
