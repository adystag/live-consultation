<?php

namespace App\Http\Controllers;

use App\Events\ExampleEvent;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Models\UserCase;
use App\Models\Consultation;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function tac()
    {
        return view('tac');
    }

    public function history()
    {
        return view('history');
    }
    public function historyChat()
    {
        return view('historyChat');
    }
    public function getHistoryData(Request $request)
    {
      $me = auth()->user();
      if ($request->user()->role && $request->user()->role->name == 'user')
      {

          $users = UserCase::where('user_id',$me->id)->with('consultant','category')->get();

      } elseif($request->user()->role && $request->user()->role->name == 'consultant'){

            $users = UserCase::where('consultant_id',$me->id)->with('user','category')->get();
      }

        $users=$users->map(function($item, $key){
          $collection = collect($item);
          $collection->put('role_name', auth()->user()->role->name);

          if ($collection->get("role_name") == "user") {
            $collection->put("related", "{$item->consultant->first_name} {$item->consultant->last_name}");
          } else {
            $collection->put("related", "{$item->user->first_name} {$item->user->last_name}");
          }
          // dd($item);
          // return collect($item)->put('role_name', auth()->user()->role->name);
          return $collection;
        });

        return Datatables::of($users)->make(true);

    }

    public function getHistoryChatData(Request $request)
    {
      $me = auth()->user();
      if ($request->user()->role && $request->user()->role->name == 'user')
      {

            $users = Consultation::where('user_id',$me->id)->with('consultant')->get();

      } elseif($request->user()->role && $request->user()->role->name == 'consultant'){

            $users = Consultation::where('consultant_id',$me->id)->with('user')->get();
      }

        $users=$users->map(function($item, $key){
          return collect($item)->put('role_name', auth()->user()->role->name);
        });

        return Datatables::of($users)->make(true);

    }



}
