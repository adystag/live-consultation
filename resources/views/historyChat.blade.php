@extends('layouts.app')

@section('main')
<div class="card">
    <div class="card-header">History</div>

    <div class="card-body">
        @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
        @endif

<div>

  <table class="table table-bordered" id="history-table">
    @if (!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user'))
        <thead>
            <tr>
                <th>Id</th>
                <th>Consultant</th>
                <th>Time</th>
                <th>Date</th>
            </tr>
        </thead>
        @elseif (!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant'))
        <thead>
            <tr>
              <tr>
                  <th>Id</th>
                  <th>Client</th>
                  <th>Time</th>
                  <th>Date</th>
              </tr>
            </tr>
        </thead>
        @endif

    </table>

</div>

    </div>
</div>
@endsection

@push('scripts')
<script>

  $(function() {
      $('#history-table').DataTable({
          processing: true,
          serverSide: true,
          ajax: '{!! route('historyChat.data') !!}',
          columns: [

              { data: 'id', name: 'id' },
              { data: null, render: function ( data, type, row ) {
                if (data.role_name =='consultant') {
                    return data.user.first_name+' '+data.user.last_name;
                }else {
                    return data.consultant.first_name+' '+data.consultant.last_name;
                }

              }
            },
              { data: 'time_remaining', name: 'time_remaining' },
              { data: 'created_at', name: 'created_at' },
          ]
      });
  });


</script>
@endpush
