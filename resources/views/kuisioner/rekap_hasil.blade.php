@extends('layouts.app')

@section('main')
<div class="p-3 rounded shadow-sm bg-white">
    <h3 class="mb-3">HARS</h3>
    
    @if (session('status'))
    <div class="alert alert-{{ session('status') }} alert-dismissible fade show mb-3" role="alert">
        {{ session('status-message') }}
        
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
            @csrf
    Depresi
    <table class="table table-striped mb-0">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col" colspan="2" style="text-align:center">Depresi</th>
                <th scope="col"colspan="3" style="text-align:center">Kecemasan</th>
                <th scope="col" colspan="2" style="text-align:center">Stress</th>
            </tr>
            <tr>
                <th scope="col"></th>
                <th scope="col">DASS 42</th>
                <th scope="col">BDI</th>
                <th scope="col">DASS 42</th>
                <th scope="col">TMAS</th>
                <th scope="col">HARS</th>
                <th scope="col">DASS 42</th>
                <th scope="col">HOLMES</th>
            </tr>
        </thead>
        
        <tbody>
                @foreach($dass as $row)
                <tr>
                    <th>1</th>
                    <td>{{ $row->tingkat_d }}</td>
                    <td>{{ $row->tingkat }}</td>
                    <td>{{ $row->tingkat_a }}</td>
                    <td></td>
                    <td></td>
                    <td>{{ $row->tingkat_s }}</td>
                    <td></td>
                </tr>
                @endforeach
        </tbody>
    </table>
        
                
                    
</div>

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
<link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css') }}">
@endpush

@push('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
<script src="{{ asset('js/sweetalert2.min.js') }}"></script>
<script>
    $(document).ready(function() {
    $('#table').DataTable( {
        "ordering": false
    } );
} );
    </script>
    @endpush

