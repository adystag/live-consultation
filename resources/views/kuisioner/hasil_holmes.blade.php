@extends('layouts.app')

@section('main')
<div class="p-3 rounded shadow-sm bg-white mb-3">
    <h3 class="mb-3">Jawaban Kuisioner</h3>
    
    @if (session('status'))
    <div class="alert alert-{{ session('status') }} alert-dismissible fade show mb-3" role="alert">
        {{ session('status-message') }}
        
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    {{ csrf_field() }}
    {{ method_field('PUT') }}

   
    <table id="table" class="table table-striped mb-0">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Kondisi</th>
                            <th scope="col">Skor</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                            <tr>
                                    <th>1</th>
                                    <td>Kematian suami/istri
                                    </td>
                                    <?php if ($hasil->d1 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>2</th>
                                    <td>Perceraian
                                    </td>
                                    <?php if ($hasil->d2 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>3</th>
                                    <td>Pemisahan perkawinan
                                    </td>
                                    <?php if ($hasil->d3 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>4</th>
                                    <td>Penjara
                                    </td>
                                    <?php if ($hasil->d4 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>5</th>
                                    <td>Kematian anggota keluarga dekat
                                    </td>
                                    <?php if ($hasil->d5 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>6</th>
                                    <td>Cedera atau sakit
                                    </td>
                                    <?php if ($hasil->d6 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>7</th>
                                    <td>Pernikahan
                                    </td>
                                    <?php if ($hasil->d7 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>8</th>
                                    <td>Dipecat dari tempat kerja
                                    </td>
                                    <?php if ($hasil->d8 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>9</th>
                                    <td>Perkawinan rekonsiliasi
                                    </td>
                                    <?php if ($hasil->d9 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>10</th>
                                    <td>Pensiun
                                    </td>
                                    <?php if ($hasil->d10 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>11</th>
                                    <td>Perubahan kesehatan anggota keluarga
                                    </td>
                                    <?php if ($hasil->d11 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>12</th>
                                    <td>Kehamilan
                                    </td>
                                    <?php if ($hasil->d12 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>13</th>
                                    <td>Kesulitan seks
                                    </td>
                                    <?php if ($hasil->d13 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>14</th>
                                    <td>Kehadiran anggota keluarga baru
                                    </td>
                                    <?php if ($hasil->d14 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>15</th>
                                    <td>Penyesuaian bisnis
                                    </td>
                                    <?php if ($hasil->d15 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>16</th>
                                    <td>Perubahan dalam status keuangan
                                    </td>
                                    <?php if ($hasil->d16 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>17</th>
                                    <td>Kematian teman dekat
                                    </td>
                                    <?php if ($hasil->d17 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>18</th>
                                    <td>Mutasi pekerjaan
                                    </td>
                                    <?php if ($hasil->d18 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>19</th>
                                    <td>Konflik dengan pasangan
                                    </td>
                                    <?php if ($hasil->d19 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>20</th>
                                    <td>Banyak hutang
                                    </td>
                                    <?php if ($hasil->d20 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>21</th>
                                    <td>Penyitaan harta
                                    </td>
                                    <?php if ($hasil->d21 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>22</th>
                                    <td>Perubahan tanggung jawab di tempat kerja
                                    </td>
                                    <?php if ($hasil->d22 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>23</th>
                                    <td>Anak meninggalkan rumah
                                    </td>
                                    <?php if ($hasil->d23 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>24</th>
                                    <td>Masalah dengan mertua
                                    </td>
                                    <?php if ($hasil->d24 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>25</th>
                                    <td>Posisi prestasi pribadi
                                    </td>
                                    <?php if ($hasil->d25 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>26</th>
                                    <td>Pasangan mulai atau berhenti bekerja
                                    </td>
                                    <?php if ($hasil->d26 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>27</th>
                                    <td>Memulai atau mengakhiri sekolah/kuliah
                                    </td>
                                    <?php if ($hasil->d27 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>28</th>
                                    <td>Perubahan kondisi hidup
                                    </td>
                                    <?php if ($hasil->d28 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>29</th>
                                    <td>Perubahan kebiasaan pribadi
                                    </td>
                                    <?php if ($hasil->d29 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>30</th>
                                    <td>Masalah dengan atasan
                                    </td>
                                    <?php if ($hasil->d30 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>31</th>
                                    <td>Perubahan kondisi atau jam kerja
                                    </td>
                                    <?php if ($hasil->d31 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>32</th>
                                    <td>Perubahan tempat tinggal
                                    </td>
                                    <?php if ($hasil->d32 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>33</th>
                                    <td>Perubahan di sekolah / perguruan tinggi
                                    </td>
                                    <?php if ($hasil->d33 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>34</th>
                                    <td>Perubahan rekreasi
                                    </td>
                                    <?php if ($hasil->d34 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>35</th>
                                    <td>Perubahan kegiatan ibadah
                                    </td>
                                    <?php if ($hasil->d35 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>36</th>
                                    <td>Perubahan kegiatan sosial
                                    </td>
                                    <?php if ($hasil->d36 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>37</th>
                                    <td>Hutang dalam jumlah kecil
                                    </td>
                                    <?php if ($hasil->d37 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>38</th>
                                    <td>Perubahan kebiasaan tidur
                                    </td>
                                    <?php if ($hasil->d38 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>39</th>
                                    <td>Perubahan kebiasaan silaturahmi keluarga
                                    </td>
                                    <?php if ($hasil->d39 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>40</th>
                                    <td>Perubahan kebiasaan makan
                                    </td>
                                    <?php if ($hasil->d40 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>41</th>
                                    <td>Liburan
                                    </td>
                                    <?php if ($hasil->d41 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>42</th>
                                    <td>Hari raya
                                    </td>
                                    <?php if ($hasil->d42 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                                <tr>
                                    <th>43</th>
                                    <td>Pelanggaran ringan
                                    </td>
                                    <?php if ($hasil->d43 == '1') {
                                        $hasilnya = 'Ya';
                                    } else {
                                        $hasilnya = 'Tidak';
                                    }
                                      ?>
                                    <td>{{ $hasilnya }}</td> 
                                </tr>
                    </tbody>
                </table>
                <br>
                <hr>
            </div>

            

                    @if (!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant'))
                    <div class="p-3 rounded shadow-sm bg-white">
                            <h3 class="mb-3">Hasil Kuisioner</h3>
                        <table class="table table-striped mb-0">  
                                <thead>
                                        <tr>
                                            <th scope="col"></th>
                                            <th scope="col">Skor</th>
                                            <th scope="col">Tingkat</th>
                                        </tr>
                                    </thead>                 
                                <tbody>
                                        <tr>
                                            <td>Stress</td>
                                            <td>{{ $hasil->total }}</td>
                                            <td>{{ $hasil->tingkat }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                    <div class="p-3 rounded shadow-sm bg-white mb-3">
                            <form method="post" action="/kuisioner/updateHolmes/{{ $hasil->id }}">

                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <h3 class="mb-3">Keterangan</h3>
                                <textarea class="form-control" name="penjelasan" >{{ $hasil->penjelasan }}</textarea>
                        <input type="hidden" class="form-control" name="status" value="Selesai di Analisis"/>
                        <br>

                        <div class="form-group mb-0 d-flex">
                                <button type="submit" class="btn btn-primary ml-auto">Submit</button>
                            </div>
                        </form>
                    </div>

                    
            @elseif (!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user' && $hasil->status == 'Selesai di Analisis'))
            <div class="p-3 rounded shadow-sm bg-white">
                    <h3 class="mb-3">Hasil Kuisioner</h3>
                <table class="table table-striped mb-0">  
                        <thead>
                                <tr>
                                    <th scope="col"></th>
                                    <th scope="col">Skor</th>
                                    <th scope="col">Tingkat</th>
                                </tr>
                            </thead>                 
                        <tbody>
                                <tr>
                                    <td>Stress</td>
                                    <td>{{ $hasil->total }}</td>
                                    <td>{{ $hasil->tingkat }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    
            <div class="p-3 rounded shadow-sm bg-white mb-3">
                        <h3 class="mb-3">Keterangan</h3>
                        <textarea class="form-control" name="penjelasan" >{{ $hasil->penjelasan }}</textarea>
                <input type="hidden" class="form-control" name="status" value="Selesai di Analisis"/>
                <br>

            @endif
                    


@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
<link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css') }}">
@endpush

@push('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
<script src="{{ asset('js/sweetalert2.min.js') }}"></script>
<script>
    $(document).ready(function() {
    $('#table').DataTable( {
        "ordering": false
    } );
} );
    </script>
    @endpush

