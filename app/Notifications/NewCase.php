<?php

namespace App\Notifications;

use Carbon\Carbon;
use App\Models\UserCase;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;

class NewCase extends Notification
{
    use Queueable;
    
    protected $case;
    
    /**
    * Create a new notification instance.
    *
    * @return void
    */
    public function __construct(UserCase $case)
    {
        $this->case = $case;
    }
    
    /**
    * Get the notification's delivery channels.
    *
    * @param  mixed  $notifiable
    * @return array
    */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }
    
    /**
    * Get the array representation of the notification.
    *
    * @param  mixed  $notifiable
    * @return array
    */
    public function toDatabase($notifiable)
    {
        $data = $this->case->toArray();
        $data['case_id'] = $this->case->id;
        
        return $data;
    }
    
    /**
    * Get the broadcastable representation of the notification.
    *
    * @param  mixed  $notifiable
    * @return BroadcastMessage
    */
    public function toBroadcast($notifiable)
    {
        $case = $this->case->toArray();
        $case['case_id'] = $this->case->id;
        $case['notified_at'] = Carbon::now()->toDateString();
        $case['isRead'] = false;

        return new BroadcastMessage($case);
    }
}
    