<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Payment;
use Illuminate\Auth\Access\HandlesAuthorization;

class PaymentPolicy
{
    use HandlesAuthorization;

    public function index (User $user)
    {
        return !$user->role || ($user->role && ($user->role->name == 'user' || $user->role->name == 'admin'));
    }

    public function update (User $user)
    {
        return $user->role && $user->role->name == 'admin';
    }

    public function create (User $user)
    {
        return !$user->role || ($user->role && $user->role->name == 'user');
    }

    public function view (User $user, Payment $payment)
    {
        return ($user->id == $payment->user_id) || ($user->role && $user->role->name == 'admin');
    }
}
