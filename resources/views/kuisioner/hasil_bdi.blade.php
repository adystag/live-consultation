@extends('layouts.app')

@section('main')
<div class="p-3 rounded shadow-sm bg-white mb-3">
    <h3 class="mb-3">Jawaban Kuisioner</h3>
    
    @if (session('status'))
    <div class="alert alert-{{ session('status') }} alert-dismissible fade show mb-3" role="alert">
        {{ session('status-message') }}
        
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    {{ csrf_field() }}
    {{ method_field('PUT') }}

   
    <table id="table" class="table table-striped mb-0">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Item</th>
                            <th scope="col">Kondisi</th>
                            <th scope="col">Skor</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                            <tr>
                                    <th>1</th>
                                    <td>Kesedihan
                                    </td>
                                    <td>Saya begitu sedih sehingga saya merasa tidak tahan lagi
                                    </td>
                                    <td>{{ $hasil->d1 }}</td>  
                                </tr>
                                <tr>
                                    <th>2</th>
                                    <td>Pesimis
                                    </td>
                                    <td>Saya merasa tidak ada sesuatu yang saya nantikan
                                    </td>
                                    <td>{{ $hasil->d2 }}</td>  
                                </tr>
                                <tr>
                                    <th>3</th>
                                    <td>Kegagalan
                                    </td>
                                    <td>Saya merasa lebih banyak mengalami kegagalan daripada rata – rata orang
                                    </td>
                                    <td>{{ $hasil->d3 }}</td>  
                                </tr>
                                <tr>
                                    <th>4</th>
                                    <td>Kehilangan Kesenangan
                                    </td>
                                    <td>Saya tidak dapat menikmati segala sesuatu seperti biasanya
                                    </td>
                                    <td>{{ $hasil->d4 }}</td>  
                                </tr>
                                <tr>
                                    <th>5</th>
                                    <td>Perasaan Bersalah
                                    </td>
                                    <td>Saya cukup sering merasa bersalah
                                    </td>
                                    <td>{{ $hasil->d5 }}</td>  
                                </tr>
                                <tr>
                                    <th>6</th>
                                    <td>Perasaan akan Hukuman
                                    </td>
                                    <td>Saya merasa bahwa saya sedang dihukum
                                    </td>
                                    <td>{{ $hasil->d6 }}</td>  
                                </tr>
                                <tr>
                                    <th>7</th>
                                    <td>Tidak Menyukai Diri Sendiri
                                    </td>
                                    <td>Saya tidak merasa kecewa terhadap diri saya sendiri
                                    </td>
                                    <td>{{ $hasil->d7 }}</td>  
                                </tr>
                                <tr>
                                    <th>8</th>
                                    <td>Mengkritik Diri Sendiri
                                    </td>
                                    <td>Saya selalu mencela diri saya sendiri karena kelemahan atau kekeliruan saya
                                    </td>
                                    <td>{{ $hasil->d8 }}</td>  
                                </tr>
                                <tr>
                                    <th>9</th>
                                    <td>Pikiran atau Keinginan Bunuh Diri
                                    </td>
                                    <td>Saya ingin bunuh diri
                                    </td>
                                    <td>{{ $hasil->d9 }}</td>  
                                </tr>
                                <tr>
                                    <th>10</th>
                                    <td>Menangis
                                    </td>
                                    <td>Saya biasanya dapat menangis, tetapi sekarang saya tidak dapat menangis meskipun saya ingin menangis
                                    </td>
                                    <td>{{ $hasil->d10 }}</td>  
                                </tr>
                                <tr>
                                    <th>11</th>
                                    <td>Pergolakan dalam Diri
                                    </td>
                                    <td>Saya tidak dibuat jengkel oleh hal – hal yang biasanya menjengkelkan saya
                                    </td>
                                    <td>{{ $hasil->d11 }}</td>  
                                </tr>
                                <tr>
                                    <th>12</th>
                                    <td>Kehilangan Minat
                                    </td>
                                    <td>Saya masih tetap senang bergaul dengan orang lain
                                    </td>
                                    <td>{{ $hasil->d12 }}</td>  
                                </tr>
                                <tr>
                                    <th>13</th>
                                    <td>Bimbang/ragu-ragu
                                    </td>
                                    <td>Saya lebih banyak menunda keputusan daripada biasanya
                                    </td>
                                    <td>{{ $hasil->d13 }}</td>  
                                </tr>
                                <tr>
                                    <th>14</th>
                                    <td>Perasaan Tidak Berharga
                                    </td>
                                    <td>Saya tidak merasa bahwa saya kelihatan lebih jelek daripada sebelumnya
                                    </td>
                                    <td>{{ $hasil->d14 }}</td>  
                                </tr>
                                <tr>
                                    <th>15</th>
                                    <td>Kehilangan Energi
                                    </td>
                                    <td>Saya membutuhkan usaha istimewa untuk mulai mengerjakan sesuatu
                                    </td>
                                    <td>{{ $hasil->d15 }}</td>  
                                </tr>
                                <tr>
                                    <th>16</th>
                                    <td>Kehilangan Pola Tidur
                                    </td>
                                    <td>Saya bangun beberapa jam lebih awal daripada biasanya dan tidak dapat tidur kembali
                                    </td>
                                    <td>{{ $hasil->d16 }}</td>  
                                </tr>
                                <tr>
                                    <th>17</th>
                                    <td>Cepat Marah
                                    </td>
                                    <td>Saya merasa terlalu lelah untuk mengerjakan apa saja
                                    </td>
                                    <td>{{ $hasil->d17 }}</td>  
                                </tr>
                                <tr>
                                    <th>18</th>
                                    <td>Perubahan Nafsu Makan
                                    </td>
                                    <td>Nafsu makan saya masih seperti biasanya
                                    </td>
                                    <td>{{ $hasil->d18 }}</td>  
                                </tr>
                                <tr>
                                    <th>19</th>
                                    <td>Kesulitan Konsentrasi
                                    </td>
                                    <td>Saya telah kehilangan berat badan 2,5 kg lebih
                                    </td>
                                    <td>{{ $hasil->d19 }}</td>  
                                </tr>
                                <tr>
                                    <th>20</th>
                                    <td>Keletihan atau Kelelahan
                                    </td>
                                    <td>Saya sangat cemas akan masalah kesehatan fisik saya dan sulit memikirkan hal – hal lainnya
                                    </td>
                                    <td>{{ $hasil->d20 }}</td>  
                                </tr>
                                <tr>
                                    <th>21</th>
                                    <td>Kehilangan Ketertarikan akan Seks
                                    </td>
                                    <td>Saya sama sekali kehilangan minat terhadap seks
                                    </td>
                                    <td>{{ $hasil->d21 }}</td>  
                                </tr>
                    </tbody>
                </table>
                <br>
            </div>

            
                    
                    @if (!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant'))
                    <div class="p-3 rounded shadow-sm bg-white">
                            <h3 class="mb-3">Hasil Kuisioner</h3>
                        <table class="table table-striped mb-0">  
                                <thead>
                                        <tr>
                                            <th scope="col"></th>
                                            <th scope="col">Skor</th>
                                            <th scope="col">Tingkat</th>
                                        </tr>
                                    </thead>                 
                                <tbody>
                                        <tr>
                                            <td>Depresi</td>
                                            <td>{{ $hasil->total }}</td>
                                            <td>{{ $hasil->tingkat }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <hr>
                                <br>
                            </div>

                    
                                    
                            <form method="post" action="/kuisioner/updateBdi/{{ $hasil->id }}">

                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                            <h3 class="mb-3">Keterangan</h3>
                        <textarea class="form-control" name="penjelasan" >{{ $hasil->penjelasan }}</textarea>
                        <input type="hidden" class="form-control" name="status" value="Selesai di Analisis"/>
                        <br>

                        <div class="form-group mb-0 d-flex">
                                <button type="submit" class="btn btn-primary ml-auto">Submit</button>
                            </div>
                        </form>
                    </div>

                    
            @elseif (!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user' && $hasil->status == 'Selesai di Analisis'))
            <div class="p-3 rounded shadow-sm bg-white mb-3">
                    <div class="p-3 rounded shadow-sm bg-white">
                            <h3 class="mb-3">Hasil Kuisioner</h3>
                        <table class="table table-striped mb-0">  
                                <thead>
                                        <tr>
                                            <th scope="col"></th>
                                            <th scope="col">Skor</th>
                                            <th scope="col">Tingkat</th>
                                        </tr>
                                    </thead>                 
                                <tbody>
                                        <tr>
                                            <td>Depresi</td>
                                            <td>{{ $hasil->total }}</td>
                                            <td>{{ $hasil->tingkat }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <hr>
                                <br>
                            </div>
            <div class="p-3 rounded shadow-sm bg-white mb-3">

                    <h3 class="mb-3">Keterangan</h3>
                <textarea class="form-control" name="penjelasan" >{{ $hasil->penjelasan }}</textarea>
                <input type="hidden" class="form-control" name="status" value="Selesai di Analisis"/>
                <br>
            </div>

            @endif

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
<link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css') }}">
@endpush

@push('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
<script src="{{ asset('js/sweetalert2.min.js') }}"></script>
<script>
    $(document).ready(function() {
    $('#table').DataTable( {
        "ordering": false
    } );
} );
    </script>
    @endpush

