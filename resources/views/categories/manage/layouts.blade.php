@extends('layouts.app')

@section('main')
<div class="p-3 rounded shadow-sm mb-3 bg-white">
    <h3 class="mb-3">Manage {{ $category->name }}</h3>
    
    @if (session('status'))
    <div class="alert alert-{{ session('status') }} alert-dismissible fade show mb-3" role="alert">
        {{ session('status-message') }}
        
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    
    <form class="form" id="category-form" method="POST" action="{{ route('update-category', [ 'categoryId' => $category->id ]) }}">
        @csrf
        @method('PUT')
        
        <div class="form-group">
            <label class="mr-4">Name</label>
            <input type="text" name="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="Enter category name" value="{{ $category->name }}">
            
            @if ($errors->has('name'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
            @endif
        </div>
        
        <div class="form-group mb-0 d-flex">
            <button type="submit" class="btn btn-primary ml-auto">Save</button>
        </div>
    </form>
</div>

<div class="p-3 rounded shadow-sm bg-white">
    <h3 class="mb-3">Manage {{ $category->name }} Members</h3>
    
    @if (session('status-members'))
    <div class="alert alert-{{ session('status-members') }} alert-dismissible fade show mb-3" role="alert">
        {{ session('status-members-message') }}
        
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    
    <ul class="nav nav-tabs nav-fill mb-3">
        <li class="nav-item">
            <a class="nav-link {{ Request::route()->getName() == 'show-category' ? 'active' : '' }}" href="{{ route('show-category', [ 'categoryId' => $category->id ]) }}">Manage</a>
        </li>
        
        <li class="nav-item">
            <a class="nav-link {{ Request::route()->getName() == 'show-users-candidate' ? 'active' : '' }}" href="{{ route('show-users-candidate', [ 'categoryId' => $category->id ]) }}">Add Member</a>
        </li>
    </ul>
    
    @yield('members')
</div>
@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
<link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css') }}">
@endpush

@push('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
<script src="{{ asset('js/sweetalert2.min.js') }}"></script>
<script>
    $(document).ready(function () {
        $('#category-members-table').DataTable();
    })
</script>
@endpush