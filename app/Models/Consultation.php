<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Consultation extends Model
{
    protected $fillable = [
        'status',
        'time_remaining',
        'user_id',
        'consultant_id'
    ];

    public function user ()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function consultant ()
    {
        return $this->belongsTo('App\Models\User', 'consultant_id');
    }

    public function responds ()
    {
        return $this->morphMany('App\Models\Respond', 'respondable');
    }
}
