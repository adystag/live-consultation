<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

// Broadcast::channel('App.User.{id}', function ($user, $id) {
//     return (int) $user->id === (int) $id;
// });

Broadcast::channel('consultation', function () {
    if (Auth::check()) {
        $user = Auth::user();

        return [
            'id' => $user->id,
            'fist_name' => $user->first_name,
            'last_name' => $user->last_name
        ];
    }
});

Broadcast::channel('consultation.{consultationId}', function ($user, $consultationId) {
    if (Auth::check()) {
        $canJoin = false;
        $consultation = App\Models\Consultation::find($consultationId);

        if ($consultation && $consultation->status != 'closed') {
            if (!$user->role || ($user->role && $user->role->name == 'user')) {
                if ($consultation->user_id == $user->id) {
                   $canJoin = true;
                }
            }

            if ($user->role && $user->role->name == 'consultant') {
                if ($consultation->consultant_id == $user->id) {
                    $canJoin = true;
                }
            }
        }

        if ($canJoin) {
            return [
                'id' => $user->id,
                'fist_name' => $user->first_name,
                'last_name'  => $user->last_name
            ];
        }
    }
});

Broadcast::channel('App.Models.User.{userId}', function ($user, $userId) {
    return $user->id == $userId;
});
