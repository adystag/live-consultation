<?php

use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userRole = Role::where('name', 'user')->first();
        $adminRole = Role::where('name', 'admin')->first();
        $consultantRole = Role::where('name', 'consultant')->first();
        $password = Hash::make('123456');

        DB::table('users')->insert([
            'email' => 'admin@email.com',
            'first_name' => 'Admin',
            'last_name' => 'Consulta',
            'password' => $password,
            'role_id'  => $adminRole->id
        ]);

        DB::table('users')->insert([
            'email' => 'klien@email.com',
            'first_name' => 'Fathul',
            'last_name' => 'Khair',
            'password' => $password,
            'role_id'  => $userRole->id
        ]);

        DB::table('users')->insert([
            'email' => 'psikolog@email.com',
            'first_name' => 'Hepi',
            'last_name' => 'Wahyuningsih S.Psi., M.Si.',
            'password' => $password,
            'role_id'  => $consultantRole->id
        ]);
    }
}
