<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKuisbdisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kuisbdis', function (Blueprint $table) {
            $table->increments('id_user_kuisbdi');
            $table->string('email');
            $table->integer('d1')->length(1);
            $table->integer('d2')->length(1);
            $table->integer('d3')->length(1);
            $table->integer('d4')->length(1);
            $table->integer('d5')->length(1);
            $table->integer('d6')->length(1);
            $table->integer('d7')->length(1);
            $table->integer('d8')->length(1);
            $table->integer('d9')->length(1);
            $table->integer('d10')->length(1);
            $table->integer('d11')->length(1);
            $table->integer('d12')->length(1);
            $table->integer('d13')->length(1);
            $table->integer('d14')->length(1);
            $table->integer('d15')->length(1);
            $table->integer('d16')->length(1);
            $table->integer('d17')->length(1);
            $table->integer('d18')->length(1);
            $table->integer('d19')->length(1);
            $table->integer('d20')->length(1);
            $table->integer('d21')->length(1);
            $table->integer('d22')->length(1);
            $table->integer('d23')->length(1);
            $table->integer('d24')->length(1);
            $table->integer('d25')->length(1);
            $table->integer('d26')->length(1);
            $table->integer('d27')->length(1);
            $table->integer('d28')->length(1);
            $table->integer('d29')->length(1);
            $table->integer('d30')->length(1);
            $table->integer('d31')->length(1);
            $table->integer('d32')->length(1);
            $table->integer('d33')->length(1);
            $table->integer('d34')->length(1);
            $table->integer('d35')->length(1);
            $table->integer('d36')->length(1);
            $table->integer('d37')->length(1);
            $table->integer('d38')->length(1);
            $table->integer('d39')->length(1);
            $table->integer('d40')->length(1);
            $table->integer('d41')->length(1);
            $table->integer('d42')->length(1);
            $table->integer('d43')->length(1);
            $table->integer('d44')->length(1);
            $table->integer('d45')->length(1);
            $table->integer('d46')->length(1);
            $table->integer('d47')->length(1);
            $table->integer('d48')->length(1);
            $table->integer('d49')->length(1);
            $table->integer('d50')->length(1);
            $table->integer('d51')->length(1);
            $table->integer('d52')->length(1);
            $table->integer('d53')->length(1);
            $table->integer('d54')->length(1);
            $table->integer('d55')->length(1);
            $table->integer('d56')->length(1);
            $table->integer('d57')->length(1);
            $table->integer('d58')->length(1);
            $table->integer('d59')->length(1);
            $table->integer('d60')->length(1);
            $table->integer('d61')->length(1);
            $table->integer('d62')->length(1);
            $table->integer('d63')->length(1);
            $table->integer('d64')->length(1);
            $table->integer('d65')->length(1);
            $table->integer('d66')->length(1);
            $table->integer('d67')->length(1);
            $table->integer('d68')->length(1);
            $table->integer('d69')->length(1);
            $table->integer('d70')->length(1);
            $table->integer('d71')->length(1);
            $table->integer('d72')->length(1);
            $table->integer('d73')->length(1);
            $table->integer('d74')->length(1);
            $table->integer('d75')->length(1);
            $table->integer('d76')->length(1);
            $table->integer('d77')->length(1);
            $table->integer('d78')->length(1);
            $table->integer('d79')->length(1);
            $table->integer('d80')->length(1);
            $table->integer('d81')->length(1);
            $table->integer('d82')->length(1);
            $table->integer('d83')->length(1);
            $table->integer('d84')->length(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kuisbdis');
    }
}