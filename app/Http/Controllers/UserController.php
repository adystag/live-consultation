<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $users = User::where('id', '!=', $request->user()->id)->get();
        $roles = Role::all();

        return view('users.index', [
            'users' => $users,
            'roles' => $roles
        ]);
    }

    public function pilih_psikolog()
    {
        $user = User::all()->where('role_id', '3');

        return view('users.psikolog', ['user' => $user]);
    }

    public function update_psikolog($id, Request $request)
    {

        $user = User::find($id);
        $user->psikolog = $request->psikolog;
        $user->save();
        return redirect('/pilihPsikolog');
    }

    public function edit(Request $request)
    {
        $user = $request->user();

        return view('users.edit', [
            'user' => $user
        ]);
    }

    public function update(Request $request)
    {
        $validatedInput = $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|unique:users,email,' . $request->user()->id . ',id',
            'bio'   => 'present'
        ]);

        $user = $request->user();

        $user->first_name = $validatedInput['first_name'];
        $user->last_name = $validatedInput['last_name'];
        $user->email = $validatedInput['email'];
        $user->bio = $validatedInput['bio'];
        $user->save();

        return redirect()->route('edit-user-profile')
            ->with('status', 'success')
            ->with('status-message', 'User profile edited.');
    }

    public function updateAvatar(Request $request)
    {
        $validatedData = $request->validate([
            'avatar'  => 'required|max:5120'
        ]);

        $path = Storage::disk('public')->putFile('avatars', new File($request->file('avatar')));

        if (!$path) {
            return redirect()->back()->with('status-create', 'danger')
                ->with('status-create-message', 'Failed to upload file.');
        }

        $user = $request->user();

        if (!is_null($user->avatar) || !empty($user->avatar)) {
            Storage::disk('public')->delete('avatars/' . $user->avatar);
        }

        $user->avatar = basename($path);
        $user->save();

        return redirect()
            ->route('edit-user-profile')
            ->with('status-create', 'success')
            ->with('status-create-message', 'Avatar updated.');
    }

    public function removeAvatar(Request $request)
    {
        $user = $request->user();

        if (!is_null($user->avatar) && !empty($user->avatar)) {
            Storage::disk('public')->delete('avatars/' . $user->avatar);
        }

        $user->avatar = '';
        $user->save();

        return redirect()
            ->route('edit-user-profile')
            ->with('status-create', 'success')
            ->with('status-create-message', 'Avatar removed.');
    }

    public function updateRole(Request $request, $userId)
    {
        $validatedData = $request->validate([
            'role_id' => 'required|exists:roles,id'
        ]);

        $user = User::find($userId);

        if (!$user) {
            return redirect()->back()->with('status', 'danger')
                ->with('status-message', 'User not found.');
        }

        $user->role_id = $validatedData['role_id'];
        $user->save();

        if (!$user->role || ($user->role->name != 'consultant')) {
            $user->categories()->detach();
        }

        return redirect()->route('index-users')->with('status', 'success')
            ->with('status-message', 'User role set.');
    }

    public function updatePassword(Request $request)
    {
        $validatedInput = $request->validate([
            'old_password' => 'required',
            'password' => 'required|min:6',
            'password_confirmation' => 'required|same:password'
        ]);

        $user = $request->user();

        if (!Hash::check($validatedInput['old_password'], $user->password)) {
            return redirect()->route('edit-user-profile')
                ->with('status', 'danger')
                ->with('status-message', 'Password not match.');
        }

        if (Hash::check($validatedInput['password'], $user->password)) {
            return redirect()->route('edit-user-profile')
                ->with('status', 'danger')
                ->with('status-message', 'Cannot use the old password.');
        }

        $user->password = Hash::make($validatedInput['password']);
        $user->save();

        return redirect()->route('edit-user-profile')
            ->with('status', 'success')
            ->with('status-message', 'Password changed.');
    }
}