<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'file',
        'status',
        'reason',
        'user_id'
    ];

    public function user ()
    {
        return $this->belongsTo('App\Models\User');
    }
}
