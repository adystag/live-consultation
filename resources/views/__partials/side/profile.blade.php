<div class="p-3 d-flex flex-column align-items-center justify-content-center h-25" style="background-color: #24baba; color: #fff; background-image: url({{ asset('img/bg-user.png') }});">
    <img class="rounded-circle" src="{{ !is_null(Auth::user()->avatar) && !empty(Auth::user()->avatar) ?  Auth::user()->avatar_url : asset('img/user.png') }}" style="height: 64px; width: 64px; object-fit: contain; object-position: center;">

    <div class="mt-1 text-center">
        <p class="m-0">
            <b>{{ Auth::user()->first_name.' '.Auth::user()->last_name }}</b>
        </p>

        <p class="m-0">{{ Auth::user()->email }}</p>

        <small>{{ Auth::user()->role ? ucfirst(Auth::user()->role->name) : 'User' }}</small>
    </div>
</div>
