<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Respond;
use App\Models\UserCase;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Notifications\NewCase;
use Illuminate\Validation\Rule;
use App\Notifications\CaseRespond;
use Illuminate\Support\Facades\Notification;
use App\Models\Setting;

class CaseController extends Controller
{
    public function index(Request $request)
    {
        $status = $request->get('status') && $request->get('status') == 'closed' ? $request->get('status') : 'open';
        $cases = $request->user()->cases;
        $point = Setting::where('setting_key', 'point')->first();
        $openCasesCount = $cases->filter(function ($value, $key) {
            return $value->status == 'open';
        });
        $closedCasesCount = $cases->filter(function ($value, $key) {
            return $value->status == 'closed';
        });

        $userPoints = 0;

        if (!$request->user()->role || ($request->user()->role && $request->user()->role->name == 'user')) {
            $consultations = $request->user()->consultations;
            $userPayments = $request->user()->payments()->where('status', 'accepted')->get();
            $userPoints = ($userPayments->count() * (int) $point->setting_value) - ($cases->count() + $consultations->count());
        }

        if ($request->user()->role && $request->user()->role->name == 'consultant') {
            $userCases = UserCase::with('user', 'user.role')->withCount('responds')
                ->where('consultant_id', $request->user()->id)
                ->where('status', $status)
                ->paginate(15);
        } else {
            $userCases = UserCase::with('consultant', 'consultant.role')->withCount('responds')
                ->where('user_id', $request->user()->id)
                ->where('status', $status)
                ->paginate(15);
        }

        return view('cases.index', [
            'userCases' => $userCases,
            'userPoints' => $userPoints,
            'openCasesCount' => $openCasesCount->count(),
            'closedCasesCount' => $closedCasesCount->count()
        ]);
    }

    public function compose(Request $request)
    {
        $cases = $request->user()->cases;
        $point = Setting::where('setting_key', 'point')->first();
        $userPayments = $request->user()->payments()->where('status', 'accepted')->get();
        $userPoints = ($userPayments->count() * (int) $point->setting_value) - $cases->count();

        if ($userPoints > 0) {
            $categories = Category::all();
            $user = User::all()->where('role_id', '3');

            return view('cases.compose', [
                'categories' => $categories,
                'user' => $user
            ]);
        }

        return redirect()->route('index-payments')
            ->with('status', 'danger')
            ->with('status-message', 'Pleas add your point to make a case.');
    }

    public function store(Request $request)
    {
        $cases = $request->user()->cases;
        $point = Setting::where('setting_key', 'point')->first();
        $userPayments = $request->user()->payments()->where('status', 'accepted')->get();
        $userPoints = ($userPayments->count() * (int) $point->setting_value) - $cases->count();

        if ($userPoints <= 0) {
            return redirect()->route('index-payments')
                ->with('status', 'danger')
                ->with('status-message', 'Pleas add your point to make a case.');
        }

        $validatedData = $request->validate([
            'consultant_id' => 'required|exists:users,id',
            'subject'   => 'required|string',
            'category_id' => 'required|exists:categories,id',
            'question'  => 'required|string'
        ]);

        $consultant = User::with('role')->find($validatedData['consultant_id']);

        if (!$consultant || (!$consultant->role || ($consultant->role && $consultant->role->name != 'consultant'))) {
            return redirect()->back()
                ->withInput(
                    $request->except('consultant_id')
                )
                ->with('status', 'danger')
                ->with('status-message', 'Invalid Consultant.');
        }

        $case = UserCase::create([
            'subject'  => $validatedData['subject'],
            'question'  => $validatedData['question'],
            'consultant_id' => $consultant->id,
            'user_id'   => $request->user()->id,
            'category_id' => $request->category_id
        ]);
        $case->load('user', 'user.role', 'consultant', 'consultant.role');

        Notification::send($case->consultant, new NewCase($case));

        return redirect()->route('show-case', [
            'caseId' => $case->id
        ])
            ->with('status', 'success')
            ->with('status-message', 'Case created.');;
    }

    public function show(Request $request, $caseId)
    {
        $case = UserCase::with('user', 'consultant')->withCount('responds')->find($caseId);

        if (!$case) {
            return redirect()->route('index-cases')
                ->with('status', 'danger')
                ->with('status-message', 'Case not found.');
        }

        $this->authorize('show', $case);

        $responds = $case->responds()->with('user')->paginate(15);

        return view('cases.show', [
            'case' => $case,
            'responds' => $responds
        ]);
    }

    public function update(Request $request, $caseId)
    {
        $validatedData = $request->validate([
            'subject' => 'required',
            'question'  => 'required'
        ]);

        $case = UserCase::find($caseId);

        if (!$case) {
            return redirect()->route('index-cases')
                ->with('status', 'danger')
                ->with('status-message', 'Case not found.');
        }

        $this->authorize('update', $case);

        $case->subject = $validatedData['subject'];
        $case->question = $validatedData['question'];
        $case->save();

        return redirect()->back()
            ->with('status', 'success')
            ->with('status-message', 'Case updated.');
    }

    public function edit(Request $request, $caseId)
    {
        $case = UserCase::with('consultant', 'consultant.role')->find($caseId);

        $this->authorize('update', $case);

        if (!$case) {
            return redirect()->route('index-cases')
                ->with('status', 'danger')
                ->with('status-message', 'Case not found.');
        }

        return view('cases.compose', [
            'case' => $case
        ]);
    }

    public function respond(Request $request, $caseId)
    {
        $validatedData = $request->validate([
            'content' => 'required|string',
        ]);

        $case = UserCase::with('user', 'consultant')->find($caseId);

        if (!$case) {
            return redirect()->route('index-cases')
                ->with('status', 'danger')
                ->with('status-message', 'Case not found.');
        }

        $this->authorize('respond', $case);

        if ($case->status == 'closed') {
            return redirect()->back()
                ->with('status', 'danger')
                ->with('status-message', 'Case has been closed.');
        }

        $respond = $case->responds()->create([
            'content' => $validatedData['content'],
            'user_id' => $request->user()->id
        ]);
        $respond->load('user', 'respondable');

        Notification::send(($respond->user_id == $case->user_id ? $case->consultant : $case->user), new CaseRespond($respond));

        return redirect()->route('show-case', [
            'caseId' => $case->id
        ])
            ->with('status', 'success')
            ->with('status-message', 'Respond created.');
    }

    public function updateStatus(Request $request, $caseId)
    {
        $validatedData = $request->validate([
            'status' => [
                'required',
                Rule::in(['closed', 'open']),
            ]
        ]);

        $case = UserCase::find($caseId);

        if (!$case) {
            return redirect()->route('index-cases')
                ->with('status', 'danger')
                ->with('status-message', 'Case not found.');
        }

        $this->authorize('updateStatus', $case);

        $case->status = $validatedData['status'];
        $case->save();

        return redirect()->back()
            ->with('status', 'success')
            ->with('status-message', 'Case ' . $validatedData['status']);
    }
}