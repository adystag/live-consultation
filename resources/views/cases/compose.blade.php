@extends('layouts.app')

@section('main')
<div class="p-3 rounded shadow-sm bg-white">
    <h3 class="mb-3">{{ isset($case) ? 'Edit' : 'Open' }} Case{{ isset($case) ? ': '.$case->id : '' }}</h3>

    @if (session('status'))
    <div class="alert alert-{{ session('status') }} alert-dismissible fade show mb-3" role="alert">
        {{ session('status-message') }}

        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

    <form method="POST" action="{{ isset($case) ? route('update-case', [ 'caseId' => $case->id ]) : route('create-case') }}">
        @if (isset($case))
            @method('PUT')
        @endif

        @csrf
        <div class="row no-gutters">
            <div class="col-4 border-right pr-3">
                <h5 class="mb-3">{{ isset($case) ? '' : 'Pick ' }}Consultant</h5>

                @if (!isset($case))
                <div class="form-group">
                    <select class="form-control{{ $errors->has('consultant_id') ? ' is-invalid' : '' }}" name="category_id" id="categories-select">
                        <option value="">Category</option>
                        @foreach ($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('consultant_id'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('consultant_id') }}</strong>
                    </span>
                    @endif
                </div>
                @endif

                <div class="form-group m-0">
                    <ul id="{{ isset($case) ? '' : 'consultant-list' }}" class="list-group">
                        @if (isset($case))
                        <li class="list-group-item" onclick="showProfileModal({{ json_encode($case->consultant->toArray()) }})">
                            <img class="rounded-circle ml-3" src="{{ !is_null($case->consultant->avatar) && !empty($case->consultant->avatar) ?  $case->consultant->avatar_url : asset('img/user.png') }}" style="height: 48px; width: 48px; object-fit: contain; object-position: center;">
                            <span class="ml-3">
                                <b>{{ $case->consultant->first_name.' '.$case->consultant->last_name }}</b>
                            </span>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>

            <div class="col-8 pl-3 pr-3">
                <h5 class="mb-3">Case Detail</h5>

                <div class="form-group">
                    <label>Subject</label>
                    <input type="text" class="form-control{{ $errors->has('subject') ? ' is-invalid' : '' }}" name="subject" value="{{ isset($case) ? $case->subject : old('subject') }}">

                    @if ($errors->has('subject'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('subject') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group">
                    <label>Question</label>
                    <textarea id="case-question" name="question" rows="5" class="form-control{{ $errors->has('question') ? ' is-invalid' : '' }}">
                        {!! isset($case) ? $case->question : old('question') !!}
                    </textarea>

                    @if ($errors->has('question'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('question') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group mb-0 d-flex">
                    <a class="btn btn-secondary ml-auto mr-2" href="{{ route('index-cases') }}">
                        Cancel
                    </a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </form>
</div>

<div id="profile-modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content p-3 d-flex flex-colum align-items-center justify-content-center" style="position: relative;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute; top: 0; right: 8px;">
                <span aria-hidden="true" style="font-size: 30px;">&times;</span>
            </button>

            <img id="profile-avatar" class="rounded-circle" src="" style="height: 128px; width: 128px; object-fit: contain; object-position: center;">

            <div class="mt-1 text-center">
                <p class="m-0" style="font-size: 24px;">
                    <b id="profile-name">User Name</b>
                </p>

                <p id="profile-role" style="font-size: 18px;">User Role</p>
            </div>
        </div>
    </div>
</div>
@endsection

@push('styles')
<style>
    .case-form-wrapper {
        padding: 16px;
    }

    #consultant-list {
        visibility: hidden;
    }
</style>
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.css" rel="stylesheet">
@endpush

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.js"></script>
<script>
    var defaultProfpic = "{!! asset("img/user.png") !!}"

    $(document).ready(function () {
        $('#categories-select').on('change', function (e) {
            $('#consultant-list').html('')

            if (!$(this).val()) {
                $('#consultant-list').css('visibility', 'hidden')
            } else {
                $.ajax({
                    url: '{!! url("/categories") !!}' + '/' + $(this).val() + '/json',
                    method: 'get'
                })
                .done(function (result) {
                    var users = result.category.users

                    for(var index = 0; index < users.length; index++) {
                        var user = JSON.stringify(users[index])

                        $('#consultant-list').append(
                        '<li class="list-group-item mb-2">'
                            +'<div class="ml-2 d-flex align-items-center">'
                                +'<input id="consultant-' +  users[index].id + '" class="form-check-input" type="radio" name="consultant_id" value="' + users[index].id + '">'
                                +'<label class="form-check-label d-flex align-items-center" onclick=\'showProfileModal(' + user + ')\' >'
                                    +'<img class="rounded-circle ml-3" src="' + (users[index].avatar ? users[index].avatar_url : defaultProfpic) + '" style="height: 48px; width: 48px; object-fit: contain; object-position: center;">'
                                    +'<span class="ml-3"><b>' + users[index].first_name + ' ' + users[index].last_name + '</b></span>'
                                    +'</label>'
                                    +'</div>'
                                    +'</li>'
                                    )
                                }

                                $('#consultant-list').css('visibility', 'visible')
                            })
                            .fail(function (err) {
                                if (err.responseJSON) {
                                    alert(err.responseJSON.message)
                                } else {
                                    alert(err.statusText)
                                }
                            })
                        }
                    })

                    $('#case-question').summernote({
                        height: 200
                    })
                })

                function showProfileModal (user) {
                    $('#profile-modal #profile-name').html(user.first_name + ' ' + user.last_name)
                    var userRole = user.role ? (user.role.name[0].toUpperCase() + user.role.name.substring(1)) : 'User';
                    $('#profile-modal #profile-role').html(userRole)
                    $('#profile-modal #profile-avatar').attr('src', (user.avatar ? user.avatar_url : defaultProfpic))
                    $('#profile-modal').modal('show')
                }
            </script>
            @endpush
