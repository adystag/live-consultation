<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PaymentController extends Controller
{
    public function index (Request $request)
    {
        if ($request->user()->role && $request->user()->role->name == 'admin')
        {
            $payments = Payment::where('status', 'pending')->get();
        } else {
            $payments = Payment::all();
        }

        return view('payments.index', [
            'payments' => $payments
        ]);
    }

    public function showPaymentFile (Request $request, $paymentId)
    {   
        $payment = Payment::with('user')->find($paymentId);

        if (!$payment) {
            return redirect()->back()->with('status', 'danger')
                                    ->with('status-message', 'Payment not found.');
        }

        $this->authorize('view', $payment);

        return response()->file(storage_path("app/payments/{$payment->file}"));
    }

    public function store (Request $request)
    {
        $validatedData = $request->validate([
            'file'  => 'required|max:5120'
        ]);

        $path = Storage::putFile('payments', new File($request->file('file')));
        
        if (!$path) {
            return redirect()->back()->with('status-create', 'danger')
                                    ->with('status-create-message', 'Failed to upload file.');
        }

        Payment::create([
            'file' => basename($path),
            'user_id' => $request->user()->id
        ]);

        return redirect()
                ->route('index-payments')
                ->with('status-create', 'success')
                ->with('status-create-message', 'Payment uploaded.');
    }

    public function update (Request $request, $paymentId)
    {
        $data = $request->only('status', 'reason');
        $payment = Payment::find($paymentId);

        if (!$payment) {
            return redirect()->back()->with('status', 'danger')
                                    ->with('status-message', 'Payment not found.');
        }

        $payment->status = $data['status'];
        $payment->reason = array_key_exists('reason', $data) ? $data['reason'] : '';
        $payment->save();

        return redirect()
                ->route('index-payments')
                ->with('status', 'success')
                ->with('status-message', "Payment {$data['status']}.");
    }
}
