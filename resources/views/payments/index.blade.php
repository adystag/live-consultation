@extends('layouts.app')

@section('main')
@can('create', App\Models\Payment::class)
<div class="p-3 rounded shadow-sm bg-white mb-3">
    <h3 class="mb-3">Make Payment</h3>

    @if (session('status-create'))
    <div class="alert alert-{{ session('status-create') }} alert-dismissible fade show mb-3" role="alert">
        {{ session('status-create-message') }}

        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

    <form id="payment-form" method="POST" action="{{ route('create-payment') }}"  enctype="multipart/form-data">
        @csrf

        <div class="form-group">
            <label>Payment struct</label>

            <div class="custom-file">
                <input type="file" class="custom-file-input {{ $errors->has('file') ? ' is-invalid' : '' }}" id="payment-file" name="file">
                <label class="custom-file-label" for="payment-file">Choose file</label>

                @if ($errors->has('file'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('file') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group mb-0 d-flex">
            <button type="submit" class="btn btn-primary ml-auto">Submit</button>
        </div>
    </form>
</div>
@endcan

<div class="p-3 rounded shadow-sm bg-white">
    <h3 class="mb-3">Payments</h3>

    @if (session('status'))
    <div class="alert alert-{{ session('status') }} alert-dismissible fade show mb-3" role="alert">
        {{ session('status-message') }}

        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

    <table id="payments-table" class="table table-striped mb-0">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Date</th>
                <th scope="col">Status</th>
                <th scope="col">Action</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($payments as $key => $payment)
            <tr>
            @if ((Auth::user()->id == $payment->user_id) || (Auth::user()->role && Auth::user()->role->name == 'admin'))
                <th>{{ $key + 1 }}</th>
                <td>{{ $payment->updated_at }}</td>
                <td style="color: {{ $payment->status == 'accepted' ? 'green' :  ($payment->status == 'rejected' ? 'red' : '') }}">{{ $payment->status }}</td>
                <td>
                    <a class="btn btn-primary btn-sm" href="{{ route('show-payment-file', [ 'paymentId' => $payment->id ]) }}" target="_blank">View</a>

                    @if ((!Auth::user()->role || Auth::user()->role->name == 'user') && $payment->status == 'rejected')
                    <button class="btn btn-sm btn-warning" onclick="showReason('{{ $payment->reason }}')">Reason</button>
                    @endif

                    @can('update', App\Models\Payment::class)
                    <button class="btn btn-success btn-sm" onclick="confirmAcceptance({{ $payment->id }})">Accept</button>
                    <button class="btn btn-danger btn-sm" onclick="confirmRejection({{ $payment->id }})">Reject</button>
                    <form id="accept-payment-{{ $payment->id }}" method="POST" action="{{ route('update-payment', [ 'paymentId' => $payment->id ]) }}">
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="status" value="accepted" />
                    </form>
                    <form id="reject-payment-{{ $payment->id }}" method="POST" action="{{ route('update-payment', [ 'paymentId' => $payment->id ]) }}">
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="status" value="rejected" />
                        <input type="hidden" name="reason" />
                    </form>
                    @endcan
                </td>
                    @endif
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
<link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css') }}">
@endpush

@push('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
<script src="{{ asset('js/sweetalert2.min.js') }}"></script>
<script>
    $(document).ready(function () {
        @can('create', App\Models\Payment::class)
        $('#payment-file').on('change', function (e) {
            if ($(this)[0].files[0]) {
                $('label[for=payment-file]').html($(this)[0].files[0].name)
            } else {
                $('label[for=payment-file]').html('Choose file')
            }
        })
        @endcan

        $('#payments-table').DataTable();
    })

    @can('update', App\Models\Payment::class)
    function confirmRejection (id) {
        swal({
            title: 'Reject Payment',
            text: 'Do you want to reject this payment?',
            type: 'question',
            input: 'textarea',
            showCancelButton: true,
            confirmButtonText: 'Yes'
        })
        .then(result => {
            if (!result.dismiss) {
                $('#reject-payment-'+id+' > input[name=reason]').val(result.value)
                $('#reject-payment-'+id).submit()
            }
        })
    }

    function confirmAcceptance (id) {
        swal({
            title: 'Accept Payment',
            text: 'Do you want to accept this payment?',
            type: 'question',
            showCancelButton: true,
            confirmButtonText: 'Yes'
        })
        .then(result => {
            if (!result.dismiss) {
                $('#accept-payment-'+id).submit()
            }
        })
    }
    @endcan

    function showReason (reason) {
        reason = reason ? reason : 'No reason.'

        swal({
            title: 'Rejection Reason',
            text: reason,
            type: 'error',
            confirmButtonText: 'OK'
        })
    }
</script>
@endpush
