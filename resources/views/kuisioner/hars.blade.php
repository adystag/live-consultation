@extends('layouts.app')

@section('main')
<div class="p-3 rounded shadow-sm bg-white">
        <nav class="nav nav-pills nav-justified">
                <a class="nav-item nav-link" href="/kuisioner/tmas">TMAS</a>
                <a class="nav-item nav-link active" href="/kuisioner/hars">HARS</a>
              </nav>
              <hr>
    <h3 class="mb-3">Hamilton Anxiety Rating Scale (HARS)
        </h3>
    
    @if (session('status'))
    <div class="alert alert-{{ session('status') }} alert-dismissible fade show mb-3" role="alert">
        {{ session('status-message') }}
        
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <form action="{{ route('tambahdatahars')}}" method="POST">
            @csrf
    <table class="table table-striped mb-0">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Kondisi</th>
                <th scope="col">0</th>
                <th scope="col">1</th>
                <th scope="col">2</th>
                <th scope="col">3</th>
                <th scope="col">4</th>
            </tr>
        </thead>
        
        <tbody>
                <input type="hidden" name="email" value="{{ Auth::user()->email }}">
                <tr>
                    <th>1</th>
                    <td>Perasaan cemas firasat buruk, takut akan pikiran sendiri, mudah tensinggung.
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d1" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d1" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d1" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d1" value="3"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d1" value="4"></label></td>
                </tr>
                <tr>
                    <th>2</th>
                    <td>Ketegangan merasa tegang, gelisah, gemetar, mudah terganggu dan lesu.
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d2" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d2" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d2" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d2" value="3"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d2" value="4"></label></td>

                </tr>
                <tr>
                    <th>3</th>
                    <td>Ketakutan: takut terhadap gelap, terhadap orang asing, bila tinggal sendiri dan takut pada binatang besar.
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d3" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d3" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d3" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d3" value="3"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d3" value="4"></label></td>
                </tr>
                <tr>
                    <th>4</th>
                    <td>Gangguan tidur sukar memulai tidur, terbangun pada malam hari, tidur tidak pulas dan mimpi buruk.
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d4" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d4" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d4" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d4" value="3"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d4" value="4"></label></td>
                </tr>
                <tr>
                    <th>5</th>
                    <td>Gangguan kecerdasan: penurunan daya ingat, mudah lupa dan sulit konsentrasi.
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d5" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d5" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d5" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d5" value="3"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d5" value="4"></label></td>
                </tr>
                <tr>
                    <th>6</th>
                    <td>Perasaan depresi: hilangnya minat, berkurangnya kesenangan pada hoby, sedih, perasaan tidak menyenangkan sepanjang hari.
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d6" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d6" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d6" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d6" value="3"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d6" value="4"></label></td>
                </tr>
                <tr>
                    <th>7</th>
                    <td>Gejala somatik: nyeni path otot-otot dan kaku, gertakan gigi, suara tidak stabil dan kedutan otot.
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d7" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d7" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d7" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d7" value="3"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d7" value="4"></label></td>
                </tr>
                <tr>
                    <th>8</th>
                    <td>Gejala sensorik: perasaan ditusuk-tusuk, penglihatan kabur, muka merah dan pucat serta merasa lemah.
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d8" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d8" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d8" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d8" value="3"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d8" value="4"></label></td>
                </tr>
                <tr>
                    <th>9</th>
                    <td>Gejala kardiovaskuler: takikardi, nyeri di dada, denyut nadi mengeras dan detak jantung hilang sekejap.
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d9" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d9" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d9" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d9" value="3"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d9" value="4"></label></td>
                </tr>
                <tr>
                    <th>10</th>
                    <td>Gejala pemapasan: rasa tertekan di dada, perasaan tercekik, sering menarik napas panjang dan merasa napas pendek.
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d10" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d10" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d10" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d10" value="3"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d10" value="4"></label></td>
                </tr>
                <tr>
                    <th>11</th>
                    <td>Gejala gastrointestinal: sulit menelan, obstipasi, berat badan menurun, mual dan muntah, nyeri lambung sebelum dan sesudah makan, perasaan panas di perut.
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d11" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d11" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d11" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d11" value="3"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d11" value="4"></label></td>
                </tr>
                <tr>
                    <th>12</th>
                    <td>Gejala urogenital: sering kencing, tidak dapat menahan kencing, aminorea, ereksi lemah atau impotensi.
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d12" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d12" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d12" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d12" value="3"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d12" value="4"></label></td>
                </tr>
                <tr>
                    <th>13</th>
                    <td>Gejala vegetatif: mulut kering, mudah berkeringat, muka merah, bulu roma berdiri, pusing atau sakit kepala.
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d13" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d13" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d13" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d13" value="3"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d13" value="4"></label></td>
                </tr>
                <tr>
                    <th>14</th>
                    <td>Perilaku sewaktu wawancara: gelisah, jari-jari gemetar, mengkerutkan dahi atau kening, muka tegang, tonus otot meningkat dan napas pendek dan cepat.
                    </td>
                    
                    <td><label class="radio-inline"><input type="radio" name="d14" value="0" checked="checked"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d14" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d14" value="2"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d14" value="3"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d14" value="4"></label></td>
                </tr>
        </tbody>
    </table><br>
    {{-- <div class="form-group">
        <input type="submit" class="btn btn-success" value="Simpan">
    </div>
    <form action="{{ route('tambahdata')}}" method="POST">
            @csrf
            <input type="text" name="id_user" value="1">
            <input type="text" name="id_dass" value="1">
            <input type="text" name="nilai_dass" value="1"> --}}
            <div class="form-group mb-0 d-flex">
                    <button type="submit" class="btn btn-primary ml-auto">Submit</button>
                </div>
    </form>
        
                
                    
</div>

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
<link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css') }}">
@endpush

@push('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
<script src="{{ asset('js/sweetalert2.min.js') }}"></script>
<script>
    $(document).ready(function() {
    $('#table').DataTable( {
        "ordering": false
    } );
} );
    </script>
    @endpush

