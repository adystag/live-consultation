<?php

namespace App\Models;

use Illuminate\Support\Facades\Storage;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'role_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getAvatarUrlAttribute($value)
    {
        return Storage::disk('public')->url('avatars/'.$this->avatar);
    }

    public function payments ()
    {
        return $this->hasMany('App\Models\Payment');
    }

    public function dass ()
    {
        return $this->belongsTo('App\Models\Dass');
    }

    public function role ()
    {
        return $this->belongsTo('App\Models\Role');
    }

    public function responds ()
    {
        return $this->hasMany('App\Models\Respond');
    }

    public function cases ()
    {
        if ($this->role && $this->role->name == 'consultant') {
            return $this->hasMany('App\Models\UserCase', 'consultant_id');
        }

        return $this->hasMany('App\Models\UserCase');
    }

    public function consultations ()
    {
        if ($this->role && $this->role->name == 'consultant') {
            return $this->hasMany('App\Models\Consultation', 'consultant_id');
        }

        return $this->hasMany('App\Models\Consultation');
    }

    public function categories ()
    {
        return $this->belongsToMany('App\Models\Category');
    }

    public function toArray ()
    {
        $user = parent::toArray();
        $user['avatar_url'] = Storage::disk('public')->url('avatars/'.$this->avatar);

        return $user;
    }
}