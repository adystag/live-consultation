@extends('layouts.app')

@section('main')
<div class="p-3 rounded shadow-sm bg-white mb-3">
    <h3 class="mb-3">Jawaban Kuisioner</h3>
    
    @if (session('status'))
    <div class="alert alert-{{ session('status') }} alert-dismissible fade show mb-3" role="alert">
        {{ session('status-message') }}
        
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    {{ csrf_field() }}
    {{ method_field('PUT') }}

   
    <table id="table" class="table table-striped mb-0">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Kondisi</th>
                            <th scope="col">Skor</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                            <tr>
                                    <th>1</th>
                                    <td>Perasaan cemas firasat buruk, takut akan pikiran sendiri, mudah tensinggung.
                                    </td>
                                    <td>{{ $hasil->d1 }}</td>   
                                </tr>
                                <tr>
                                    <th>2</th>
                                    <td>Ketegangan merasa tegang, gelisah, gemetar, mudah terganggu dan lesu.
                                    </td>
                                    <td>{{ $hasil->d2 }}</td>   
                
                                </tr>
                                <tr>
                                    <th>3</th>
                                    <td>Ketakutan: takut terhadap gelap, terhadap orang asing, bila tinggal sendiri dan takut pada binatang besar.
                                    </td>
                                    <td>{{ $hasil->d3 }}</td>   
                                </tr>
                                <tr>
                                    <th>4</th>
                                    <td>Gangguan tidur sukar memulai tidur, terbangun pada malam hari, tidur tidak pulas dan mimpi buruk.
                                    </td>
                                    <td>{{ $hasil->d4 }}</td>   
                                </tr>
                                <tr>
                                    <th>5</th>
                                    <td>Gangguan kecerdasan: penurunan daya ingat, mudah lupa dan sulit konsentrasi.
                                    </td>
                                    <td>{{ $hasil->d5 }}</td>   
                                </tr>
                                <tr>
                                    <th>6</th>
                                    <td>Perasaan depresi: hilangnya minat, berkurangnya kesenangan pada hoby, sedih, perasaan tidak menyenangkan sepanjang hari.
                                    </td>
                                    <td>{{ $hasil->d6 }}</td>   
                                </tr>
                                <tr>
                                    <th>7</th>
                                    <td>Gejala somatik: nyeni path otot-otot dan kaku, gertakan gigi, suara tidak stabil dan kedutan otot.
                                    </td>
                                    <td>{{ $hasil->d7 }}</td>   
                                </tr>
                                <tr>
                                    <th>8</th>
                                    <td>Gejala sensorik: perasaan ditusuk-tusuk, penglihatan kabur, muka merah dan pucat serta merasa lemah.
                                    </td>
                                    <td>{{ $hasil->d8 }}</td>   
                                </tr>
                                <tr>
                                    <th>9</th>
                                    <td>Gejala kardiovaskuler: takikardi, nyeri di dada, denyut nadi mengeras dan detak jantung hilang sekejap.
                                    </td>
                                    <td>{{ $hasil->d9 }}</td>   
                                </tr>
                                <tr>
                                    <th>10</th>
                                    <td>Gejala pemapasan: rasa tertekan di dada, perasaan tercekik, sering menarik napas panjang dan merasa napas pendek.
                                    </td>
                                    <td>{{ $hasil->d10 }}</td>   
                                </tr>
                                <tr>
                                    <th>11</th>
                                    <td>Gejala gastrointestinal: sulit menelan, obstipasi, berat badan menurun, mual dan muntah, nyeri lambung sebelum dan sesudah makan, perasaan panas di perut.
                                    </td>
                                    <td>{{ $hasil->d11 }}</td>   
                                </tr>
                                <tr>
                                    <th>12</th>
                                    <td>Gejala urogenital: sering kencing, tidak dapat menahan kencing, aminorea, ereksi lemah atau impotensi.
                                    </td>
                                    <td>{{ $hasil->d12 }}</td>   
                                </tr>
                                <tr>
                                    <th>13</th>
                                    <td>Gejala vegetatif: mulut kering, mudah berkeringat, muka merah, bulu roma berdiri, pusing atau sakit kepala.
                                    </td>
                                    <td>{{ $hasil->d12 }}</td>   
                                </tr>
                                <tr>
                                    <th>14</th>
                                    <td>Perilaku sewaktu wawancara: gelisah, jari-jari gemetar, mengkerutkan dahi atau kening, muka tegang, tonus otot meningkat dan napas pendek dan cepat.
                                    </td>
                                    <td>{{ $hasil->d13 }}</td>   
                                </tr>
                    </tbody>
                </table>
                <br>
                <hr>
            </div>

           
                    @if (!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant'))
                    <div class="p-3 rounded shadow-sm bg-white">
                            <h3 class="mb-3">Hasil Kuisioner</h3>
                        <table class="table table-striped mb-0">  
                                <thead>
                                        <tr>
                                            <th scope="col"></th>
                                            <th scope="col">Skor</th>
                                            <th scope="col">Tingkat</th>
                                        </tr>
                                    </thead>                 
                                <tbody>
                                        <tr>
                                            <td>Kecemasan</td>
                                            <td>{{ $hasil->total }}</td>
                                            <td>{{ $hasil->tingkat }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            
                    <div class="p-3 rounded shadow-sm bg-white mb-3">
                            <form method="post" action="/kuisioner/updateHars/{{ $hasil->id }}">

                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <h3 class="mb-3">Keterangan</h3>
                                <textarea class="form-control" name="penjelasan" >{{ $hasil->penjelasan }}</textarea>
                        <input type="hidden" class="form-control" name="status" value="Selesai di Analisis"/>
                        <br>

                        <div class="form-group mb-0 d-flex">
                                <button type="submit" class="btn btn-primary ml-auto">Submit</button>
                            </div>
                        </form>
                    </div>

                    
            @elseif (!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user' && $hasil->status == 'Selesai di Analisis'))
            <div class="p-3 rounded shadow-sm bg-white">
                    <h3 class="mb-3">Hasil Kuisioner</h3>
                <table class="table table-striped mb-0">  
                        <thead>
                                <tr>
                                    <th scope="col"></th>
                                    <th scope="col">Skor</th>
                                    <th scope="col">Tingkat</th>
                                </tr>
                            </thead>                 
                        <tbody>
                                <tr>
                                    <td>Kecemasan</td>
                                    <td>{{ $hasil->total }}</td>
                                    <td>{{ $hasil->tingkat }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    
            <div class="p-3 rounded shadow-sm bg-white mb-3">
                        <h3 class="mb-3">Keterangan</h3>
                        <textarea class="form-control" name="penjelasan" >{{ $hasil->penjelasan }}</textarea>
                <input type="hidden" class="form-control" name="status" value="Selesai di Analisis"/>
                <br>

            @endif

                   
                    


@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
<link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css') }}">
@endpush

@push('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
<script src="{{ asset('js/sweetalert2.min.js') }}"></script>
<script>
    $(document).ready(function() {
    $('#table').DataTable( {
        "ordering": false
    } );
} );
    </script>
    @endpush

