@extends('layouts.app')

@section('main')
<div class="card">
    <div class="card-header">Term And Condition</div>

    <div class="card-body">
        @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
        @endif

<p>
<strong>Umum</strong><br>
1. Dalam berkonsultasi Client diharuskan untuk menggunakan bahasa yang baik dan baku <br>
2. Untuk mendapatkan sebuah konsultasi, klien diharuskan membayar sebesar Rp. 300.000 yang kemudian ditukarkan dengan poin konsultasi<br>
3. Pembayaran dapat di transfer ke rekening Mandiri A.n PUSKAGA UII xxxxxx <br>
<br>

<strong>Point</strong> <br>
1. Setiap Konsultasi Memerlukan 1 Point<br>
2. Point didapatkan dengan cara melakukan pembayaran<br>
3. Proses Konfirmasi Pembayaran dilakukan 1x24 jam<br>
4. Setiap 1 kali Pembayaran mendapatkan 2 Point <br>
5. Menu Konsultasi berbasis Cases memerlukan 1 Point <br>
6. Menu Konsultasi berbasis Live Consultation memerlukan 1 point<br>

<br>
<strong>Konsultasi</strong><br>
1. Konsultasi berbasis Live Consultation hanya dapat dilakukan apabila Psikolog yang dipilih berdasar Kategori Tersedia / Online <br>
2. Konsultasi berbasis Live Consultation memiliki durasi waktu selama 60 Menit <br>
3. Konsultasi berbasis Live Consultation akan dihentikan sementara apabila Psikolog atau Client tidak masuk kedalam room chat, baik pindah menu maupun Offline<br>
4. Setiap Cases yang dibuka oleh client akan dibalas oleh Psikolog dalam waktu 1x24 jam<br>
5. Konsultasi berbasis Cases akan ditutup apabila dirasa kasus telah selesai<br>
</p>

    </div>
</div>
@endsection
