<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Models\Hars;
Route::get('/', function () {
    return view('welcome');
});

Route::get('/terms-and-condition', function () {
    return view('terms-and-condition');
});

Auth::routes();

Route::middleware(['auth'])->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/tac', 'HomeController@tac')->name('tac');
    Route::get('/history', 'HomeController@history')->name('history');
    Route::get('/historyChat', 'HomeController@historyChat')->name('historyChat');
    Route::get('/history/data', 'HomeController@getHistoryData')->name('history.data');
    Route::get('/historyChat/data', 'HomeController@getHistoryChatData')->name('historyChat.data');

    Route::get('/setting', 'SettingController@edit')->name('setting.edit');
    Route::put('/setting/update/{id}', 'SettingController@update')->name('setting.update');

    Route::get('/payments', 'PaymentController@index')->name('index-payments')->middleware(['can:index,App\Models\Payment']);
    Route::post('/payments', 'PaymentController@store')->name('create-payment')->middleware(['can:create,App\Models\Payment']);
    Route::get('/payments/{paymentId}', 'PaymentController@showPaymentFile')->name('show-payment-file');
    Route::put('/payments/{paymentId}', 'PaymentController@update')->name('update-payment')->middleware(['can:update,App\Models\Payment']);

    Route::get('/users', 'UserController@index')->name('index-users')->middleware(['can:update-user-role']);
    Route::get('/users/profile', 'UserController@edit')->name('edit-user-profile');
    Route::put('/users/{userId}/roles', 'UserController@updateRole')->name('update-user-role')->middleware(['can:update-user-role']);
    Route::put('/users/profile', 'UserController@update')->name('update-user-profile');
    Route::put('/users/avatar', 'UserController@updateAvatar')->name('update-user-avatar');
    Route::put('/users/password', 'UserController@updatePassword')->name('update-user-password');
    Route::delete('/users/avatar', 'UserController@removeAvatar')->name('remove-user-avatar');

    Route::get('/categories/{categoryId}/json', 'CategoryController@showJson')->name('show-category-json');
    Route::get('/categories/{categoryId}/users/json', 'CategoryController@whosUsersOnline')->name('whos-users-online');

    // Kuisioner
    Route::get('/kuisioner/new', function () {
        $admin = Hars::all();
      $sikolog = Hars::all()->where('sikolog',Auth::user()->id);
        return view('kuisioner.rekap_holmes',['admin' => $admin],['sikolog' => $sikolog]);
        });
    Route::get('/kuisioner/dass', function () {
        return view('kuisioner.dass');
        });
    Route::post('/tambahdatadass', 'KuisionerController@tambah_dass')->name('tambahdatadass');
    
    Route::get('/kuisioner/bdi', function () {
        return view('kuisioner.bdi');
        });
    Route::post('/tambahdatabdi', 'KuisionerController@tambah_bdi')->name('tambahdatabdi');

    Route::get('/kuisioner/tmas', function () {
        return view('kuisioner.tmas');
        });
    Route::post('/tambahdatatmas', 'KuisionerController@tambah_tmas')->name('tambahdatatmas');

    Route::get('/kuisioner/hars', function () {
        return view('kuisioner.hars');
        });
    Route::post('/tambahdatahars', 'KuisionerController@tambah_hars')->name('tambahdatahars');

    Route::get('/kuisioner/holmes', function () {
        return view('kuisioner.holmes');
        });
    Route::post('/tambahdataholmes', 'KuisionerController@tambah_holmes')->name('tambahdataholmes');

    Route::get('/kuisioner/kuisbdi', function () {
        return view('kuisioner.kuisbdi');
        });
    Route::post('/tambahdatakuisbdi', 'KuisionerController@tambah_kuisbdi')->name('tambahdatakuisbdi');

    Route::get('/kuisioner/test', 'KuisionerController@upd');

    Route::get('/kuisioner/rekapKuisbdi', 'KuisionerController@rekapKuisBdi');
    Route::get('/kuisioner/hasilKuisbdi/{id}', 'KuisionerController@hasilKuisBdi');
    Route::get('/kuisioner/hasilDass/{id}', 'KuisionerController@hasilDass');
    Route::put('/kuisioner/updateDass/{id}', 'KuisionerController@update_dass');
    Route::get('/kuisioner/rekapDassK', 'KuisionerController@rekapDassK');
    Route::get('/kuisioner/rekapDassD', 'KuisionerController@rekapDassD');
    Route::get('/kuisioner/rekapDassS', 'KuisionerController@rekapDassS');
    Route::get('/kuisioner/hasilBdi/{id}', 'KuisionerController@hasilBdi');
    Route::put('/kuisioner/updateBdi/{id}', 'KuisionerController@update_bdi');
    Route::get('/kuisioner/rekapBdi', 'KuisionerController@rekapBdi');
    Route::get('/kuisioner/hasilTmas/{id}', 'KuisionerController@hasilTmas');
    Route::put('/kuisioner/updateTmas/{id}', 'KuisionerController@update_tmas');
    Route::get('/kuisioner/rekapTmas', 'KuisionerController@rekapTmas');
    Route::get('/kuisioner/hasilHars/{id}', 'KuisionerController@hasilHars');
    Route::put('/kuisioner/updateHars/{id}', 'KuisionerController@update_hars');
    Route::get('/kuisioner/rekapHars', 'KuisionerController@rekapHars');
    Route::get('/kuisioner/hasilHolmes/{id}', 'KuisionerController@hasilHolmes');
    Route::put('/kuisioner/updateHolmes/{id}', 'KuisionerController@update_holmes');
    Route::get('/kuisioner/rekapHolmes', 'KuisionerController@rekapHolmes');
    Route::get('/kuisioner/rekaphasil', 'KuisionerController@rekaphasil');
    Route::get('/kuisioner', 'KuisionerController@kuisioner');

    Route::get('/kuisioner/hapusDass/{id}', 'KuisionerController@deleteDass');
    Route::get('/kuisioner/hapusBdi/{id}', 'KuisionerController@deleteBdi');
    Route::get('/kuisioner/hapusTmas/{id}', 'KuisionerController@deleteTmas');
    Route::get('/kuisioner/hapusHars/{id}', 'KuisionerController@deleteHars');
    Route::get('/kuisioner/hapusHolmes/{id}', 'KuisionerController@deleteHolmes');
    Route::get('/kuisioner/hapusKuisbdi/{id}', 'KuisionerController@deleteKuisbdi');

    Route::get('/kuisioner/rekapUserDassK', 'KuisionerController@rekap_user_DassK');
    Route::get('/kuisioner/rekapUserDassD', 'KuisionerController@rekap_user_DassD');
    Route::get('/kuisioner/rekapUserDassS', 'KuisionerController@rekap_user_DassS');
    Route::get('/kuisioner/rekapUserBdi', 'KuisionerController@rekap_user_Bdi');
    Route::get('/kuisioner/rekapUserKuisbdi', 'KuisionerController@rekap_user_KuisBdi');
    Route::get('/kuisioner/rekapUserTmas', 'KuisionerController@rekap_user_Tmas');
    Route::get('/kuisioner/rekapUserHars', 'KuisionerController@rekap_user_Hars');
    Route::get('/kuisioner/rekapUserHolmes', 'KuisionerController@rekap_user_Holmes');

    Route::get('/kuisioner/editHars/{id}', 'KuisionerController@edit_hars');
    Route::put('/kuisioner/update_hars/{id}', 'KuisionerController@update_kuisioner_hars');

    Route::get('/pilihPsikolog', 'UserController@pilih_psikolog');
    Route::put('/updatePsikolog/{id}', 'UserController@update_psikolog');
    // Route::get('/kuisioner/dass', 'KuisionerController@dass')->name('dass');
    // Route::post('/tambahdata', 'KuisionerController@store')->name('tambahdata');
    
    // Route::post('/dass/store', 'KuisionerController@store');
    // end-Kuisioner

    Route::middleware(['can:manage-categories'])->group(function () {
        Route::post('/categories', 'CategoryController@store')->name('create-category');
        Route::get('/categories', 'CategoryController@index')->name('index-categories');
        Route::get('/categories/{categoryId}', 'CategoryController@show')->name('show-category');
        Route::put('/categories/{categoryId}', 'CategoryController@update')->name('update-category');
        Route::get('/categories/{categoryId}/users', 'CategoryController@showUsersToAdd')->name('show-users-candidate');
        Route::post('/categories/{categoryId}/users', 'CategoryController@addMember')->name('add-category-member');
        Route::delete('/categories/{categoryId}/users/{userId}', 'CategoryController@deleteMember')->name('delete-category-member');
        Route::delete('/categories/{categoryId}', 'CategoryController@delete')->name('delete-category');
    });

    Route::get('/cases/compose', 'CaseController@compose')->name('compose-case')->middleware(['can:create,App\Models\UserCase']);
    Route::get('/cases', 'CaseController@index')->name('index-cases')->middleware(['can:index,App\Models\UserCase']);
    Route::get('/cases/{caseId}', 'CaseController@show')->name('show-case');
    Route::get('/cases/{caseId}/edit', 'CaseController@edit')->name('edit-case');
    Route::post('/cases', 'CaseController@store')->name('create-case')->middleware(['can:create,App\Models\UserCase']);
    Route::post('/cases/{caseId}/responds', 'CaseController@respond')->name('respond-case');
    Route::put('/cases/{caseId}/status', 'CaseController@updateStatus')->name('update-case-status');
    Route::put('/cases/{caseId}', 'CaseController@update')->name('update-case');

    Route::get('/consultations', 'ConsultationController@index')->name('index-consultations')->middleware(['can:index,App\Models\Consultation']);
    Route::get('/consultations/compose', 'ConsultationController@compose')->name('compose-consultation')->middleware(['can:create,App\Models\Consultation']);
    Route::get('/consultations/{consultationId}', 'ConsultationController@show')->name('show-consultation');
    Route::put('/consultations/{consultationId}', 'ConsultationController@update')->name('update-consultation');
    Route::post('/consultations', 'ConsultationController@store')->name('create-consultation')->middleware(['can:create,App\Models\Consultation']);
    Route::post('/consultations/{consultationId}/responds', 'ConsultationController@respond')->name('respond-consultation');

    Route::get('/notifications', function () {
        $notifications = Auth::user()->notifications;
        $notifications = $notifications->sortByDesc('created_at')->map(function ($value) {
            $data = $value->data;
            $data['type'] = $value->type;
            $data['notified_at'] = $value->created_at->toDateTimeString();
            $data['isRead'] = !!$value->read_at;

            return $data;
        })->toArray();
        Auth::user()->unreadNotifications->markAsRead();

        return view('notifications.index', compact('notifications'));
    })->name('notifications');
});