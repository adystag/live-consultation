<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index ()
    {
        $categories = Category::all();

        return view('categories.index', [
            'categories' => $categories
        ]);
    }

    public function store (Request $request)
    {
        $validatedData = $request->validate([
            'name'  => 'required'
        ]);

        Category::create($validatedData);

        return redirect()
                    ->route('index-categories')
                    ->with('status-create', 'success')
                    ->with('status-create-message', 'Category created.');
    }

    public function show ($categoryId)
    {
        $category = Category::with('users')->find($categoryId);

        if (!$category) {
            return redirect()
                    ->route('index-categories')
                    ->with('status', 'danger')
                    ->with('status-message', 'Category not found.');
        }

        return view('categories.manage.index', [
            'category' => $category
        ]);
    }

    public function showJson ($categoryId)
    {
        $category = Category::with('users', 'users.role')->find($categoryId);

        if (!$category) {
            return response()->json([
                'err'       => 'NotFound',
                'code'      => 404,
                'message'   => 'Category not found.'
            ], 404);
        }

        return response()->json([
            'category' => $category
        ], 200);
    }

    public function update (Request $request, $categoryId)
    {
        $validatedData = $request->validate([
            'name'  => 'required'
        ]);

        $category = Category::find($categoryId);

        if (!$category) {
            return redirect()
                    ->route('index-categories')
                    ->with('status', 'danger')
                    ->with('status-message', 'Category not found.');
        }

        $category->name = $validatedData['name'];
        $category->save();

        return redirect()
                    ->route('show-category', [ 'categoryId' => $categoryId ])
                    ->with('status', 'success')
                    ->with('status-message', 'Category updated.');
    }

    public function showUsersToAdd ($categoryId)
    {
        $category = Category::with('users')->find($categoryId);

        if (!$category) {
            return redirect()
                    ->route('index-categories')
                    ->with('status', 'danger')
                    ->with('status-message', 'Category not found.');
        }

        $categoryUsers = $category->users->map(function ($user) {
            return $user->id;
        });
        $consultantRole = Role::where('name', 'consultant')->first();
        $users = User::where('role_id', $consultantRole->id)->whereNotIn('id', $categoryUsers)->get();

        return view('categories.manage.add_member', [
            'category' => $category,
            'users' => $users
        ]);
    }

    public function addMember (Request $request, $categoryId)
    {
        $validatedData = $request->validate([
            'user_id'  => 'required|exists:users,id'
        ]);

        $category = Category::find($categoryId);

        if (!$category) {
            return redirect()
                    ->route('index-categories')
                    ->with('status', 'danger')
                    ->with('status-message', 'Category not found.');
        }

        $user = User::with('role')->find($validatedData['user_id']);

        if (!$user) {
            return redirect()
                    ->back()
                    ->with('status', 'danger')
                    ->with('status-message', 'Category not found.');
        }

        if (!$user->role || ($user->role->name != 'consultant')) {
            return redirect()
                    ->back()
                    ->with('status-members', 'danger')
                    ->with('status-members-message', 'Can\'t add user.');
        }

        $category->users()->attach($user->id);

        return redirect()
                    ->route('show-users-candidate', [ 'categoryId' => $categoryId])
                    ->with('status-members', 'success')
                    ->with('status-members-message', 'User added as member.');
    }

    public function delete ($categoryId)
    {
        $category = Category::find($categoryId);

        if (!$category) {
            return redirect()
                    ->route('index-categories')
                    ->with('status', 'danger')
                    ->with('status-message', 'Category not found.');
        }

        $category->users()->detach();
        $category->delete();

        return redirect()
                    ->route('index-categories')
                    ->with('status', 'success')
                    ->with('status-message', 'Category deleted.');
    }

    public function deleteMember ($categoryId, $userId)
    {
        $category = Category::find($categoryId);

        if (!$category) {
            return redirect()
                    ->route('index-categories')
                    ->with('status', 'danger')
                    ->with('status-message', 'Category not found.');
        }

        $category->users()->detach($userId);

        return redirect()
                    ->route('show-category', [ 'categoryId' => $categoryId ])
                    ->with('status-members', 'success')
                    ->with('status-members-message', 'Member deleted from category.');
    }

    public function whosUsersOnline ($categoryId)
    {
        $category = Category::with(['users', 'users.role', 'users.consultations' => function ($query) {
            $query->where('status', 'active');
        }])->find($categoryId);

        if (!$category) {
            return response()->json([
                'err'       => 'NotFound',
                'code'      => 404,
                'message'   => 'Category not found.'
            ], 404);
        }

        $users = $category->users->filter(function ($value, $key) {
            return $value->consultations->count() <= 0;
        });

        return response()->json(
            $users->makeHidden(['email', 'role_id', 'consultations', 'created_at', 'updated_at', 'pivot'])->toArray(), 
        200);
    }
}
