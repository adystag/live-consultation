@extends('layouts.shell')

@section('content')
<div class="row d-flex align-items-center justify-content-center" style="height: 100vh;">
    <div class="col-md-4">
        <div class="p-3 bg-white rounded shadow-sm">
            <h1 class="text-center">{{ config('app.name', 'Laravel') }}</h1>
            
            <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                @csrf
                
                <div class="form-group">
                    <label for="first_name">{{ __('First Name') }}</label>
                    
                    <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" required autofocus>
                    
                    @if ($errors->has('first_name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('first_name') }}</strong>
                    </span>
                    @endif
                </div>
                
                <div class="form-group">
                    <label for="last_name">{{ __('Last Name') }}</label>
                    
                    <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}" required autofocus>
                    
                    @if ($errors->has('last_name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('last_name') }}</strong>
                    </span>
                    @endif
                </div>
                
                <div class="form-group">
                    <label for="email">{{ __('E-Mail Address') }}</label>
                    
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                    
                    @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>
                
                <div class="form-group">
                    <label for="password">{{ __('Password') }}</label>
                    
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                    
                    @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                </div>
                
                <div class="form-group">
                    <label for="password-confirm">{{ __('Confirm Password') }}</label>
                    
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                </div>
                
                <div class="form-group mb-1">
                    <button type="submit" class="btn btn-block btn-primary">
                        {{ __('Register') }}
                    </button>
                </div>
                
                <div class="form-group mb-0 d-flex align-items-center justify-content-center">                        
                    <a class="btn btn-link" href="{{ route('login') }}">
                        {{ __('Login ') }}
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
