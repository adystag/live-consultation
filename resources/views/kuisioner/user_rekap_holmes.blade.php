@extends('layouts.app')

@section('main')
<div class="card">
        <div class="card-header">
          <ul class="nav nav-tabs card-header-tabs">
            <li class="nav-item">
              <a class="nav-link" href="/kuisioner/rekapUserBdi">Depresi</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/kuisioner/rekapUserDassK">Kecemasan</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/kuisioner/rekapUserDassS">Kecemasan</a>
            </li>
          </ul>
        </div>
        <div class="card-body">
<div class="p-3 rounded shadow-sm bg-white">
        <nav class="nav nav-pills nav-justified">
                <a class="nav-item nav-link" href="/kuisioner/rekapUserDassS">Dass 42</a>
                <a class="nav-item nav-link active" href="/kuisioner/rekapUserHolmes">Holmes & Rahe</a>
              </nav>
              <hr>
    <h3 class="mb-3">Rekapitulasi Kuisioner Holmes & Rahe</h3>
    
    @if (session('status'))
    <div class="alert alert-{{ session('status') }} alert-dismissible fade show mb-3" role="alert">
        {{ session('status-message') }}
        
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
            @csrf
    <table class="table table-striped mb-0">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Email</th>
                <th scope="col">Skor</th>
                <th scope="col">Tingkat Stress</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        
        <tbody>
                <?php $i=1; ?>
                @foreach($pasien as $row)
                <tr>
                        <td>{{ $i }}</td>
                        <td>{{ $row->email }}</td>
                        <td>{{ $row->total }}</td>
                        <td>{{ $row->tingkat }}</td>
                        <td><a href="/kuisioner/hasilHolmes/{{ $row->id }}" class="btn btn-success">Lihat</a></td>
                    </tr>
                    <?php $i++; ?>
                @endforeach
                
        </tbody>
    </table>
</div>
</div>
        
                
                    
</div>

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
<link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css') }}">
@endpush

@push('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
<script src="{{ asset('js/sweetalert2.min.js') }}"></script>
<script>
    $(document).ready(function() {
    $('#table').DataTable( {
        "ordering": false
    } );
} );
    </script>
    @endpush

