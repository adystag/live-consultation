@extends('layouts.shell')

@section('content')
<div class="row justify-content-center align-items-center" style="height: 100vh;">
    <div class="col-md-4">
        <div class="p-3 bg-white rounded shadow-sm">
            <h1 class="text-center">Reset Password</h1>
            
            @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
            @endif
            
            <form method="POST" action="{{ route('password.email') }}" aria-label="{{ __('Reset Password') }}">
                @csrf
                
                <div class="form-group">
                    <label for="email">{{ __('E-Mail Address') }}</label>
                    
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                    
                    @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>
                
                <div class="form-group mb-1">
                    <button type="submit" class="btn btn-block btn-primary">
                        {{ __('Send Password Reset Link') }}
                    </button>
                </div>
                
                <div class="form-group mb-0 d-flex align-items-center justify-content-between">
                    <a class="btn btn-link" href="{{ route('login') }}">
                        {{ __('Login') }}
                    </a>
                    
                    <a class="btn btn-link" href="{{ route('register') }}">
                        {{ __('Register ') }}
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
