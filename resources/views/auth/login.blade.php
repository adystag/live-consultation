@extends('layouts.shell')

@section('content')
<div class="row align-items-center justify-content-center" style="height: 100vh;">
    <div class="col-md-4">
        <div class="p-3 bg-white rounded shadow-sm">
            <h1 class="text-center">{{ config('app.name', 'Laravel') }}</h1>
            
            <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                @csrf
                
                <div class="form-group">
                    <label for="email">{{ __('E-Mail Address') }}</label>
                    
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                    
                    @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>
                
                <div class="form-group">
                    <label for="password">{{ __('Password') }}</label>
                    
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                    
                    @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                </div>
                
                {{-- <div class="form-group">
                    <div class="col-md-6 offset-md-4">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            
                            <label class="form-check-label" for="remember">
                                {{ __('Remember Me') }}
                            </label>
                        </div>
                    </div>
                </div> --}}
                
                <div class="form-group mb-1">
                    <button type="submit" class="btn btn-block btn-primary">
                        {{ __('Login') }}
                    </button>
                </div>
                
                <div class="form-group mb-0 d-flex align-items-center justify-content-between">
                    <a class="btn btn-link" href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                    </a>
                    
                    <a class="btn btn-link" href="{{ route('register') }}">
                        {{ __('Register ') }}
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
