@extends('layouts.app')

@section('main')
<div class="p-3 rounded shadow-sm bg-white">
    <div class="d-flex align-items-center mb-3">
        <h3 class="m-0">Consultations</h3>

        @can ('create', App\Models\Consultation::class)
        <span class="ml-auto d-flex align-items-center justify-content-center">
            <span class="mr-2">Point remaining:</span>
            <span class="badge badge-pill badge-secondary">{{ $userPoints }}</span>
        </span>

        @if ($userPoints <= 0)
        <a href="{{ route('index-payments') }}" class="ml-2">
            <small>Add point</small>
        </a>
        @else
        <a class="btn btn-primary ml-2" href="{{ route('compose-consultation') }}">
            Open Consultation
        </a>
        @endif
        @endif
    </div>

    @if (session('status'))
    <div class="alert alert-{{ session('status') }} alert-dismissible fade show mb-3" role="alert">
        {{ session('status-message') }}

        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

    <ul class="nav nav-tabs nav-fill mb-3">
        <li class="nav-item">
            <a class="nav-link {{ Request::get('status') != 'closed' ? 'active' : '' }} d-flex align-items-center justify-content-center" href="{{ route('index-consultations') }}">
                <span class="mr-2">Active Consultations</span>
                <span class="badge badge-pill badge-success">{{ $activeConsultationsCount }}</span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link {{ Request::get('status') == 'closed' ? 'active' : '' }} d-flex align-items-center justify-content-center"  href="{{ route('index-consultations') }}?status=closed">
                <span class="mr-2">Closed Consultations</span>
                <span class="badge badge-pill badge-danger">{{ $closedConsultationsCount }}</span>
            </a>
        </li>
    </ul>

    @foreach ($consultations as $consultation)
    <div class="user-case p-3 border rounded bg-white row no-gutters">
        <div class="user-case-profile col-2 d-flex flex-column justify-content-center align-items-center" onclick="showProfileModal({{ Auth::user()->role && Auth::user()->role->name == 'consultant' ? json_encode($consultation->user->toArray()) : json_encode($consultation->consultant->toArray()) }})">
            @if (!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user'))
            <img class="rounded-circle" src="{{ !is_null($consultation->consultant->avatar) && !empty($consultation->consultant->avatar) ?  $consultation->consultant->avatar_url : asset('img/user.png') }}" style="height: 64px; width: 64px; object-fit: contain; object-position: center;">
            @else
            <img class="rounded-circle" src="{{ !is_null($consultation->user->avatar) && !empty($consultation->user->avatar) ?  $consultation->user->avatar_url : asset('img/user.png') }}" style="height: 64px; width: 64px; object-fit: contain; object-position: center;">
            @endif

            <div class="mt-1 text-center">
                @if (!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user'))
                <p class="m-0" style="display: -webkit-box; -webkit-line-clamp: 1; -webkit-box-orient: vertical; overflow: hidden;">
                    <b>{{ $consultation->consultant->first_name.' '.$consultation->consultant->last_name }}</b>
                </p>

                <small>{{ ucfirst($consultation->consultant->role->name) }}</small>
                @else
                <p class="m-0" style="display: -webkit-box; -webkit-line-clamp: 1; -webkit-box-orient: vertical; overflow: hidden;">
                    <b>{{ $consultation->user->first_name.' '.$consultation->user->last_name }}</b>
                </p>

                <small>{{ $consultation->user->role ? ucfirst($consultation->user->role->name) : 'User' }}</small>
                @endif
            </div>
        </div>

        <div class="col-8 pl-3">
            {{-- <p style="font-size: 16px;" class="mb-1">
                <b>{{ $userCase->subject }}</b>
            </p>

            <div>
                {!! $userCase->question !!}
            </div> --}}
        </div>

        <div class="col-2 pl-3 text-right d-flex flex-column">
            <small class="ml-auto">
                <b>Status:</b>
                <span style="color: {{ $consultation->status == 'active' ? 'green' : ($consultation->status == 'closed' ? 'red' : '') }}">{{ strtoupper($consultation->status) }}</span>
            </small>

            <a class="btn btn-primary mt-auto w-50 ml-auto" href="{{ route('show-consultation', [ 'consultationId' => $consultation->id ]) }}" style="text-decoration: none; color: #ffffff;">
                View
            </a>
        </div>
    </div>
    @endforeach

    <div class="d-flex align-items-center justify-content-end">
        @if (Request::get('status') == 'closed')
        {{ $consultations->appends(['status' => 'closed'])->links() }}
        @else
        {{ $consultations->links() }}
        @endif
    </div>
</div>

<div id="profile-modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content p-3 d-flex flex-colum align-items-center justify-content-center" style="position: relative;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute; top: 0; right: 8px;">
                <span aria-hidden="true" style="font-size: 30px;">&times;</span>
            </button>

            <img id="profile-avatar" class="rounded-circle" src="" style="height: 128px; width: 128px; object-fit: contain; object-position: center;">

            <div class="mt-1 text-center">
                <p class="m-0" style="font-size: 24px;">
                    <b id="profile-name">User Name</b>
                </p>

                <p id="profile-role" style="font-size: 18px;">User Role</p>
            </div>
        </div>
    </div>
</div>
@endsection

@push('styles')
<style>
    .user-case:not(:last-child) {
        margin-bottom: 8px;
    }

    .user-case-profile:hover {
        cursor: pointer;
    }

    .pagination {
        margin-bottom: 8px;
    }
</style>
@endpush

@push('scripts')
<script>
    var defaultProfpic = "{!! asset("img/user.png") !!}"

    function showProfileModal (user) {
        $('#profile-modal #profile-name').html(user.first_name + ' ' + user.last_name)
        var userRole = user.role ? (user.role.name[0].toUpperCase() + user.role.name.substring(1)) : 'User';
        $('#profile-modal #profile-role').html(userRole)
        $('#profile-modal #profile-avatar').attr('src', (user.avatar ? user.avatar_url : defaultProfpic))
        $('#profile-modal').modal('show')
    }
</script>
@endpush
