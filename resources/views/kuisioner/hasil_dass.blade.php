@extends('layouts.app')

@section('main')
<div class="p-3 rounded shadow-sm bg-white mb-3">
    <h3 class="mb-3">Jawaban Kuisioner</h3>
    
    @if (session('status'))
    <div class="alert alert-{{ session('status') }} alert-dismissible fade show mb-3" role="alert">
        {{ session('status-message') }}
        
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    {{ csrf_field() }}
    {{ method_field('PUT') }}

   
    <table id="table" class="table table-striped mb-0">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Kondisi</th>
                            <th scope="col">Skor</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                            <input type="hidden" name="email" value="email">
                            <tr>
                                <th>1</th>
                                <td>Menjadi marah karena hal-hal kecil/sepele
                                </td>
                                
                                <td>{{ $dasses->d1 }}</td>                                
                            </tr>
                            <tr>
                                <th>2</th>
                                <td>Mulut terasa kering
                                </td>
                                
                                <td>{{ $dasses->d2 }}</td>  
                            </tr>
                            <tr>
                                <th>3</th>
                                <td>Tidak dapat melihat hal yang positif dari suatu kejadian
                                </td>
                                
                                <td>{{ $dasses->d3 }}</td>  
                                
                            </tr>
                            <tr>
                                <th>4</th>
                                <td>Merasakan gangguan dalam bernafas (nafas cepat, sulit bernafas)
                                </td>
                                
                                <td>{{ $dasses->d4 }}</td>  
                                
                            </tr>
                            <tr>
                                <th>5</th>
                                <td>Merasa tidak kuat lagi untuk melakukan kegiatan
                                </td>
                                
                                <td>{{ $dasses->d5 }}</td>  
                                
                            </tr>
                            <tr>
                                <th>6</th>
                                <td>Cenderung bereaksi berlebihan terhadap suatu situasi
                                </td>
                                
                                <td>{{ $dasses->d6 }}</td>  
                                
                            </tr>
                            <tr>
                                <th>7</th>
                                <td>Kelemahan pada anggota tubuh
                                </td>
                                
                                <td>{{ $dasses->d7 }}</td>  
                                
                            </tr>
                            <tr>
                                <th>8</th>
                                <td>Kesulitan untuk berelaksasi/bersantai
                                </td>
                                
                                <td>{{ $dasses->d8 }}</td>  
                                
                            </tr>
                            <tr>
                                <th>9</th>
                                <td>Cemas berlebihan dalam suatu situasi namun bisa lega bila situasi/hal itu berakhir
                                </td>
                                
                                <td>{{ $dasses->d9 }}</td>  
                                
                            </tr>
                            <tr>
                                <th>10</th>
                                <td>Pesimis
                                </td>
                                
                                <td>{{ $dasses->d10 }}</td>  
                                
                            </tr>
                            <tr>
                                <th>11</th>
                                <td>Mudah merasa kesal
                                </td>
                                
                                <td>{{ $dasses->d11 }}</td>  
                                
                            </tr>
                            <tr>
                                <th>12</th>
                                <td>Merasa banyak menghasilkan energi karena cemas
                                </td>
                                
                                <td>{{ $dasses->d12 }}</td>  
                                
                            </tr>
                            <tr>
                                <th>13</th>
                                <td>Merasa sedih dan depresi
                                </td>
                                <td>{{ $dasses->d13 }}</td>  
                                
                            </tr>
                            <tr>
                                <th>14</th>
                                <td>Tidak sabaran
                                </td>
                                <td>{{ $dasses->d14 }}</td>  
                                
                            </tr>
                            <tr>
                                <th>15</th>
                                <td>Kelelahan
                                </td>
                                <td>{{ $dasses->d15 }}</td>  
                                
                            </tr>
                            <tr>
                                <th>16</th>
                                <td>Kehilangan minat pada banyak hal (misal makan, ambulasi, sosialiasi)
                                </td>
                                <td>{{ $dasses->d16 }}</td>  
                                
                            </tr>
                            <tr>
                                <th>17</th>
                                <td>Merasa diri tidak layak
                                </td>
                                <td>{{ $dasses->d17 }}</td>  
                                
                            </tr>
                            <tr>
                                <th>18</th>
                                <td>Mudah tersinggung
                                </td>
                                <td>{{ $dasses->d18 }}</td>  
                                
                            </tr>
                            <tr>
                                <th>19</th>
                                <td>Berkeringat
                                </td>
                                <td>{{ $dasses->d19 }}</td>  
                                
                            </tr>
                            <tr>
                                <th>20</th>
                                <td>Ketakutan tanpa alasan yang jelas
                                </td>
                                <td>{{ $dasses->d20 }}</td>  
                                
                            </tr>
                            <tr>
                                <th>21</th>
                                <td>Merasa hidup tidak berharga
                                </td>
                                <td>{{ $dasses->d21 }}</td>  
                                
                            </tr>
                            <tr>
                                <th>22</th>
                                <td>Sulit untuk beristirahat
                                </td>
                                <td>{{ $dasses->d22 }}</td>  
                                
                            </tr>
                            <tr>
                                <th>23</th>
                                <td>Kesulitan dalam menelan
                                </td>
                                <td>{{ $dasses->d23 }}</td>  
                                
                            </tr>
                            <tr>
                                <th>24</th>
                                <td>Tidak dapat menikmati hal-hal yang dilakukan
                                </td>
                                <td>{{ $dasses->d24 }}</td>  
                                
                            </tr>
                            <tr>
                                <th>25</th>
                                <td>Perubahan kegiatan jantung dan denyut nadi tanpa stimulasi oleh latihan fisik
                                </td>
                                <td>{{ $dasses->d25 }}</td>  
                                
                            </tr>
                            <tr>
                                <th>26</th>
                                <td>Merasa hilang harapan dan putus asa
                                </td>
                                <td>{{ $dasses->d26 }}</td>  
                                
                            </tr>
                            <tr>
                                <th>27</th>
                                <td>Mudah marah
                                </td>
                                <td>{{ $dasses->d27 }}</td>  
                                
                            </tr>
                            <tr>
                                <th>28</th>
                                <td>Mudah panik
                                </td>
                                <td>{{ $dasses->d28 }}</td>  
                                
                            </tr>
                            <tr>
                                <th>29</th>
                                <td>Kesulitan untuk tenang setelah sesuatu yang mengganggu
                                </td>
                                <td>{{ $dasses->d29 }}</td>  
                                
                            </tr>
                            <tr>
                                <th>30</th>
                                <td>Takut diri terhambat oleh tugas -tugas yang tidak bisa dilakukan
                                </td>
                                <td>{{ $dasses->d30 }}</td>  
                                
                            </tr>
                            <tr>
                                <th>31</th>
                                <td>Sulit untuk antusias pada banyak hal
                                </td>
                                <td>{{ $dasses->d31 }}</td>  
                                
                            </tr>
                            <tr>
                                <th>32</th>
                                <td>Sulit menteloransi gangguan-gangguan terhadap hal-hal yang dilakukan
                                </td>
                                <td>{{ $dasses->d32 }}</td>  
                                
                            </tr>
                            <tr>
                                <th>33</th>
                                <td>Berada pada keadaan tegang
                                </td>
                                <td>{{ $dasses->d33 }}</td>  
                                
                            </tr>
                            <tr>
                                <th>34</th>
                                <td>Berasa tidak berharga
                                </td>
                                <td>{{ $dasses->d34 }}</td>  
                                
                            </tr>
                            <tr>
                                <th>35</th>
                                <td>Tidak dapat memaklumi hal apapun yang menghalangi untuk menyelesaikan hal yang sedang dilakukan
                                </td>
                                <td>{{ $dasses->d35 }}</td>  
                                
                            </tr>
                            <tr>
                                <th>36</th>
                                <td>Ketakutan
                                </td>
                                <td>{{ $dasses->d36 }}</td>  
                                
                            </tr>
                            <tr>
                                <th>37</th>
                                <td>Tidak ada harapan untuk masa depan
                                </td>
                                <td>{{ $dasses->d37 }}</td>  
                                
                            </tr>
                            <tr>
                                <th>38</th>
                                <td>Merasa hidup tidak berarti
                                </td>
                                <td>{{ $dasses->d38 }}</td>  
                                
                            </tr>
                            <tr>
                                <th>39</th>
                                <td>Mudah gelisah
                                </td>
                                <td>{{ $dasses->d39 }}</td>  
                                
                            </tr>
                            <tr>
                                <th>40</th>
                                <td>Khawatir dengan situasi saat diri anda mungkin menjadi panik dan mempermalukan diri anda sendiri
                                </td>
                                <td>{{ $dasses->d40 }}</td>  
                            </tr>
                            <tr>
                                <th>41</th>
                                <td>Gemetar
                                </td>
                                <td>{{ $dasses->d41 }}</td>  
                                
                            </tr>
                            <tr>
                                <th>42</th>
                                <td>Sulit untuk meningkatkan inisaitif dalam melakukan sesuatu
                                </td>
                                <td>{{ $dasses->d42 }}</td>  
                                
                            </tr>
                    </tbody>
                </table>
                <br>
                <hr>
            </div>

            @if (!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'consultant'))
            <div class="p-3 rounded shadow-sm bg-white">
                    <h3 class="mb-3">Hasil Kuisioner</h3>
                <table class="table table-striped mb-0">  
                        <thead>
                                <tr>
                                    <th scope="col"></th>
                                    <th scope="col">Skor</th>
                                    <th scope="col">Tingkat</th>
                                </tr>
                            </thead>                 
                        <tbody>
                                <tr>
                                    <td>Skor Depresi</td>
                                    <td>{{ $dasses->depresi }}</td>
                                    <td>{{ $dasses->tingkat_d }}</td>
                                </tr>
                                <tr>
                                    <td>Skor Kecemasan</td>
                                    <td>{{ $dasses->kecemasan }}</td>
                                    <td>{{ $dasses->tingkat_a }}</td>
                                </tr>
                                <tr>
                                    <td>Skor Stress</td>
                                    <td>{{ $dasses->stress }}</td>
                                    <td>{{ $dasses->tingkat_s }}</td>                            
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    
                    <div class="p-3 rounded shadow-sm bg-white mb-3">
                            <form method="post" action="/kuisioner/updateDass/{{ $dasses->id }}">

                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <h3 class="mb-3">Keterangan</h3>
                                <textarea class="form-control" name="penjelasan" >{{ $dasses->penjelasan }}</textarea>
                        <input type="hidden" class="form-control" name="status" value="Selesai di Analisis"/>
                        <br>

                        <div class="form-group mb-0 d-flex">
                                <button type="submit" class="btn btn-primary ml-auto">Submit</button>
                            </div>
                        </form>
                    </div>
                    
            @elseif (!Auth::user()->role || (Auth::user()->role && Auth::user()->role->name == 'user' && $dasses->status == 'Selesai di Analisis'))
            <div class="p-3 rounded shadow-sm bg-white">
                    <h3 class="mb-3">Hasil Kuisioner</h3>
                <table class="table table-striped mb-0">  
                        <thead>
                                <tr>
                                    <th scope="col"></th>
                                    <th scope="col">Skor</th>
                                    <th scope="col">Tingkat</th>
                                </tr>
                            </thead>                 
                        <tbody>
                                <tr>
                                    <td>Skor Depresi</td>
                                    <td>{{ $dasses->depresi }}</td>
                                    <td>{{ $dasses->tingkat_d }}</td>
                                </tr>
                                <tr>
                                    <td>Skor Kecemasan</td>
                                    <td>{{ $dasses->kecemasan }}</td>
                                    <td>{{ $dasses->tingkat_a }}</td>
                                </tr>
                                <tr>
                                    <td>Skor Stress</td>
                                    <td>{{ $dasses->stress }}</td>
                                    <td>{{ $dasses->tingkat_s }}</td>                            
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    
                    <div class="p-3 rounded shadow-sm bg-white mb-3">

                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <h3 class="mb-3">Keterangan</h3>
                                <textarea class="form-control" name="penjelasan" >{{ $dasses->penjelasan }}</textarea>
                        <input type="hidden" class="form-control" name="status" value="Selesai di Analisis"/>
                        <br>
                    </div>
            @endif

            


@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
<link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css') }}">
@endpush

@push('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
<script src="{{ asset('js/sweetalert2.min.js') }}"></script>
<script>
    $(document).ready(function() {
    $('#table').DataTable( {
        "ordering": false
    } );
} );
    </script>
    @endpush

