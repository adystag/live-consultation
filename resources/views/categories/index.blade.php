@extends('layouts.app')

@section('main')
<div class="p-3 rounded shadow-sm bg-white mb-3">
    <h3 class="mb-3">Create New Category</h3>
    
    @if (session('status-create'))
    <div class="alert alert-{{ session('status-create') }} alert-dismissible fade show mb-3" role="alert">
        {{ session('status-create-message') }}
        
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    
    <form id="category-form" method="POST" action="{{ route('create-category') }}">
        @csrf
        
        <div class="form-group">
            <label>Category Name</label>
            <input type="text" name="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="Enter category name">

            @if ($errors->has('name'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
            @endif
        </div>
        
        <div class="form-group mb-0 d-flex">
            <button type="submit" class="btn btn-primary ml-auto">Submit</button>
        </div>
    </form>
</div>

<div class="p-3 rounded shadow-sm bg-white mb-3">
    <h3 class="mb-3">Categories</h3>
    
    @if (session('status'))
    <div class="alert alert-{{ session('status') }} alert-dismissible fade show mb-3" role="alert">
        {{ session('status-message') }}
        
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    
    <table id="categories-table" class="table table-striped">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        
        <tbody>
            @foreach ($categories as $key => $category)
            <tr>
                <th>{{ $key + 1}}</th>
                <td>{{ $category->name }}</td>
                <td>
                    <a class="btn btn-warning btn-sm" href="{{ route('show-category', [ 'categoryId' => $category->id ]) }}">Manage</a>
                    <button class="btn btn-danger btn-sm" onclick="confirmDeleteCategory({{ $category->id }}, '{{ $category->name }}')">Delete</button>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    
    <form id="delete-category-form" method="POST">
        @csrf
        @method('DELETE')
    </form>
</div>
@endsection

@push('styles')
<style>
    #category-form-wrapper {
        display: none;
    }
</style>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
<link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css') }}">
@endpush

@push('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
<script src="{{ asset('js/sweetalert2.min.js') }}"></script>
<script>
    $(document).ready(function () {
        $('#categories-table').DataTable();
    })
    
    function confirmDeleteCategory (id, name) {
        var url = '{!! url("/categories") !!}'
        
        swal({
            title: 'Delete Category',
            text: 'Do you want to delete this category: ' + name,
            type: 'question',
            showCancelButton: true,
            confirmButtonText: 'Yes'
        })
        .then(result => {
            if (!result.dismiss) {
                $('#delete-category-form').attr('action', url + '/' + id)
                $('#delete-category-form').submit()
            }
        })
    }
</script>
@endpush