@extends('layouts.app')

@section('main')

<div class="p-3 rounded shadow-sm bg-white">
        <nav class="nav nav-pills nav-justified">
                <a class="nav-item nav-link active" href="/kuisioner/tmas">TMAS</a>
                <a class="nav-item nav-link" href="/kuisioner/hars">HARS</a>
              </nav>
              <hr>
    <h3 class="mb-3">Taylor Manifest Anxiety Scale (TMAS)
        </h3>
    
    @if (session('status'))
    <div class="alert alert-{{ session('status') }} alert-dismissible fade show mb-3" role="alert">
        {{ session('status-message') }}
        
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    
    <form action="{{ route('tambahdatatmas')}}" method="POST">
            @csrf
    <table class="table table-striped mb-0">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Kondisi</th>
                <th scope="col">Y</th>
                <th scope="col">T</th>
            </tr>
        </thead>
        
        <tbody>
                <input type="hidden" name="email" value="{{ Auth::user()->email }}">
                <tr>
                    <th>1</th>
                    <td>Saya tidak cepat lelah
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d1" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d1" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>2</th>
                    <td>Saya sering mengalami perasaan mual
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d2" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d2" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>3</th>
                    <td>Saya sering merasa tegang pada waktu bekerja/beraktifitas
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d3" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d3" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>4</th>
                    <td>Saya merasa sukar untuk konsentrasi pada suatu hal
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d4" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d4" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>5</th>
                    <td>Saya cemas akan keadaan sakit saya
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d5" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d5" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>6</th>
                    <td>Saya sering melihat bahwa tangan saya bergetar apabila saya mencoba mengerjakan sesuatu
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d6" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d6" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>7</th>
                    <td>Muka saya sering menjadi merah seperti juga terjadi pada orang lain
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d7" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d7" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>8</th>
                    <td>Saya sering merasa khawatir akan kemungkinan terjadinya hal-hal yang tidak menyenangkan pada diri saya
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d8" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d8" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>9</th>
                    <td>Saya sering takut bahwa muka saya sering merah (tersipu malu)
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d9" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d9" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>10</th>
                    <td>Saya sering mengalami mimpi yang menakutkan pada waktu tidur
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d10" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d10" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>11</th>
                    <td>Saya mudah berkeringat meski hari tidak panas
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d11" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d11" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>12</th>
                    <td>Kadang-kadang saya merasa kikuk, saya menjadi berkeringat dan sangat mengganggu saya
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d12" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d12" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>13</th>
                    <td>Saya jarang merasa jantung berdebar-debar dan saya jarang merasa tersengal-sengal
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d13" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d13" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>14</th>
                    <td>Saya setiap saat merasa lapar
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d14" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d14" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>15</th>
                    <td>Saya sering mengalami gangguan perut
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d15" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d15" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>16</th>
                    <td>Saya sering tidak dapat tidur karena mengkhawatirkan sesuatu
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d16" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d16" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>17</th>
                    <td>Tidur saya tidak nyenyak dan sering terganggu
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d17" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d17" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>18</th>
                    <td>Saya sering mimpi mengenai hal yang tidak saya ceritakan pada orang lain
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d18" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d18" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>19</th>
                    <td>Saya mudah untuk merasa kikuk
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d19" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d19" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>2Y</th>
                    <td>Saya sering menemukan bahwa saya mengkhawatirkan sesuatu
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d20" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d20" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>21</th>
                    <td>Saya biasanya tenang dan tidak mudah marah
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d21" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d21" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>22</th>
                    <td>Saya mudah menangis
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d22" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d22" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>23</th>
                    <td>Saya hampir selalu merasa khawatir mengenai suatu hal atau seseorang
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d23" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d23" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>24</th>
                    <td>Saya hampir selalu gembira
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d24" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d24" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>25</th>
                    <td>Saya merasa gelisah apabila saya harus menanti
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d25" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d25" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>26</th>
                    <td>Pada waktu-waktu tertentu saya gelisah
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d26" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d26" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>27</th>
                    <td>Kadang-kadang saya begitu bergelora, sehingga sangat sukar bagi saya untuk tidur
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d27" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d27" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>28</th>
                    <td>Saya kadang-kadang merasa bahwa kesukaran-kesukaran menumpuk begitu tinggi sehingga saya tidak dapat mengatasinya
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d28" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d28" value="0" checked="checked"></label></td> 
                </tr>
                <tr>
                    <th>29</th>
                    <td>Pada waktu-waktu tertentu saya merasa khawatir tanpa alasan mengenai sesuatu yang sesungguhnya tidak berarti
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d29" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d29" value="0" checked="checked"></label></td>  
                </tr>
                <tr>
                    <th>30</th>
                    <td>Apabila dibanding dengan teman-teman saya, saya tidak banyak mempunyai ketakutan-ketakutan seperti mereka
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d30" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d30" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>31</th>
                    <td>Saya sering takut terhadap benda-benda atau manusia yang saya tahu tidak akan menyakiti saya
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d31" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d31" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>32</th>
                    <td>Pada waktu-waktu tertentu saya merasa tidak berguna
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d32" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d32" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>33</th>
                    <td>Saya merasa sukar untuk memusatkan perhatian saya
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d33" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d33" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>34</th>
                    <td>Saya adalah seseorang yang menganggap bahwa segala sesuatu berat
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d34" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d34" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>35</th>
                    <td>Saya adalah seorang yang sering gugup
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d35" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d35" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>36</th>
                    <td>Hidup sering merupakan beban bagi saya
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d36" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d36" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>37</th>
                    <td>Pada waktu-waktu tertentu saya merasa bahwa saya orang yang sama sekali tidak berguna
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d37" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d37" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>38</th>
                    <td>Saya benar-benar tidak percaya pada diri sendiri
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d38" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d38" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>39</th>
                    <td>Pada waktu-waktu tertentu saya merasa hancur
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d39" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d39" value="0" checked="checked"></label></td>
                </tr>
                <tr>
                    <th>40</th>
                    <td>Saya tidak suka untuk menghadapi kesukaran atau membuat keputusan yang penting
                    </td>
                    <input type="hidden" name="id_dass" value="">
                    <td><label class="radio-inline"><input type="radio" name="d40" value="1"></label></td>
                    <td><label class="radio-inline"><input type="radio" name="d40" value="0" checked="checked"></label></td>
                </tr>
        </tbody>
    </table><br>
    <div class="form-group mb-0 d-flex">
            <button type="submit" class="btn btn-primary ml-auto">Submit</button>
        </div>
    </form>
        
                
                    
</div>

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
<link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css') }}">
@endpush

@push('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
<script src="{{ asset('js/sweetalert2.min.js') }}"></script>
<script>
    $(document).ready(function() {
    $('#table').DataTable( {
        "ordering": false
    } );
} );
    </script>
    @endpush

